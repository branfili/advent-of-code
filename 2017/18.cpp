#include <iostream>
#include <vector>

using namespace std;
const int MAXN = 26;
string s;
char r;
long long x, y;
int z;

long long a[2][MAXN];
int p[2];
int reg[2];
int snd[2];

vector <long long> v[2];
vector <string> v2;

long long uintaj(int cur, string s){
  if (s == "0"){
    return 0;
  }

  if ((int) s.size() == 1 &&
      s[0] >= 'a' && s[0] <= 'z'){
    return a[cur][s[0] - 'a'];
  }

  bool neg = false;
  if (s[0] == '-'){
    neg = true;
    s = s.substr(1);
  }

  long long z = 0;
  for (int i = 0; i < (int) s.size(); i++){
    z *= 10;
    z += (s[i] - '0');
  }

  if (neg){
    z *= -1;
  }
  return z;
}

bool uvjet(int cur){
  return (reg[cur] < (int) v2.size());
}

bool wait(int cur){
  return (v2[reg[cur]].substr(0, 3) == "rcv" &&
          p[cur] == (int) v[cur].size());
}

void run(int cur){
  while (uvjet(cur)){
    s = v2[reg[cur]].substr(0, 3);
    if (s == "jgz"){
      for (z = 4; z < (int) v2[reg[cur]].size() && v2[reg[cur]][z] != ' '; z++);
      x = uintaj(cur, v2[reg[cur]].substr(4, z - 4));
      y = uintaj(cur, v2[reg[cur]].substr(z + 1));
      if (x > 0){
        reg[cur] += y - 1;
      }
    }
    else{
      r = v2[reg[cur]][4];
      if (s == "snd"){
        v[1 - cur].push_back(a[cur][r - 'a']);
        snd[cur]++;
      }
      else if (s == "rcv"){
        if (wait(cur)){
          if (wait(1 - cur)){ //deadlock
            break;
          }
          cur = 1 - cur;
          continue;
        }
        a[cur][r - 'a'] = v[cur][p[cur]];
        p[cur]++;
      }
      else{
        x = uintaj(cur, v2[reg[cur]].substr(6));
        if (s == "set"){
          a[cur][r - 'a'] = x;
        }
        else if (s == "add"){
          a[cur][r - 'a'] += x;
        }
        else if (s == "mul"){
          a[cur][r - 'a'] *= x;
        }
        else if (s == "mod"){
          a[cur][r - 'a'] %= x;
        }
      }
    }
    reg[cur]++;

    if (!wait(1 - cur)){
      cur = 1 - cur;
    }
  }
}

int main (void){
  while (getline(cin, s)){
    v2.push_back(s);
  }

  for (int i = 0; i < 2; i++){
    a[i]['p' - 'a'] = i;
  }

  run(0);

  cout << snd[1] << endl;
}
