#include <cstdio>
#include <iostream>

using namespace std;
const int MAXN = 1e8;
const int STEP = 12629077;
bool a[MAXN];
int cur, st;
int sol;

int main (void){
  cur = MAXN / 2;

  for (int i = 0; i < STEP; i++){
    switch(st){
      case 0:
        if (a[cur] == 0){
          a[cur] = 1;
          cur++;
          st = 1;
        }
        else{
          a[cur] = 0;
          cur--;
          st = 1;
        }
        break;
      case 1:
        if (a[cur] == 0){
          a[cur] = 0;
          cur++;
          st = 2;
        }
        else{
          a[cur] = 1;
          cur--;
          st = 1;
        }
        break;
      case 2:
        if (a[cur] == 0){
          a[cur] = 1;
          cur++;
          st = 3;
        }
        else{
          a[cur] = 0;
          cur--;
          st = 0;
        }
        break;
      case 3:
        if (a[cur] == 0){
          a[cur] = 1;
          cur--;
          st = 4;
        }
        else{
          a[cur] = 1;
          cur--;
          st = 5;
        }
        break;
      case 4:
        if (a[cur] == 0){
          a[cur] = 1;
          cur--;
          st = 0;
        }
        else{
          a[cur] = 0;
          cur--;
          st = 3;
        }
        break;
      case 5:
        if (a[cur] == 0){
          a[cur] = 1;
          cur++;
          st = 0;
        }
        else{
          a[cur] = 1;
          cur--;
          st = 4;
        }
        break;
      default:
        break;
    }
  }

  for (int i = 0; i < MAXN; i++){
    sol += a[i];
  }

  cout << sol << endl;
  return 0;
}
