#include <iostream>
#include <algorithm>
#include <vector>

using namespace std;
string s;
int sol;
char c[128][128];

int h[256];
bool bio[128][128];

int smjx[] = {-1, 0, 1, 0};
int smjy[] = {0, 1, 0, -1};

string ustringaj(int x, int b, int len){
  if (x == 0){
    if (len == -1){
      return "0";
    }
    else{
      string s = "";
      for (int i = 0; i < len; i++){
        s += '0';
      }
      return s;
    }
  }

  string s = "";
  while (x > 0){
    s += (x % b) + '0';
    x /= b;
  }
  if (len != -1){
    for (int i = (int)s.size(); i < len; i++){
      s += '0';
    }
  }
  reverse(s.begin(), s.end());

  return s;
}

void revers(int x, int l){
  int a = x;
  int b = x + l - 1;
  for (; a < b; a++, b--){
    h[a % 256] ^= h[b % 256];
    h[b % 256] ^= h[a % 256];
    h[a % 256] ^= h[b % 256];
  }
}

string knotHash(string s){
  for (int i = 0; i < 256; i++){
    h[i] = i;
  }

  vector<int> v;
  
  for (int i = 0; i < (int)s.size(); i++){
    v.push_back(s[i]);
  }
  v.push_back(17);
  v.push_back(31);
  v.push_back(73);
  v.push_back(47);
  v.push_back(23);

  int sk = 0;
  int cur = 0;
  for (int i = 0; i < 64; i++){
    for (int j = 0; j < (int) v.size(); j++){
      revers(cur, v[j]);
      
      cur = (cur + sk + v[j]) % 256;
      sk++;
    }
  }

  string res = "";
  for (int i = 0; i < 16; i++){
    int x = 0;
    for (int j = 0; j < 16; j++){
      x ^= h[i*16 + j];
    }

    string tmp = ustringaj(x, 2, 8);
    for (int j = 0; j < 8; j++){
      res += (tmp[j] == '1' ? '#' : '.');
    }
  }

  return res;
}

bool uvjet(int x, int y){
  if (x < 0 || x >= 128 || y < 0 || y >= 128){
    return false;
  }
  
  if (c[x][y] == '.'){
    return false;
  }

  return !bio[x][y];
}

void floodfill(int x, int y){
  if (!uvjet(x, y)){
    return ;
  }

  bio[x][y] = true;
  for (int i = 0; i < 4; i++){
    floodfill(x + smjx[i], y + smjy[i]);
  }
}

int main (void){
  cin >> s;
  
  for (int i = 0; i < 128; i++){
    string s2 = knotHash(s + '-' + ustringaj(i, 10, -1));
    for (int j = 0; j < 128; j++){
      c[i][j] = s2[j];
    }
  }

  for (int i = 0; i < 128; i++){
    for (int j = 0; j < 128; j++){
      if (!bio[i][j] && c[i][j] == '#'){
        floodfill(i, j);
        sol++;
      }
    }
  }

  cout << sol << endl;
  return 0;
}
