#include <iostream>
#include <vector>

using namespace std;
int x, y;
int cur;
int sol;

vector<int> v;

int main (void){
  while (cin >> x){
    v.push_back(x);
  }

  for (sol = 0; cur >=0 && cur < (int) v.size(); sol++){
    y = cur;
    cur += v[y];
    if (v[y] >= 3){
      v[y]--;
    }
    else{
      v[y]++;
    }
  }

  cout << sol << endl;
  return 0;
}
