#include <iostream>
#include <map>
#include <vector>
#include <algorithm>

using namespace std;

typedef struct{
  string name;
  int val;
  int index;
  vector<string> child;
} Node;

int sol;

string s;
string s2;

string root;

vector<bool> bio;
vector<int> w;
map<string, Node> m;

int uintaj(string s){
  int x=0;
  for (int i=0;i<(int)s.size();i++){
    x*=10;
    x+=(s[i]-'0');
  }
  return x;
}

void rek(Node x){
  bio[x.index] = true;

  for (int i = 0; i < (int) x.child.size(); i++){
    rek(m[x.child[i]]);
  }

  return ;
}

int rek2(Node x){
  int z = 0;
  z += x.val;

  for (int i = 0; i < (int) x.child.size(); i++){
    z += rek2(m[x.child[i]]);
  }

  return w[x.index] = z;
}

void rek3(Node x, int delta){
  if (x.child.empty()){
    sol = x.val + delta;
    return;
  }
  
  vector<pair<int, string>> v;
  
  for (int i = 0; i < (int) x.child.size(); i++){
    v.push_back({w[m[x.child[i]].index], x.child[i]});
  }

  sort(v.begin(), v.end());
  if (v[0].first == v[v.size() - 1].first){
    sol = x.val + delta;
  }
  else if (v[0].first == v[1].first){
    rek3(m[v[v.size() - 1].second], v[0].first - v[v.size() - 1].first);
  }
  else{
    rek3(m[v[0].second], v[v.size() - 1].first - v[0].first);
  }
  
  return ;
}

int main (void){
  while (getline(cin, s)){
    s += ' ';
    s2 = "";
    int flag = 0;
    Node tmp;

    for (int i = 0; i < (int) s.size(); i++){
      if (s[i] == ' '){
        if (s2 == "->"){
          flag = 1;
          s2 = "";
          continue;
        }
        if (flag == 0){
          if (s2[0] == '\('){
            tmp.val = uintaj(s2.substr(1, s2.size() - 2));
          }
          else{
            tmp.name = s2;
          }
        }
        else{
          if (s2[s2.size() - 1] == ','){
            s2 = s2.substr(0, s2.size() - 1);
          }
          tmp.child.push_back(s2);
        }
        s2 = "";
      }
      else{
        s2 += s[i];
      }
    }
    tmp.index = bio.size();

    m[tmp.name] = tmp;
    bio.push_back(false);
    w.push_back(0);
  }

  for (auto it = m.begin(); it != m.end(); it++){
    for (int i = 0; i < (int) bio.size(); i++){
      bio[i] = false;
    }

    rek(it->second);

    bool flag = true;
    for (int i = 0; i < (int) bio.size(); i++){
      if (!bio[i]){
        flag = false;
        break;
      }
    }

    if (flag){
      root = it->first;
      break;
    }
  }
  rek2(m[root]);

  rek3(m[root], 0);

  cout << sol << endl;
  return 0;
}
