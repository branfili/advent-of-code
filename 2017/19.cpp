#include <iostream>
#include <vector>

using namespace std;
string s;
string sol;
int curx, cury;
int lx, ly;
int smj;
int cnt;

int smjx[] = {-1, 0, 1, 0};
int smjy[] = {0, 1, 0, -1};

vector <string> v;

bool uvjet(int x, int y){
  if (x < 0 || x >= (int) v.size()){
    return false;
  }
  if (y < 0 || y >= (int) v[x].size()){
    return false;
  }
  if (x == lx && y == ly){
    return false;
  }
  return (v[x][y] != ' ');
}

int main (void){
  while (getline(cin, s)){
    v.push_back(s);
  }

  for (int i = 0; i < (int) v[0].size(); i++){
    if (v[0][i] == '|'){
      curx = 0;
      cury = i;
      break;
    }
  }
  smj = 2;

  lx = ly = -1;

  while (uvjet(curx, cury)){
    if (v[curx][cury] == '+'){
      for (int i = 0; i < 4; i++){
        if (uvjet(curx + smjx[i], cury + smjy[i])){
          smj = i;       
          break;
        }
      }
    }

    lx = curx;
    ly = cury;

    curx += smjx[smj];
    cury += smjy[smj];

    if (v[curx][cury] >= 'A' &&
        v[curx][cury] <= 'Z'){
      sol += v[curx][cury];
    }

    cnt++;
  }

  cout << sol << endl;
  cout << cnt << endl;
  return 0;
}
