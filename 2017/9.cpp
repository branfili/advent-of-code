#include <iostream>

using namespace std;
string s;
int d;
bool garbage;

int sol;

bool escaped(int x){
  int z = 0;
  for (int i = x - 1; i >= 0 && s[i] == '!'; i--, z++);
  return (z % 2 == 1);
}

int main (void){
  cin >> s;
  for (int i = 0; i < (int) s.size(); i++){
    if (escaped(i)){
      continue;
    }
    
    if (garbage){
      if (s[i] == '>'){
        garbage = false;
      }
      else if (s[i] != '!'){
        sol++;
      }
    }
    else{
      if (s[i] == '<'){
        garbage = true;
      }
      else if (s[i] == '{'){
        d++;
      }
      else if (s[i] == '}'){
        d--;
      }
    }
  }
  cout << sol << endl;
  return 0;
}
