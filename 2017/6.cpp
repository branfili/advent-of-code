#include <set>
#include <vector>
#include <iostream>
#include <algorithm>

using namespace std;
int x;
int sol;
int re;
int maks, maksi;

vector<int> v;
vector<vector<int>> v2;

int main(void){
  while (cin >> x){
    v.push_back(x);
  }

  for (sol = 0; find(v2.begin(), v2.end(), v) == v2.end(); sol++){
    v2.push_back(v);
    maks = 0;

    for (int i = 0; i < (int) v.size(); i++){
      if (v[i] > maks){
        maks = v[i];
        maksi = i;
      }
    }

    re = maks;
    v[maksi] = 0;

    for (int i = maksi + 1; re > 0; i++){
      if (i >= (int) v.size()){
        i %= (int) v.size();
      }
      re--;
      v[i]++;
    }
  }

  for (int i = 0; v2[i] != v; i++){
    sol--;
  }

  cout << sol << endl;
  return 0;
}
