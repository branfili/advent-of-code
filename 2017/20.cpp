#include <iostream>
#include <tuple>
#include <vector>
#include <algorithm>

using namespace std;
const long long MAXN = 1e3;
const tuple<long long, long long, long long> CENTER = make_tuple(0LL, 0LL, 0LL);

typedef struct{
  tuple<long long, long long, long long> x, v, a;
} particle;

int sol;

string s;
string s2, s3;

vector <particle> v;

long long uintaj(string s){
  if (s == "0"){
    return 0;
  }
  
  bool neg = false;
  if (s[0] == '-'){
    neg = true;
    s = s.substr(1);
  }

  long long x = 0;
  for (int i = 0; i < (int) s.size(); i++){
    x *= 10;
    x += (s[i] - '0');
  }

  if (neg){
    x *= -1;
  }
  return x;
}

tuple<long long, long long, long long> process(string s){
  s = s.substr(3, s.size() - 5);

  vector <long long> buff;

  s += ',';
  s3 = "";
  for (int i = 0; i < (int) s.size(); i++){
    if (s[i] == ','){
      buff.push_back(uintaj(s3));
      s3 = "";
      continue;
    }
    s3 += s[i];
  }

  return make_tuple(buff[0], buff[1], buff[2]);
}

tuple<long long, long long, long long> operator +(
    tuple<long long, long long, long long> a,
    tuple<long long, long long, long long> b){

  return make_tuple(get<0>(a) + get<0>(b),
                    get<1>(a) + get<1>(b),
                    get<2>(a) + get<2>(b));
}

tuple<long long, long long, long long> operator *(
    long long c,
    tuple<long long, long long, long long> a){

  return make_tuple(c * get<0>(a),
                    c * get<1>(a),
                    c * get<2>(a));
}

long long distance(
    tuple<long long, long long, long long> a,
    tuple<long long, long long, long long> b){

  return abs(get<0>(a) - get<0>(b)) +
         abs(get<1>(a) - get<1>(b)) +
         abs(get<2>(a) - get<2>(b));
}

int main (void){
  while (getline(cin, s)){
    particle tmp;

    s += ", ";
    s2 = "";
    for (int i = 0; i < (int) s.size(); i++){
      if (s[i] == ' '){
        if (s2[0] == 'p'){
          tmp.x = process(s2);
        }
        else if (s2[0] == 'v'){
          tmp.v = process(s2);
        }
        else {
          tmp.a = process(s2);
        }
        s2 = "";
        continue;
      }
      s2 += s[i];
    }

    v.push_back(tmp);
  }

  for (int i = 0; i < MAXN; i++){
    vector <bool> exists;

    for (int j = 0; j < (int) v.size(); j++){
      v[j].v = v[j].v + v[j].a;
      v[j].x = v[j].x + v[j].v;
      exists.push_back(true);
    }

    for (int j = 0; j < (int) v.size(); j++){
      for (int k = 0; k < (int) v.size(); k++){
        if (j != k && v[j].x == v[k].x){
          exists[j] = false;
          exists[k] = false;
        }
      }
    }

    vector <particle> v2;
    for (int j = 0; j < (int) v.size(); j++){
      if (exists[j]){
        v2.push_back(v[j]);
      }
    }

    v = v2;
  }

  cout << v.size() << endl;

  return 0;
}
