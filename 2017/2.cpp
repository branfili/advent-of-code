#include <cstdio>

using namespace std;
int n, m;
int sol;

int a[20];

int main (void){
  scanf("%d%d", &n, &m);

  for (int i=0;i<n;i++){
    for (int j=0;j<m;j++){
      scanf("%d", &a[j]);
    }

    bool flag=false;
    for (int j=0;j<m;j++){
      for (int k=0;k<m;k++){
        if (j != k && a[j] % a[k] == 0){
          sol += a[j] / a[k];
          flag = true;
          break;
        }
      }
      if (flag){
        break;
      }
    }
  }

  printf("%d\n", sol);
  return 0;
}
