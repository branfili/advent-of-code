#include <cstdio>
#include <algorithm>

using namespace std;
const int MAXN=1e4;
int x;
int a[MAXN][MAXN];
int smjx[4] = {0, -1, 0, 1};
int smjy[4] = {1, 0, -1, 0};
int smjx2[8] = {0, -1, -1, -1, 0, 1, 1, 1};
int smjy2[8] = {1, 1, 0, -1, -1, -1, 0, 1};

bool uvjet(int x, int y, int val){
  return (x >= 0 &&
          x < MAXN &&
          y >= 0 &&
          y < MAXN &&
          a[x][y] <= val);
}

int sum(int x, int y, int val){
  int s=0;
  for (int i=0;i<8;i++){
    if (uvjet(x, y, val)){
      s += a[x+smjx2[i]][y+smjy2[i]];
    }
  }
  return s;
}

void generate(int x){
  int curX = MAXN / 2;
  int curY = MAXN / 2;

  a[curX][curY] = 1;

  for (int s=0; uvjet(curX, curY, x); s++){
    for (int i=0; i <= s/2; i++){
      curX += smjx[s%4];
      curY += smjy[s%4];
      if (!uvjet(curX, curY, x)){
        break;
      }
      a[curX][curY] = sum(curX, curY, x);
      if (a[curX][curY] > x){
        printf("%d\n", a[curX][curY]);
        break;
      }
    }
  }
}

int main (void){
  scanf("%d", &x);
  
  generate(x);

  return 0;
}
