#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

typedef struct{
  int name;
  string oper;
  int val;
} Condit;

typedef struct {
  int name;
  int mod;
  int val;
  Condit cond;
} Instruct;

int sol;
string s, s2;

vector<int> mem;
vector<Instruct> cod;
vector<string> reg;

vector<string> inp;

int uintaj(string s){
  if (s.empty()){
    return 0;
  }

  bool negative = false;
  if (s[0] == '-'){
    negative = true;
    s = s.substr(1);
  }

  int z = 0;
  for (int i = 0; i < (int) s.size(); i++){
    z *= 10;
    z += (s[i] - '0');
  }

  if (negative){
    z *= -1;
  }
  return z;
}

bool evaluate(Condit cond){
  if (cond.oper == ">"){
    return mem[cond.name] > cond.val;
  }
  else if (cond.oper == "<"){
    return mem[cond.name] < cond.val;
  }
  else if (cond.oper == "=="){
    return mem[cond.name] == cond.val;
  }
  else if (cond.oper == "!="){
    return mem[cond.name] != cond.val;
  }
  else if (cond.oper == "<="){
    return mem[cond.name] <= cond.val;
  }
  else if (cond.oper == ">="){
    return mem[cond.name] >= cond.val;
  }

  return false;
}

int main (void){
  while (getline(cin, s)){
    s += ' ';
    s2 = "";
    inp.clear();

    for (int i = 0; i < (int) s.size(); i++){
      if (s[i] == ' '){
        inp.push_back(s2);
        s2 = "";
        continue;
      }
      s2 += s[i];
    }

    Instruct tmp;

    int x = (int) (find(reg.begin(), reg.end(), inp[0]) - reg.begin());
    if (x == (int) reg.size()){
      reg.push_back(inp[0]);
      mem.push_back(0);
    }
    tmp.name = x;

    tmp.mod = 1;
    if (inp[1] == "dec"){
      tmp.mod *= -1;
    }

    tmp.val = uintaj(inp[2]);

    int y = (int) (find(reg.begin(), reg.end(), inp[4]) - reg.begin());
    if (y == (int) reg.size()){
      reg.push_back(inp[4]);
      mem.push_back(0);
    }
    tmp.cond.name = y;

    tmp.cond.oper = inp[5];
    tmp.cond.val = uintaj(inp[6]);

    cod.push_back(tmp);
  }

  for (int i = 0; i < (int) cod.size(); i++){
    if (evaluate(cod[i].cond)){
      mem[cod[i].name] += cod[i].mod * cod[i].val;
    }

    for (int i = 0; i < (int) mem.size(); i++){
      sol = max(sol, mem[i]);
    }
  }

  cout << sol << endl;
  return 0;
}
