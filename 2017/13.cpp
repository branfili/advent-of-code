#include <iostream>
#include <vector>

using namespace std;
int d, x;
int sol;
string s;

vector<pair<int, int>> v;

int uintaj(string s){
  int z = 0;
  for (int i = 0; i < (int) s.size(); i++){
    z *= 10;
    z += (s[i] - '0');
  }
  return z;
}

bool uvjet(int x){
  for (int i = 0; i < (int) v.size(); i++){
    if ((v[i].first + x) % (2 * v[i].second - 2) == 0){
      return false;
    }
  }
  return true;
}

int main (void){
  while (getline(cin, s)){
    int k;
    for (k = 0; s[k] != ':'; k++);
    d = uintaj(s.substr(0, k));
    x = uintaj(s.substr(k + 2));
    v.push_back({d, x});
  }

  for (sol = 0; !uvjet(sol); sol++);
  
  cout << sol << endl;
  return 0;
}
