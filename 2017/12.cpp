#include <iostream>
#include <vector>
#include <cstring>

using namespace std;
const int MAXN=1e4;

string s;
string s2;
int x, y;
int n;
int sol;
int bio[MAXN];

vector<int> v[MAXN];

int uintaj(string s){
  int z = 0;
  for (int i = 0; i < (int) s.size() - 1; i++){
    z *= 10;
    z += (s[i] - '0');
  }
  return z;
}

void floodfill(int x){
  if (bio[x]){
    return ;
  }

  bio[x] = 1;

  for (int i = 0; i < (int) v[x].size(); i++){
    floodfill(v[x][i]);
  }

  return ;
}

int main (void){
  while (getline(cin, s)){
    s += ", ";
    s2 = "";
    bool flag = false;
    for (int i = 0; i < (int)s.size(); i++){
      if (s[i] != ' '){
        s2 += s[i];
        continue;
      }

      if (!flag){
        if (s2 == "<->"){
          flag = true;
          s2 = "";
          continue;
        }

        s2 += ',';
        x = uintaj(s2);

        n = max(n, x + 1);
      }
      else{
        y = uintaj(s2);

        v[x].push_back(y);
        v[y].push_back(x);
      }

      s2 = "";
    }
  }

  for (int i = 0; i < MAXN; i++){
    bio[i] = 0;
  }
  
  for (int i = 0; i < n; i++){
    if (!bio[i]){
      floodfill(i);
      sol++;
    }
  }

  cout << sol << endl;
  return 0;
}
