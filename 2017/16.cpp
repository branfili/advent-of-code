#include <iostream>
#include <algorithm>
#include <vector>

using namespace std;
const int MAXN = 16;
const int MOD = 1e9;
string s;
string s2;
int x, y;
int z;
string a;

vector <string> v;

int find(char c){
  for (int i = 0; i < MAXN; i++){
    if (a[i] == c){
      return i;
    }
  }

  return -1;
}

int uintaj(string s){
  int z = 0;
  for (int i = 0; i < (int) s.size(); i++){
    z *= 10;
    z += (s[i] - '0');
  }
  return z;
}

void spin(void){
  char tmp = a[MAXN - 1];
  for (int i = MAXN - 1; i > 0; i--){
    a[i] = a[i - 1];
  }
  a[0] = tmp;
}

int main (void){
  for (int i = 0; i < MAXN; i++){
    a += (i + 'a');
  }

  cin >> s;
  s += ',';
  for (int j = 0; j < MOD; j++){
    s2 = "";
    for (int i = 0; i < (int) s.size(); i++){
      if (s[i] != ','){
        s2 += s[i];
        continue;
      }

      if (s2[0] == 's'){
        x = uintaj(s2.substr(1));
        for (int j = 0; j < x; j++){
          spin();
        }
      }
      else{
        if (s2[0] == 'p'){
          x = find(s2[1]);
          y = find(s2[3]);
        }
        else if (s2[0] == 'x'){
          int k;
          for (k = 1; s2[k] != '/'; k++);
          x = uintaj(s2.substr(1, k - 1));
          y = uintaj(s2.substr(k + 1));
        }
        else{
          break;
        }

        a[x] ^= a[y];
        a[y] ^= a[x];
        a[x] ^= a[y];
      }

      s2 = "";
    }

    z = (int) (find(v.begin(), v.end(), a) - v.begin());
    if (z != (int) v.size()){
      j = MOD - (MOD - z) % (j - z);
    }
    else{
      v.push_back(a);
    }
  }

  cout << a << endl;
  return 0;
}
