#include <iostream>
#include <algorithm>

using namespace std;
string s;
string s2;
int x, y;
int smjx[6] = {-1, -1, 1, 1, 1, -1};
int smjy[6] = {0, 1, 1, 0, -1, -1};
string smj[6] = {"n", "ne", "se", "s", "sw", "nw"};
int sol;

int main (void){
  getline(cin, s);
  s += ',';
  for (int i = 0; i < (int) s.size(); i++){
    if (s[i] == ','){
      for (int j = 0; j < 6; j++){
        if (s2 == smj[j]){
          x += smjx[j];
          y += smjy[j];
          break;
        }
      }
      sol = max(sol, max(abs(x), abs(y)));
      s2 = "";
      continue;
    }
    s2 += s[i];
  }

  cout << sol << endl;
  return 0;
}
