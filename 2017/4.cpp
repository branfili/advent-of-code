#include <iostream>
#include <set>
#include <algorithm>

using namespace std;
string in, s2;
bool flag;
int sol;

set<string> s;

int main (void){
  while (getline(cin, in)){
    s.clear();
    s2="";
    flag = true;

    in += ' ';
    for (int i=0;i<(int) in.size();i++){
      if (in[i] == ' '){
        sort(s2.begin(), s2.end());
        if (s.find(s2) != s.end()){
          flag = false;
          break;
        }
        s.insert(s2);
        s2 = "";
        continue;
      }
      s2 += in[i];
    }

    if (flag){
      sol++;
    }
  }

  cout << sol << endl;
  return 0;
}
