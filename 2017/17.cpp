#include <cstdio>
#include <list>

using namespace std;
int x;

list <int> l;

int main(void){
  scanf("%d", &x);

  auto it = l.begin();
  l.insert(it, 0);

  for (int i = 1; i <= 5e7; i++){
    for (int j = 0; j < x; j++){
      it++;
      if (it == l.end()){
        it = l.begin();
      }
    }
    it++;
    l.insert(it, i);
    it--;
  }

  it = l.begin();
  it++;

  printf("%d\n", *it);
  return 0;
}
