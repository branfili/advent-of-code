#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;
const int LEN = 256;
string s;
string s2;
int cur, sk;
int a[LEN];

vector<int> v;

int uintaj(string s){
  int z = 0;
  for (int i = 0; i < (int) s.size(); i++){
    z *= 10;
    z += (s[i] - '0');
  }
  return z;
}

char hx(int x){
  if (x < 10){
    return x + '0'; 
  }
  return (x - 10) + 'a';
}

string hex(int x){
  if (x == 0){
    return "00";
  }
  string s = "";
  for (int i = 0; i < 2; i++){
    s += hx(x % 16);
    x /= 16;
  }
  reverse(s.begin(), s.end());
  return s;
}

void simulate(int x){
  for (int l = cur, r = cur + x - 1; l < r; l++, r--){
    a[l % LEN] ^= a[r % LEN];
    a[r % LEN] ^= a[l % LEN];
    a[l % LEN] ^= a[r % LEN];
  }
  cur += x + sk;
  cur %= LEN;
  sk++;
}

int main (void){
  for (int i = 0; i < LEN; i++){
    a[i] = i;
  }

  getline(cin, s);
  for (int i = 0; i < (int) s.size(); i++){
    v.push_back(s[i]);
  }
  v.push_back(17);
  v.push_back(31);
  v.push_back(73);
  v.push_back(47);
  v.push_back(23);

  for (int i = 0; i < 64; i++){
    for (int j = 0; j < (int) v.size(); j++){
      simulate(v[j]);
    }
  }

  for (int i = 0; i < 16; i++){
    int z = 0;
    for (int j = 0; j < 16; j++){
      z ^= a[i * 16 + j];
    }
    s2 += hex(z);
  }
  cout << s2 << endl;

  return 0;
}
