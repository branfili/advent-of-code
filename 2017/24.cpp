#include <iostream>
#include <vector>
#include <tuple>
#include <algorithm>

using namespace std;
string s;
int a, b;
int mks, mkslen;

vector<tuple<int, int, int>> v;
vector<bool> bio;

int uintaj(string s){
  int x = 0;
  for (int i = 0; i < (int) s.size(); i++){
    x *= 10;
    x += (s[i] - '0');
  }
  return x;
}

void dfs(int x, int s, int len){
  int y = (int) (lower_bound(v.begin(), v.end(), make_tuple(x, -1, -1)) - v.begin());

  for (; get<0>(v[y]) == x; y++){
    if (!bio[get<2>(v[y])]){
      bio[get<2>(v[y])] = true;
      dfs(get<1>(v[y]), s + get<0>(v[y]) + get<1>(v[y]), len + 1);
      bio[get<2>(v[y])] = false;
    }
  }
  if (len > mkslen){
    mks = s;
    mkslen = len;
  }
  else if (len == mkslen){
    mks = max(mks, s);
  }

  return ;
}

int main (void){
  for (int iter = 0; getline(cin, s); iter++){
    int i;
    for (i = 0; s[i] != '/'; i++);
    
    a = uintaj(s.substr(0, i));
    b = uintaj(s.substr(i + 1));
    
    v.push_back(make_tuple(a, b, iter));
    v.push_back(make_tuple(b, a, iter));
    
    bio.push_back(false);
  }
  sort(v.begin(), v.end());

  dfs(0, 0, 0);
  cout << mks << endl;
  return 0;
}
