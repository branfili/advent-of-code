#include <iostream>

using namespace std;
const int MAXN = 1e7;
const int MAXS = 1e4;

string s;
int lenx, leny;
int curx, cury;
int smj;
int sol;

short a[MAXS][MAXS];

int smjx[] = {-1, 0, 1, 0};
int smjy[] = {0, 1, 0, -1};

int main (void){
  curx = MAXS / 2;
  cury = MAXS / 2;

  for (lenx = 0; getline(cin, s); lenx++){
    for (leny = 0; leny < (int) s.size(); leny++){
      a[curx + lenx][cury + leny] = (s[leny] == '#' ? 2 : 0);
    }
  }

  curx += lenx / 2;
  cury += leny / 2;
  smj = 0;

  for (int i = 0; i < MAXN; i++){
    if (a[curx][cury] == 0){
      smj = (smj + 3) % 4;
    }
    else if (a[curx][cury] == 1){
      sol++;
    }
    else if (a[curx][cury] == 2){
      smj = (smj + 1) % 4;
    }
    else{
      smj = (smj + 2) % 4;
    }

    a[curx][cury] = (a[curx][cury] + 1) % 4;
    curx += smjx[smj];
    cury += smjy[smj];
  }

  cout << sol << endl;
  return 0;
}
