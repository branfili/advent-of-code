#include <iostream>
#include <vector>

using namespace std;
long long a[8];
long long x, y;
int reg;
int sol;
string s;
char r;

vector<string> prog;

long long uintaj(string s){
  if (s.size() == 1 &&
      s[0] >= 'a' && s[0] <= 'h'){
    return a[s[0] - 'a'];
  }

  bool neg = false;
  if (s[0] == '-'){
    s = s.substr(1);
    neg = true;
  }

  long long x = 0LL;
  for (int i = 0; i < (int) s.size(); i++){
    x *= 10;
    x += (s[i] - '0');
  }

  if (neg){
    x *= -1;
  }
  return x;
}

bool isprime(int x){
  for (int i = 2; i * i <= x; i++){
    if (x % i == 0){
      return false;
    }
  }
  return true;
}

int main (void){
  while (getline(cin, s)){
    prog.push_back(s);
  }

  for (reg = 0; reg < (int) prog.size(); reg++){
    s = prog[reg].substr(0, 3);
    if (s == "jnz"){
      int i;
      for (i = 4; prog[reg][i] != ' '; i++);
      x = uintaj(prog[reg].substr(4, i - 4));
      y = uintaj(prog[reg].substr(i + 1));
      if (x != 0){
        reg += y - 1;
      }
    }
    else{
      r = prog[reg][4];
      x = uintaj(prog[reg].substr(6));
      if (s == "set"){
        a[r - 'a'] = x;
      }
      else if (s == "sub"){
        a[r - 'a'] -= x;
      }
      else if (s == "mul"){
        a[r - 'a'] *= x;
      }
      else{
        break;
      }
    }
  }

  for (int i = 109900; i <= 126900; i += 17){
    sol += !isprime(i);
  }
  cout << sol << endl;
  return 0;
}
