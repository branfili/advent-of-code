#include <iostream>
#include <vector>
#include <map>

using namespace std;
const int MAXN = 18;
string inp;
int x;
int sol;

vector<string> v;
map<string, string> m;

string flip(string s){
  string s2 = s;
  if (s.size() == 5){
    s2[0] = s[1];
    s2[1] = s[0];
    s2[2] = s[2];
    s2[3] = s[4];
    s2[4] = s[3];
  }
  else{
    s2[0] = s[2];
    s2[1] = s[1];
    s2[2] = s[0];
    s2[3] = s[3];
    s2[4] = s[6];
    s2[5] = s[5];
    s2[6] = s[4];
    s2[7] = s[7];
    s2[8] = s[10];
    s2[9] = s[9];
    s2[10] = s[8];
  }

  return s2;
}

string rotate(string s){
  string s2 = s;
  if (s.size() == 5){
    s2[0] = s[3];
    s2[1] = s[0];
    s2[2] = s[2];
    s2[3] = s[4];
    s2[4] = s[1];
  }
  else{
    s2[0] = s[8];
    s2[1] = s[4];
    s2[2] = s[0];
    s2[3] = s[3];
    s2[4] = s[9];
    s2[5] = s[5];
    s2[6] = s[1];
    s2[7] = s[7];
    s2[8] = s[10];
    s2[9] = s[6];
    s2[10] = s[2];
  }

  return s2;
}

string expand(string s){
  for (int i = 0; i < 4; i++){
    if (m.find(s) != m.end()){
      return m[s];
    }

    string s2 = flip(s);
    if (m.find(s2) != m.end()){
      return m[s2];
    }

    s = rotate(s);
  }
  return "";
}

int main (void){
  while (getline(cin, inp)){
    int i;
    for (i = 0; inp.substr(i, 4) != " => "; i++);
    m[inp.substr(0, i)] = inp.substr(i + 4);
  }

  v.push_back(".#.");
  v.push_back("..#");
  v.push_back("###");
  for (int iter = 0; iter < MAXN; iter++){
    int sz = v.size();
    if (sz % 2 == 0){
      x = 2;
    }
    else{
      x = 3;
    }

    vector<string> tmp;
    for (int i = 0; i < sz; i += x){
      for (int j = 0; j < sz; j += x){
        string s = "";
        for (int k = 0; k < x; k++){
          s += v[i + k].substr(j, x);
          if (k < x - 1){
            s += '/';
          }
        }
        tmp.push_back(expand(s));
      }
    }

    int nsz = sz / x * (x + 1);
    v.clear();
    for (int i = 0; i < nsz; i++){
      v.push_back("");
    }

    for (int i = 0; i < nsz / (x + 1); i ++){
      for (int j = 0; j <= x; j++){
        for (int k = 0; k < nsz / (x + 1); k++){
          v[i * (x + 1) + j] += tmp[i * nsz / (x + 1) + k].substr(j * (x + 2), x + 1);
        }
      }
    }
  }

  for (int i = 0; i < (int) v.size(); i++){
    for (int j = 0; j < (int) v[i].size(); j++){
      sol += (v[i][j] == '#');
    }
  }

  cout << sol << endl;
  return 0;
}
