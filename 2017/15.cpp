#include <cstdio>

using namespace std;
const int factA = 16807;
const int factB = 48271;
const int MAXN = 5e6;
const int MOD = (1LL << 31) - 1;
long long a, b;
int sol;

long long genA(long long x){
  do{
    x = (x * factA) % MOD;
  } while (x % 4 != 0);
  return x;
}

long long genB(long long x){
  do{
    x = (x * factB) % MOD;
  } while (x % 8 != 0);
  return x;
}

int main(void){
  scanf("%lld%lld", &a, &b);
  for (int i = 0; i < MAXN; i++){
    a = genA(a);
    b = genB(b);
    if (a % (1 << 16) == b % (1 << 16)){
      sol++;
    }
  }
  printf("%d\n", sol);
  return 0;
}
