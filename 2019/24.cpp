#include <iostream>

using namespace std;

const int MAXN = 5;
const int STEP = 200;
const int MAXD = 500;

char c;

bool map[MAXD][MAXN][MAXN];
bool map2[MAXD][MAXN][MAXN];

int dx[] = {-1, 0, 1, 0};
int dy[] = {0, 1, 0, -1};

int main (void){
  for (int i = 0; i < MAXN; i++){
    for (int j = 0; j < MAXN; j++){
      cin >> c;

      map[MAXD / 2][i][j] = (c == '#');
    }
  }

  int y, z;
  bool x;
  int nx, ny;
  for (int step = 0; step < STEP; step++){
    for (int d = 0; d < MAXD; d++){
      for (int i = 0; i < MAXN; i++){
        for (int j = 0; j < MAXN; j++){
          if (i == MAXN / 2 && j == MAXN / 2){
            continue;
          }

          x = map[d][i][j];
          z = 0;

          for (int k = 0; k < 4; k++){
            nx = i + dx[k];
            ny = j + dy[k];

            if (nx < 0 || nx >= MAXN ||
                ny < 0 || ny >= MAXN){
              if (d == 0){
                continue;
              }

              y = map[d - 1][MAXN / 2 + dx[k]][MAXN / 2 + dy[k]];
            }
            else if (nx == MAXN / 2 && ny == MAXN / 2){ 
              if (d == MAXD - 1){
                continue;
              }

              y = 0;
              for (int i = 0; i < MAXN; i++){
                if (k == 0){
                  y += map[d + 1][MAXN - 1][i];
                }
                else if (k == 1){
                  y += map[d + 1][i][0];
                }
                else if (k == 2){
                  y += map[d + 1][0][i];
                }
                else if (k == 3){
                  y += map[d + 1][i][MAXN - 1];
                }
              }
            }
            else{
              y = map[d][nx][ny];
            }

            z += y;
          }

          if (x == 1 && z != 1){
            x = 0;
          }
          else if (x == 0 && (z == 1 || z == 2)){
            x = 1;
          }

          map2[d][i][j] = x;
        }
      }
    }

    for (int d = 0; d < MAXD; d++){
      for (int i = 0; i < MAXN; i++){
        for (int j = 0; j < MAXN; j++){
          map[d][i][j] = map2[d][i][j];
        }
      }
    }
  }

  z = 0;
  for (int d = 0; d < MAXD; d++){
    for (int i = 0; i < MAXN; i++){
      for (int j = 0; j < MAXN; j++){
        if (i == MAXN / 2 && j == MAXN / 2){
          continue;
        }

        z += map[d][i][j];
      }
    }
  }

  cout << z << endl;
  return 0;
}
