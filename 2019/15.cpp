#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <stack>
#include <utility>
#include <queue>

using namespace std;

const int MAXN = 100;
const int MEMORY = 20000;

int dx[] = {-1, 1, 0, 0};
int dy[] = {0, 0, -1, 1};

string s, s2;

vector <long long> v;
vector <long long> output;

vector <long long> state;

int pnt, rel;

int map[MAXN][MAXN];
int dist[MAXN][MAXN];

int x, y;

stack<pair<int, int> > st;

queue<pair<pair<int, int>, int> > q;

long long uintaj(string s){
  bool neg = false;
  
  if (s[0] == '-'){
    neg = true;
    s = s.substr(1);
  }

  long long z = 0;
  for (int i = 0; i < (int)s.size(); i++){
    z *= 10;
    z += (s[i] - '0');
  }

  z *= (neg ? -1 : 1);

  return z;
}

vector<long long> evaluate(vector<long long>& v, vector<long long>& input, int stp, int str){
  if (stp == -1){
    for (int i = 0; i < (int) v.size(); i++){
      state.push_back(v[i]);
    }
    stp = 0;
  }
  
  vector<long long> output;
  output.clear();

  int ilen = v.size();

  while (state.size() < MEMORY){
    state.push_back(0);
  }

  int op;
  long long a, b, c;
  int inind = 0;
  int p = stp;
  int r = str;

  while (p < ilen){
    op = state[p];

    int om = op % 100;
    int am = (op % 1000) / 100;
    int bm = (op % 10000) / 1000;
    int cm = op / 10000;

    if (om == 1){
      if (am == 0){
        a = state[state[p + 1]];
      }
      else if (am == 1){
        a = state[p + 1];
      }
      else{
        a = state[r + state[p + 1]];
      }

      if (bm == 0){
        b = state[state[p + 2]];
      }
      else if (bm == 1){
        b = state[p + 2];
      }
      else{
        b = state[r + state[p + 2]];
      }

      c = a + b;

      if (cm == 0){
        state[state[p + 3]] = c;
      }
      else if (cm == 1){
        state[p + 3] = c;
      }
      else{
        state[r + state[p + 3]] = c;
      }

      p += 4;
    }
    else if (om == 2){
      if (am == 0){
        a = state[state[p + 1]];
      }
      else if (am == 1){
        a = state[p + 1];
      }
      else{
        a = state[r + state[p + 1]];
      }

      if (bm == 0){
        b = state[state[p + 2]];
      }
      else if (bm == 1){
        b = state[p + 2];
      }
      else{
        b = state[r + state[p + 2]];
      }

      c = a * b;

      if (cm == 0){
        state[state[p + 3]] = c;
      }
      else if (cm == 1){
        state[p + 3] = c;
      }
      else{
        state[r + state[p + 3]] = c;
      }

      p += 4;
    }
    else if (om == 3){
      if (inind == (int)input.size()){
        output.push_back(r);
        output.push_back(-p);
        return output;
      }

      a = input[inind++];
      
      if (am == 0){
        state[state[p + 1]] = a;
      }
      else if (am == 1){
        state[p + 1] = a;
      }
      else{
        state[r + state[p + 1]] = a;
      }

      p += 2;
    }
    else if (op % 100 == 4){
      if (am == 0){
        a = state[state[p + 1]];
      }
      else if (am == 1){
        a = state[p + 1];
      }
      else{
        a = state[r + state[p + 1]];
      }

      output.push_back(a);

      p += 2;
    }
    else if (om == 5){
      if (am == 0){
        a = state[state[p + 1]];
      }
      else if (am == 1){
        a = state[p + 1];
      }
      else{
        a = state[r + state[p + 1]];
      }

      if (bm == 0){
        b = state[state[p + 2]];
      }
      else if (bm == 1){
        b = state[p + 2];
      }
      else{
        b = state[r + state[p + 2]];
      }

      if (a != 0){
        p = b;
      }
      else{
        p += 3;
      }
    }
    else if (om == 6){
      if (am == 0){
        a = state[state[p + 1]];
      }
      else if (am == 1){
        a = state[p + 1];
      }
      else{
        a = state[r + state[p + 1]];
      }

      if (bm == 0){
        b = state[state[p + 2]];
      }
      else if (bm == 1){
        b = state[p + 2];
      }
      else{
        b = state[r + state[p + 2]];
      }

      if (a == 0){
        p = b;
      }
      else{
        p += 3;
      }
    }
    else if (om == 7){
      if (am == 0){
        a = state[state[p + 1]];
      }
      else if (am == 1){
        a = state[p + 1];
      }
      else{
        a = state[r + state[p + 1]];
      }

      if (bm == 0){
        b = state[state[p + 2]];
      }
      else if (bm == 1){
        b = state[p + 2];
      }
      else{
        b = state[r + state[p + 2]];
      }

      c = (a < b);
      
      if (cm == 0){
        state[state[p + 3]] = c;
      }
      else if (cm == 1){
        state[p + 3] = c;
      }
      else{
        state[r + state[p + 3]] = c;
      }

      p += 4;
    }
    else if (om == 8){
      if (am == 0){
        a = state[state[p + 1]];
      }
      else if (am == 1){
        a = state[p + 1];
      }
      else{
        a = state[r + state[p + 1]];
      }

      if (bm == 0){
        b = state[state[p + 2]];
      }
      else if (bm == 1){
        b = state[p + 2];
      }
      else{
        b = state[r + state[p + 2]];
      }

      c = (a == b);

      if (cm == 0){
        state[state[p + 3]] = c;
      }
      else if (cm == 1){
        state[p + 3] = c;
      }
      else{
        state[r + state[p + 3]] = c;
      }

      p += 4;
    }
    else if (om == 9){
      if (am == 0){
        a = state[state[p + 1]];
      }
      else if (am == 1){
        a = state[p + 1];
      }
      else{
        a = state[r + state[p + 1]];
      }

      r += a;
      p += 2;
    }
    else if (om == 99){
      break;
    }
  }

  output.push_back(0);
  output.push_back(-10000000);
  return output;
}

int sgn(int x){
  return ((x >= 0) - (x <= 0));
}

int main (void){
  cin >> s;

  s += ',';
  s2 = "";

  for (int i = 0; i < (int) s.size(); i++){
    if (s[i] == ','){
      v.push_back(uintaj(s2));
      s2 = "";
    }
    else{
      s2 += s[i];
    }
  }

  vector<long long> input;
  input.clear();

  pnt = -1;
  rel = 0;

  x = 50;
  y = 50;

  map[x][y] = 1;
  dist[x][y] = 0;

  for (int i = 0; i < 4; i++){
    st.push(make_pair(x + dx[i], y + dy[i]));
  }

  int dir;
  bool e;

  while (!st.empty()){
    pair<int, int> tmp = st.top();
    st.pop();

    e = true;
    while (e){
      for (dir = 0; dir < 4; dir++){
        if (x + dx[dir] == tmp.first &&
            y + dy[dir] == tmp.second){
          e = false;
          break;
        }
      }

      if (e){
        for (dir = 0; dir < 4; dir++){
          if (map[x + dx[dir]][y + dy[dir]] == 1){
            break;
          }
        }
        
        map[x][y] = 3;
        x += dx[dir];
        y += dy[dir];
      }

      input.clear();
      input.push_back(dir + 1);
      output = evaluate(v, input, pnt, rel);

      pnt = -output.back();
      output.pop_back();

      rel = output.back();
      output.pop_back();
    }

    if (output.back() == 0){
      map[x + dx[dir]][y + dy[dir]] = 2;
    }
    else{
      dist[x + dx[dir]][y + dy[dir]] = dist[x][y] + 1;

      x += dx[dir];
      y += dy[dir];

      map[x][y] = 1;

      int nx, ny;
      for (int i = 0; i < 4; i++){
        nx = x + dx[i];
        ny = y + dy[i];

        if (map[nx][ny] == 0){
          st.push(make_pair(nx, ny));
        }
      }
    }

    output.clear();
  }

  for (int i = 0; i < MAXN; i++){
    for (int j = 0; j < MAXN; j++){
      if (map[i][j] == 3){
        map[i][j] = 1;
      }
    }
  }

  q.push(make_pair(make_pair(32, 68), 0));
  int maksi = -10;
  int nx, ny, d;

  while (!q.empty()){
    pair<pair<int, int>, int> tmp = q.front();
    q.pop();

    x = tmp.first.first;
    y = tmp.first.second;
    d = tmp.second;

    maksi = max(maksi, d);
    map[x][y] = 3;

    for (int i = 0; i < 4; i++){
      nx = x + dx[i];
      ny = y + dy[i];

      if (map[nx][ny] == 1){
        q.push(make_pair(make_pair(nx, ny), d + 1));
      }
    }
  }

  cout << maksi << endl;
  return 0;
}
