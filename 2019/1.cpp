#include <iostream>

using namespace std;
int x;
int sol;

int calc(int x){
  int z = (x / 3) - 2;
  
  if (z < 0){
    return 0;
  }

  return z + calc(z);
}

int main (void){
  while (cin >> x){
    sol += calc(x);
  }
  cout << sol << endl;
}
