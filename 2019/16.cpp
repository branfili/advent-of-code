#include <iostream>
#include <string>
#include <cmath>
#include <vector>

using namespace std;

const int MAXN = 100;
const int MAXM = 10000;
const int START = 5975803;

string s;

vector <int> v;
vector <int> v2;
vector <int> sum;

int main(void){
  cin >> s;
  for (int i = 0; i < MAXM; i++){
    for (int i = 0; i < (int) s.size(); i++){
      v.push_back(s[i] - '0');
    }
  }

  int sol, dir;
  for (int step = 0; step < MAXN; step++){
    cout << step << endl;

    sum.clear();
    sol = 0;
    for (int i = 0; i < (int) v.size(); i++){
      sol += v[i];
      sum.push_back(sol);
    }

    v2.clear();
    for (int i = 0; i < (int) v.size(); i++){
      sol = 0;
      dir = 1;
      for (int j = i; j < (int) v.size(); j += 2 * (i + 1), dir *= -1){
        sol += dir * (sum[min(j + i, (int)v.size() - 1)] - sum[j - 1]);
      }

      v2.push_back((sol < 0 ? -sol : sol) % 10);
    }

    v.clear();
    for (int i = 0; i < (int) v2.size(); i++){
      v.push_back(v2[i]);
    }
  }

  for (int i = START; i < START + 8; i++){
    cout << v[i];
  }
  cout << endl;
}
