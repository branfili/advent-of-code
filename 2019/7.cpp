#include <iostream>
#include <vector>
#include <string>
#include <algorithm>

using namespace std;

string s, s2;

vector <long long> v;

long long maks;
int phases[5] = {5, 6, 7, 8, 9};

int uintaj(string s){
  bool neg = false;
  
  if (s[0] == '-'){
    neg = true;
    s = s.substr(1);
  }

  int z = 0;
  for (int i = 0; i < (int)s.size(); i++){
    z *= 10;
    z += (s[i] - '0');
  }

  z *= (neg ? -1 : 1);

  return z;
}

long long evaluate(vector<long long>& v2, vector<long long>& input){
  vector<long long> v(v2);

  int op, a, b, c;
  int inind = 0;
  int p = 0;

  while (true){
    op = v[p];

    if (op % 100 == 1){
      if ((op % 1000) / 100 == 0){
        a = v[v[p + 1]];
      }
      else{
        a = v[p + 1];
      }

      if ((op % 10000) / 1000 == 0){
        b = v[v[p + 2]];
      }
      else{
        b = v[p + 2];
      }

      c = a + b;

      if (op / 10000 == 0){
        v[v[p + 3]] = c;
      }
      else{
        v[p + 3] = c;
      }

      p += 4;
    }
    else if (op % 100 == 2){
      if ((op % 1000) / 100 == 0){
        a = v[v[p + 1]];
      }
      else{
        a = v[p + 1];
      }

      if ((op % 10000) / 1000 == 0){
        b = v[v[p + 2]];
      }
      else{
        b = v[p + 2];
      }

      c = a * b;

      if (op / 10000 == 0){
        v[v[p + 3]] = c;
      }
      else{
        v[p + 3] = c;
      }

      p += 4;
    }
    else if (op % 100 == 3){
      a = input[inind++];

      if ((op % 1000) / 100 == 0){
        v[v[p + 1]] = a;
      }
      else{
        v[p + 1] = a;
      }

      p += 2;
    }
    else if (op % 100 == 4){
      if ((op % 1000) / 100 == 0){
        a = v[v[p + 1]];
      }
      else{
        a = v[p + 1];
      }

      if (inind == (int) input.size()){
        return a;
      }

      p += 2;
    }
    else if (op % 100 == 5){
      if ((op % 1000) / 100 == 0){
        a = v[v[p + 1]];
      }
      else{
        a = v[p + 1];
      }

      if ((op % 10000) / 1000 == 0){
        b = v[v[p + 2]];
      }
      else{
        b = v[p + 2];
      }

      if (a != 0){
        p = b;
      }
      else{
        p += 3;
      }
    }
    else if (op % 100 == 6){
      if ((op % 1000) / 100 == 0){
        a = v[v[p + 1]];
      }
      else{
        a = v[p + 1];
      }

      if ((op % 10000) / 1000 == 0){
        b = v[v[p + 2]];
      }
      else{
        b = v[p + 2];
      }

      if (a == 0){
        p = b;
      }
      else{
        p += 3;
      }
    }
    else if (op % 100 == 7){
      if ((op % 1000) / 100 == 0){
        a = v[v[p + 1]];
      }
      else{
        a = v[p + 1];
      }

      if ((op % 10000) / 1000 == 0){
        b = v[v[p + 2]];
      }
      else{
        b = v[p + 2];
      }

      c = (a < b);

      if (op / 10000 == 0){
        v[v[p + 3]] = c;
      }
      else{
        v[p + 3] = c;
      }

      p += 4;
    }
    else if (op % 100 == 8){
      if ((op % 1000) / 100 == 0){
        a = v[v[p + 1]];
      }
      else{
        a = v[p + 1];
      }

      if ((op % 10000) / 1000 == 0){
        b = v[v[p + 2]];
      }
      else{
        b = v[p + 2];
      }

      c = (a == b);

      if (op / 10000 == 0){
        v[v[p + 3]] = c;
      }
      else{
        v[p + 3] = c;
      }

      p += 4;
    }
    else if (op % 100 == 99){
      break;
    }
  }

  return -1;
}

int main (void){
  cin >> s;

  s += ',';
  s2 = "";

  for (int i = 0; i < (int) s.size(); i++){
    if (s[i] == ','){
      v.push_back(uintaj(s2));
      s2 = "";
    }
    else{
      s2 += s[i];
    }
  }

  maks = 0;
  vector<long long> input[5];

  do{

    for (int i = 0; i < 5; i++){
      input[i].clear();
      input[i].push_back(phases[i]);
    }

    input[0].push_back(0);

    for (int i = 0; input[0].back() != -1; i++){
      input[(i + 1) % 5].push_back(evaluate(v, input[i % 5]));
      
      if (i % 5 == 0){
        maks = max(maks, input[0].back());
      }
    }

  } while (next_permutation(phases, phases + 5));

  cout << maks << endl;
  return 0;
}
