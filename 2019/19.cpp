#include <iostream>
#include <vector>
#include <string>
#include <algorithm>

using namespace std;

const int MAXN = 1100;
const int MEMORY = 20000;
const int SEARCH = 100;

string s, s2;

vector <long long> v;
vector <long long> input;
vector <long long> output;

vector <long long> state;

int pnt, rel;

int pulled[MAXN][MAXN];

long long uintaj(string s){
  bool neg = false;
  
  if (s[0] == '-'){
    neg = true;
    s = s.substr(1);
  }

  long long z = 0;
  for (int i = 0; i < (int)s.size(); i++){
    z *= 10;
    z += (s[i] - '0');
  }

  z *= (neg ? -1 : 1);

  return z;
}

vector<long long> evaluate(vector<long long>& v, vector<long long>& input, int stp, int str){
  if (stp == -1){
    state.clear();
    for (int i = 0; i < (int) v.size(); i++){
      state.push_back(v[i]);
    }
    stp = 0;
  }
  
  vector<long long> output;
  output.clear();

  int ilen = v.size();

  while (state.size() < MEMORY){
    state.push_back(0);
  }

  int op;
  long long a, b, c;
  int inind = 0;
  int p = stp;
  int r = str;

  while (p < ilen){
    op = state[p];

    int om = op % 100;
    int am = (op % 1000) / 100;
    int bm = (op % 10000) / 1000;
    int cm = op / 10000;

    if (om == 1){
      if (am == 0){
        a = state[state[p + 1]];
      }
      else if (am == 1){
        a = state[p + 1];
      }
      else{
        a = state[r + state[p + 1]];
      }

      if (bm == 0){
        b = state[state[p + 2]];
      }
      else if (bm == 1){
        b = state[p + 2];
      }
      else{
        b = state[r + state[p + 2]];
      }

      c = a + b;

      if (cm == 0){
        state[state[p + 3]] = c;
      }
      else if (cm == 1){
        state[p + 3] = c;
      }
      else{
        state[r + state[p + 3]] = c;
      }

      p += 4;
    }
    else if (om == 2){
      if (am == 0){
        a = state[state[p + 1]];
      }
      else if (am == 1){
        a = state[p + 1];
      }
      else{
        a = state[r + state[p + 1]];
      }

      if (bm == 0){
        b = state[state[p + 2]];
      }
      else if (bm == 1){
        b = state[p + 2];
      }
      else{
        b = state[r + state[p + 2]];
      }

      c = a * b;

      if (cm == 0){
        state[state[p + 3]] = c;
      }
      else if (cm == 1){
        state[p + 3] = c;
      }
      else{
        state[r + state[p + 3]] = c;
      }

      p += 4;
    }
    else if (om == 3){
      if (inind == (int)input.size()){
        output.push_back(r);
        output.push_back(-p);
        return output;
      }

      a = input[inind++];
      
      if (am == 0){
        state[state[p + 1]] = a;
      }
      else if (am == 1){
        state[p + 1] = a;
      }
      else{
        state[r + state[p + 1]] = a;
      }

      p += 2;
    }
    else if (op % 100 == 4){
      if (am == 0){
        a = state[state[p + 1]];
      }
      else if (am == 1){
        a = state[p + 1];
      }
      else{
        a = state[r + state[p + 1]];
      }

      output.push_back(a);

      p += 2;
    }
    else if (om == 5){
      if (am == 0){
        a = state[state[p + 1]];
      }
      else if (am == 1){
        a = state[p + 1];
      }
      else{
        a = state[r + state[p + 1]];
      }

      if (bm == 0){
        b = state[state[p + 2]];
      }
      else if (bm == 1){
        b = state[p + 2];
      }
      else{
        b = state[r + state[p + 2]];
      }

      if (a != 0){
        p = b;
      }
      else{
        p += 3;
      }
    }
    else if (om == 6){
      if (am == 0){
        a = state[state[p + 1]];
      }
      else if (am == 1){
        a = state[p + 1];
      }
      else{
        a = state[r + state[p + 1]];
      }

      if (bm == 0){
        b = state[state[p + 2]];
      }
      else if (bm == 1){
        b = state[p + 2];
      }
      else{
        b = state[r + state[p + 2]];
      }

      if (a == 0){
        p = b;
      }
      else{
        p += 3;
      }
    }
    else if (om == 7){
      if (am == 0){
        a = state[state[p + 1]];
      }
      else if (am == 1){
        a = state[p + 1];
      }
      else{
        a = state[r + state[p + 1]];
      }

      if (bm == 0){
        b = state[state[p + 2]];
      }
      else if (bm == 1){
        b = state[p + 2];
      }
      else{
        b = state[r + state[p + 2]];
      }

      c = (a < b);
      
      if (cm == 0){
        state[state[p + 3]] = c;
      }
      else if (cm == 1){
        state[p + 3] = c;
      }
      else{
        state[r + state[p + 3]] = c;
      }

      p += 4;
    }
    else if (om == 8){
      if (am == 0){
        a = state[state[p + 1]];
      }
      else if (am == 1){
        a = state[p + 1];
      }
      else{
        a = state[r + state[p + 1]];
      }

      if (bm == 0){
        b = state[state[p + 2]];
      }
      else if (bm == 1){
        b = state[p + 2];
      }
      else{
        b = state[r + state[p + 2]];
      }

      c = (a == b);

      if (cm == 0){
        state[state[p + 3]] = c;
      }
      else if (cm == 1){
        state[p + 3] = c;
      }
      else{
        state[r + state[p + 3]] = c;
      }

      p += 4;
    }
    else if (om == 9){
      if (am == 0){
        a = state[state[p + 1]];
      }
      else if (am == 1){
        a = state[p + 1];
      }
      else{
        a = state[r + state[p + 1]];
      }

      r += a;
      p += 2;
    }
    else if (om == 99){
      break;
    }
  }

  output.push_back(0);
  output.push_back(-10000000);
  return output;
}

bool check(int x, int y){
  input.clear();

  input.push_back(x);
  input.push_back(y);

  output.clear();
  output = evaluate(v, input, -1, 0);

  return output[0];
}

int main (void){
  cin >> s;

  s += ',';
  s2 = "";

  for (int i = 0; i < (int) s.size(); i++){
    if (s[i] == ','){
      v.push_back(uintaj(s2));
      s2 = "";
    }
    else{
      s2 += s[i];
    }
  }

  int start = 0;

  for (int i = 0; i < MAXN; i++){
    if (i == 1 || i == 3){
      continue;
    }
    
    for (int j = start; !check(j, i); j++, start++);

    for (int j = start; j < MAXN && check(j, i); j++){
      pulled[i][j] = true;
    }
  }

  for (int i = 0; i < MAXN; i++){
    for (int j = 0; j < MAXN; j++){
      if (i == 0 && j != 0){
        pulled[i][j] += pulled[i][j - 1];
      }
      else if (i != 0 && j == 0){
        pulled[i][j] += pulled[i - 1][j];
      }
      else if (i != 0 && j != 0){
        pulled[i][j] += pulled[i - 1][j] + pulled[i][j - 1] - pulled[i - 1][j - 1];
      }
    }
  }

  bool e = false;
  for (int i = SEARCH; i < MAXN; i++){
    for (int j = SEARCH; j < MAXN; j++){
      int area = pulled[i][j] - pulled[i - SEARCH][j] - pulled[i][j - SEARCH] + pulled[i - SEARCH][j - SEARCH];

      if (area == SEARCH * SEARCH){
        cout << (j - SEARCH + 1) * 10000 + (i - SEARCH + 1) << endl;
        e = true;
        break;
      }
    }

    if (e){
      break;
    }
  }

  return 0;
}
