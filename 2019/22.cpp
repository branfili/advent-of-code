#include <iostream>
#include <string>
#include <algorithm>
#include <vector>
#include "./bigint/bigInt.h"

using namespace std;

struct Shuffle{
  bool cut;
  long long v;
  long long inv;
};

const long long MAXN = 119315717514047L;
const long long REP = 101741582076661L;

const BigInt::Rossi ONE(1L);
const BigInt::Rossi TWO(2L);

string s, s2;

vector<Shuffle> v;

int uintaj(string s){
  bool neg = false;
  if (s[0] == '-'){
    neg = true;
    s = s.substr(1);
  }

  int res = 0;
  for (int i = 0; i < (int) s.size(); i++){
    res *= 10;
    res += (s[i] - '0');
  }

  return (neg ? -res : res);
}

long long mod_mult(long long x, long long y){
  BigInt::Rossi tmp1(x);
  BigInt::Rossi tmp2(y);
  BigInt::Rossi tmp3(MAXN);
  tmp1 = tmp1 * tmp2;
  tmp1 = tmp1 % tmp3;

  return tmp1.toUnit();
}

BigInt::Rossi pot(BigInt::Rossi x, BigInt::Rossi y, BigInt::Rossi mod){
  if (y == ONE){
    return x;
  }
  
  bool odd = false;
  
  if (y % TWO == ONE){
    y = y - ONE;
    odd = true;
  }

  BigInt::Rossi rek = pot(x, y / TWO, mod);

  rek = rek * rek;
  rek = rek % mod;

  if (odd){
    rek = rek * x;
    rek = rek % mod;
  }

  return rek;
}

long long mod_inv(long long x){
  BigInt::Rossi tmp1(x);
  BigInt::Rossi tmp2(MAXN - 2);
  BigInt::Rossi tmp3(MAXN);

  tmp1 = pot(tmp1, tmp2, tmp3);

  return tmp1.toUnit();
}

long long rev_shuffle(long long pos){
  for (int i = 0; i < (int) v.size(); i++){
    if (v[i].cut){
      if (pos >= MAXN - v[i].v){
        pos -= (MAXN - v[i].v);
      }
      else{
        pos += v[i].v;
      }
    }
    else{
      if (v[i].v == -1){
        pos = MAXN - 1 - pos;
      }
      else{
        pos = mod_mult(pos, v[i].inv);     
      }
    }
  }

  return pos;
}

int main (void){
  while (getline(cin, s)){
    s2 = "";
    for (int i = 0; i < (int) s.size(); i++){
      if (s[i] == ' '){
        s2 = "";
      }
      else{
        s2 += s[i];
      }
    }

    Shuffle tmp;
    tmp.cut = (s[0] == 'c');
    tmp.v = uintaj(s2);

    if (s2 == "stack"){
      tmp.v = -1;
    }

    if (tmp.cut && tmp.v < 0){
      tmp.v += MAXN;
    }

    if (!tmp.cut && s2 != "stack"){
      tmp.inv = mod_inv(tmp.v);
    }

    v.push_back(tmp);
  }

  reverse(v.begin(), v.end());

  long long pos = 2020;
  long long pos2 = rev_shuffle(pos);
  long long pos3 = rev_shuffle(pos2);

  long long a = mod_mult(pos2 - pos3 + MAXN, mod_inv(pos - pos2 + MAXN));
  long long b = (pos2 - mod_mult(pos, a) + MAXN) % MAXN;

  BigInt::Rossi tmp(a);
  BigInt::Rossi tmp2(REP);
  BigInt::Rossi mod(MAXN);
  BigInt::Rossi tmp3 = pot(tmp, tmp2, mod);
  long long an = tmp3.toUnit();

  cout << (mod_mult(pos, an) + mod_mult(mod_mult(b, an - 1), mod_inv(a - 1))) % MAXN << endl;
  return 0;
}
