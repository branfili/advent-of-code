#include <iostream>
#include <string>
#include <vector>

using namespace std;

vector <int> v;

int uintaj(string s){
  int z = 0;
  for (int i = 0; i < (int) s.size(); i++){
    z *= 10;
    z += (s[i] - '0');
  }
  return z;
}

int sol(int x, int y){
  vector <int> v2;
  for (int i = 0; i < (int) v.size(); i++){
    v2.push_back(v[i]);
  }

  int ind = 0;

  v2[1] = x;
  v2[2] = y;

  while (true){
    if (v2[ind] == 99){
      break;
    }
    else if (v[ind] == 1){
      v2[v2[ind + 3]] = v2[v2[ind + 1]] + v2[v2[ind + 2]];
    }
    else if (v[ind] == 2){
      v2[v2[ind + 3]] = v2[v2[ind + 1]] * v2[v2[ind + 2]];
    }

    ind += 4;
  }

  return v2[0];
}

int main (void){
  string s, s2;

  cin >> s;

  s += ',';
  s2 = "";

  for (int i = 0; i < (int) s.size(); i++){
    if (s[i] == ','){
      v.push_back(uintaj(s2));
      s2 = "";
    }
    else{
      s2 += s[i];
    }
  }

  int final_sol = 19690720;

  for (int x = 0; x < 100; x++){
    for (int y = 0; y < 100; y++){
      if (sol(x, y) == final_sol){
        cout << x << ' ' << y << endl;
      }
    }
  }

  return 0;
}
