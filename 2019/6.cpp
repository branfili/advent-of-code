#include <iostream>
#include <vector>
#include <string>

using namespace std;

string s;
int sol;

vector<string> names;

vector<vector<int> > vc;
vector<int> p;

int find(string s){
  int z = -1;

  for (int i = 0; i < (int)names.size(); i++){
    if (names[i] == s){
      z = i;
      break;
    }
  }

  return z;
}

void child(string ps, string cs){
  int x = find(ps);
  int y = find(cs);

  if (x == -1){
    vector<int> v;

    names.push_back(ps);
    vc.push_back(v);
    p.push_back(-1);

    x = names.size() - 1;
  }

  if (y == -1){
    vector<int> v;

    names.push_back(cs);
    vc.push_back(v);
    p.push_back(0);

    y = names.size() - 1;
  }

  p[y] = x;
  vc[x].push_back(y);
}

vector<int> prek(int x){
  if (x == -1){
    vector<int> tmp;
    tmp.clear();
    return tmp;
  }

  vector<int> tmp = prek(p[x]);
  tmp.push_back(x);
  return tmp;
}

void count(int x, int d){
  sol += d;
  
  for (int i = 0; i < (int)vc[x].size(); i++){
    count(vc[x][i], d + 1);
  }
}

int main (void){
  while (cin >> s){
    child (s.substr(0, 3), s.substr(4));
  }

  int st;

  for (int i = 0; i < (int)names.size(); i++){
    if (p[i] == -1){
      st = i;
      break;
    }
  }

  count(st, 0);

  cout << sol << endl;
  
  vector<int> yp = prek(p[find("YOU")]);
  vector<int> sp = prek(p[find("SAN")]);

  int cl;
  for (cl = 0; yp[cl] == sp[cl]; cl++);

  cout << yp.size() + sp.size() - 2 * cl << endl;

  return 0;
}
