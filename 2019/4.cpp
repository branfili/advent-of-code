#include <iostream>
#include <string>

using namespace std;

int a, b;
int sol;

bool check(int x){
  string s;
  while (x > 0){
    s += ((x % 10) + '0');
    x /= 10;
  }

  bool e = false;
  bool f = true;
  bool g = false;

  for (int i = 1; i < (int) s.size(); i++){
    e |= (s[i] == s[i - 1]);
    f &= (s[i] <= s[i - 1]);

    g |= (s[i] == s[i - 1]) &&
         (i > 0 ? s[i - 1] != s[i - 2] : true) &&
         (i < (int) s.size() + 1 ? s[i + 1] != s[i] : true);
  }

  return (e && f && g);
}

int main (void){
  cin >> a >> b;

  for (int i = a; i <= b; i++){
    sol += check(i);
  }

  cout << sol << endl;
  return 0;
}
