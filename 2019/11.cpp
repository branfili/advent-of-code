#include <iostream>
#include <vector>
#include <string>
#include <algorithm>

using namespace std;

const int MAXN = 1000;
const int MEMORY = 20000;

int dx[4] = {-1, 0, 1, 0};
int dy[4] = {0, 1, 0, -1};

string s, s2;

vector <long long> v;
vector <long long> output;

vector <long long> state;

int x, y;
int dir;

int pnt, rel;

bool m[MAXN][MAXN];

long long uintaj(string s){
  bool neg = false;
  
  if (s[0] == '-'){
    neg = true;
    s = s.substr(1);
  }

  long long z = 0;
  for (int i = 0; i < (int)s.size(); i++){
    z *= 10;
    z += (s[i] - '0');
  }

  z *= (neg ? -1 : 1);

  return z;
}

vector<long long> evaluate(vector<long long>& v, vector<long long>& input, int stp, int str){
  if (stp == -1){
    for (int i = 0; i < (int) v.size(); i++){
      state.push_back(v[i]);
    }
    stp = 0;
  }
  
  vector<long long> output;
  output.clear();

  int ilen = v.size();

  while (state.size() < MEMORY){
    state.push_back(0);
  }

  int op;
  long long a, b, c;
  int inind = 0;
  int p = stp;
  int r = str;

  while (p < ilen){
    op = state[p];

    int om = op % 100;
    int am = (op % 1000) / 100;
    int bm = (op % 10000) / 1000;
    int cm = op / 10000;

    if (om == 1){
      if (am == 0){
        a = state[state[p + 1]];
      }
      else if (am == 1){
        a = state[p + 1];
      }
      else{
        a = state[r + state[p + 1]];
      }

      if (bm == 0){
        b = state[state[p + 2]];
      }
      else if (bm == 1){
        b = state[p + 2];
      }
      else{
        b = state[r + state[p + 2]];
      }

      c = a + b;

      if (cm == 0){
        state[state[p + 3]] = c;
      }
      else if (cm == 1){
        state[p + 3] = c;
      }
      else{
        state[r + state[p + 3]] = c;
      }

      p += 4;
    }
    else if (om == 2){
      if (am == 0){
        a = state[state[p + 1]];
      }
      else if (am == 1){
        a = state[p + 1];
      }
      else{
        a = state[r + state[p + 1]];
      }

      if (bm == 0){
        b = state[state[p + 2]];
      }
      else if (bm == 1){
        b = state[p + 2];
      }
      else{
        b = state[r + state[p + 2]];
      }

      c = a * b;

      if (cm == 0){
        state[state[p + 3]] = c;
      }
      else if (cm == 1){
        state[p + 3] = c;
      }
      else{
        state[r + state[p + 3]] = c;
      }

      p += 4;
    }
    else if (om == 3){
      if (inind == (int)input.size()){
        output.push_back(r);
        output.push_back(-p);
        return output;
      }

      a = input[inind++];
      
      if (am == 0){
        state[state[p + 1]] = a;
      }
      else if (am == 1){
        state[p + 1] = a;
      }
      else{
        state[r + state[p + 1]] = a;
      }

      p += 2;
    }
    else if (op % 100 == 4){
      if (am == 0){
        a = state[state[p + 1]];
      }
      else if (am == 1){
        a = state[p + 1];
      }
      else{
        a = state[r + state[p + 1]];
      }

      output.push_back(a);

      p += 2;
    }
    else if (om == 5){
      if (am == 0){
        a = state[state[p + 1]];
      }
      else if (am == 1){
        a = state[p + 1];
      }
      else{
        a = state[r + state[p + 1]];
      }

      if (bm == 0){
        b = state[state[p + 2]];
      }
      else if (bm == 1){
        b = state[p + 2];
      }
      else{
        b = state[r + state[p + 2]];
      }

      if (a != 0){
        p = b;
      }
      else{
        p += 3;
      }
    }
    else if (om == 6){
      if (am == 0){
        a = state[state[p + 1]];
      }
      else if (am == 1){
        a = state[p + 1];
      }
      else{
        a = state[r + state[p + 1]];
      }

      if (bm == 0){
        b = state[state[p + 2]];
      }
      else if (bm == 1){
        b = state[p + 2];
      }
      else{
        b = state[r + state[p + 2]];
      }

      if (a == 0){
        p = b;
      }
      else{
        p += 3;
      }
    }
    else if (om == 7){
      if (am == 0){
        a = state[state[p + 1]];
      }
      else if (am == 1){
        a = state[p + 1];
      }
      else{
        a = state[r + state[p + 1]];
      }

      if (bm == 0){
        b = state[state[p + 2]];
      }
      else if (bm == 1){
        b = state[p + 2];
      }
      else{
        b = state[r + state[p + 2]];
      }

      c = (a < b);
      
      if (cm == 0){
        state[state[p + 3]] = c;
      }
      else if (cm == 1){
        state[p + 3] = c;
      }
      else{
        state[r + state[p + 3]] = c;
      }

      p += 4;
    }
    else if (om == 8){
      if (am == 0){
        a = state[state[p + 1]];
      }
      else if (am == 1){
        a = state[p + 1];
      }
      else{
        a = state[r + state[p + 1]];
      }

      if (bm == 0){
        b = state[state[p + 2]];
      }
      else if (bm == 1){
        b = state[p + 2];
      }
      else{
        b = state[r + state[p + 2]];
      }

      c = (a == b);

      if (cm == 0){
        state[state[p + 3]] = c;
      }
      else if (cm == 1){
        state[p + 3] = c;
      }
      else{
        state[r + state[p + 3]] = c;
      }

      p += 4;
    }
    else if (om == 9){
      if (am == 0){
        a = state[state[p + 1]];
      }
      else if (am == 1){
        a = state[p + 1];
      }
      else{
        a = state[r + state[p + 1]];
      }

      r += a;
      p += 2;
    }
    else if (om == 99){
      break;
    }
  }

  output.push_back(0);
  output.push_back(-10000000);
  return output;
}

int main (void){
  cin >> s;

  s += ',';
  s2 = "";

  for (int i = 0; i < (int) s.size(); i++){
    if (s[i] == ','){
      v.push_back(uintaj(s2));
      s2 = "";
    }
    else{
      s2 += s[i];
    }
  }

  vector<long long> input;
  input.clear();

  x = MAXN / 2;
  y = MAXN / 2;
  dir = 0;
  pnt = -1;
  rel = 0;

  m[x][y] = 1;

  int end, end2;

  while (true){
    input.push_back(m[x][y]);
    output = evaluate(v, input, pnt, rel);
    input.clear();

    end = output.back();
    output.pop_back();

    end2 = output.back();
    output.pop_back();

    dir += (output.back() == 1 ? 1 : -1);
    dir = (dir + 4) % 4;
    output.pop_back();

    m[x][y] = output.back();
    output.pop_back();

    x += dx[dir];
    y += dy[dir];

    if (end < -MEMORY){
      break;
    }
    else{
      pnt = -end;
      rel = end2;
    }
  }

  for (int x = 500; x < 510; x++){
    for (int y = 500; y < 550; y++){
      cout << (m[x][y] ? '#' : '.');
    }
    cout << endl;
  }

  return 0;
}
