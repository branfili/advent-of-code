#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <algorithm>
#include <queue>

using namespace std;

struct State{
  pair<int, int> pos;
  int depth;
};

string s;

vector<string> maze;

map<string, vector<pair<int, int> > > portals;

int dx[4] = {-1, 0, 1, 0};
int dy[4] = {0, 1, 0, -1};

queue<State> q;

void process(int x, int y){
  int nx, ny;
  string s;

  for (int i = 0; i < 4; i++){
    nx = x + dx[i];
    ny = y + dy[i];

    if (nx >= 0 && nx < (int)maze.size() &&
        ny >= 0 && ny < (int)maze[nx].size() &&
        'A' <= maze[nx][ny] && maze[nx][ny] <= 'Z'){
      s = "";
      s += maze[x][y];
      s += maze[nx][ny];

      if (i == 0 || i == 3){
        reverse(s.begin(), s.end());
      }

      nx += dx[i];
      ny += dy[i];
        if (nx >= 0 && nx < (int) maze.size() &&
            ny >= 0 && ny < (int) maze[nx].size() &&
            maze[nx][ny] == '.'){
          portals[s].push_back(make_pair(nx, ny));
        }
    }
  }
}

int bfs(void){
  int dist[50][maze.size()][maze[0].size()];
  int checked[50][maze.size()][maze[0].size()];

  for (int i = 0; i < (int) maze.size(); i++){
    for (int j = 0; j < (int) maze[i].size(); j++){
      for (int k = 0; k < 20; k++){
        dist[k][i][j] = 10000000;
        checked[k][i][j] = false;
      }
    }
  }

  State tmp;
  tmp.pos = portals["AA"][0];
  tmp.depth = 0;

  q.push(tmp);
  dist[0][tmp.pos.first][tmp.pos.second] = 0;
  checked[0][tmp.pos.first][tmp.pos.second] = true;

  int nx, ny;
  string s;
  while (!q.empty()){
    State tmp = q.front();
    q.pop();

    for (int i = 0; i < 4; i++){
      nx = tmp.pos.first + dx[i];
      ny = tmp.pos.second + dy[i];

      if (nx < 0 || nx >= (int) maze.size() ||
          ny < 0 || ny >= (int) maze[nx].size() ||
          maze[nx][ny] == '#'){
        continue;
      }

      State tmp2;
      tmp2.depth = tmp.depth;

      if ('A' <= maze[nx][ny] && maze[nx][ny] <= 'Z'){
        s = "";
        s += maze[nx][ny];
        s += maze[nx + dx[i]][ny + dy[i]];

        if (i == 0 || i == 3){
          reverse(s.begin(), s.end());
        }

        if (portals[s].size() == 1){
          continue;
        }
        
        if (nx == 1 || nx == (int)maze.size() - 2 ||
            ny == 1 || ny == (int)maze[nx].size() - 2){
          if (tmp2.depth == 0){
            continue;
          }
          
          tmp2.depth--;
        }
        else{
          tmp2.depth++;
        }

        if (tmp.pos.first == portals[s][0].first && 
            tmp.pos.second == portals[s][0].second){
          nx = portals[s][1].first;
          ny = portals[s][1].second;
        }
        else{
          nx = portals[s][0].first;
          ny = portals[s][0].second;
        }
      }

      if (checked[tmp2.depth][nx][ny]){
        continue;
      }

      tmp2.pos = make_pair(nx, ny);

      q.push(tmp2);
      dist[tmp2.depth][nx][ny] = dist[tmp.depth][tmp.pos.first][tmp.pos.second] + 1;
      checked[tmp2.depth][nx][ny] = true;
    }
  }

  return dist[0][portals["ZZ"][0].first][portals["ZZ"][0].second];
}

int main (void){
  while (getline(cin, s)){
    maze.push_back(s);
  }

  for (int i = 0; i < (int) maze.size(); i++){
    for (int j = 0; j < (int) maze[i].size(); j++){
      if ('A' <= maze[i][j] && maze[i][j] <= 'Z'){
        process(i, j);
      }
    }
  }

  cout << bfs() << endl;
  return 0;
}
