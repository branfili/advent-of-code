#include <iostream>
#include <string>
#include <cmath>
#include <vector>
#include <map>
#include <queue>

using namespace std;

string s, s2;

map<pair<string, long long>, vector<pair<string, long long> > > r;
queue<string> q;
map<string, long long> amount;
map<string, int> parents;

long long uintaj(string s){
  long long x = 0;
  for (int i = 0; i < (int) s.size(); i++){
    x *= 10;
    x += (s[i] - '0');
  }
  return x;
}

pair<string, int> process(string s){
  int i;
  for (i = 0; s[i] != ' '; i++);

  return make_pair(s.substr(i + 1), uintaj(s.substr(0, i)));
}

int main (void){
  while (getline(cin, s)){
    vector<pair<string, long long> > v;
    v.clear();

    s2 = "";
    for (int i = 0; i < (int) s.size(); i++){
      if (s[i] == ',' || s[i + 1] == '='){
        pair<string, long long> tmp = process(s2);
        v.push_back(tmp);
        parents[tmp.first]++;
        s2 = "";
        i++;

        if (s[i] == '='){
          i += 2;
        }
      }
      else{
        s2 += s[i];
      }
    }

    r[process(s2)] = v;
  }

  q.push("FUEL");
  amount["FUEL"] = 1184209;

  while (!q.empty()){
    string cur = q.front();
    q.pop();

    if (parents[cur] != 0){
      q.push(cur);
      continue;
    }

    if (cur == "ORE"){
      cout << amount[cur] << endl;
      break;
    }

    map<pair<string, long long>, vector<pair<string, long long> > >::iterator it = r.lower_bound(make_pair(cur, 0));

    long long mul = ceil((double) amount[cur] / it->first.second);

    for (int i = 0; i < (int) it->second.size(); i++){
      string tmp = it->second[i].first;

      parents[tmp]--;

      if (amount.find(tmp) == amount.end()){
        q.push(tmp);
      }

      amount[tmp] += mul * it->second[i].second;
    }
  }

  return 0;
}
