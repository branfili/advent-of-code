#include <iostream>
#include <string>

using namespace std;

const int ROWS = 6;
const int COLUMNS = 25;
const int LAYERS = 100;

string s;

int m[ROWS][COLUMNS];

int pixel(int l, int r, int c){
  return (s[l * ROWS * COLUMNS + r * COLUMNS + c] - '0');
}

int main (void){
  cin >> s;

  for (int i = 0; i < ROWS; i++){
    for (int j = 0; j < COLUMNS; j++){
      int l;
      for (l = 0; pixel(l, i, j) == 2; l++);

      m[i][j] = pixel(l, i, j);
      cout << (m[i][j] == 1 ? '#' : ' ');
    }
    cout << endl;
  }
  
  return 0;
}
