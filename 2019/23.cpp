#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <thread>
#include <queue>
#include <chrono>

using namespace std;

const int MAXN = 50;
const int MEMORY = 20000;

string s, s2;

vector <long long> v;
deque <long long> input[MAXN];

vector <thread> network;

long long natX, natY;

long long uintaj(string s){
  bool neg = false;
  
  if (s[0] == '-'){
    neg = true;
    s = s.substr(1);
  }

  long long z = 0;
  for (int i = 0; i < (int)s.size(); i++){
    z *= 10;
    z += (s[i] - '0');
  }

  z *= (neg ? -1 : 1);

  return z;
}

void evaluate(int id, int ilen, vector<long long> state){
  vector<long long> output;
  output.clear();

  int op;
  long long a, b, c;
  int p = 0;
  int r = 0;

  while (p < ilen){
    op = state[p];

    int om = op % 100;
    int am = (op % 1000) / 100;
    int bm = (op % 10000) / 1000;
    int cm = op / 10000;

    if (om == 1){
      if (am == 0){
        a = state[state[p + 1]];
      }
      else if (am == 1){
        a = state[p + 1];
      }
      else{
        a = state[r + state[p + 1]];
      }

      if (bm == 0){
        b = state[state[p + 2]];
      }
      else if (bm == 1){
        b = state[p + 2];
      }
      else{
        b = state[r + state[p + 2]];
      }

      c = a + b;

      if (cm == 0){
        state[state[p + 3]] = c;
      }
      else if (cm == 1){
        state[p + 3] = c;
      }
      else{
        state[r + state[p + 3]] = c;
      }

      p += 4;
    }
    else if (om == 2){
      if (am == 0){
        a = state[state[p + 1]];
      }
      else if (am == 1){
        a = state[p + 1];
      }
      else{
        a = state[r + state[p + 1]];
      }

      if (bm == 0){
        b = state[state[p + 2]];
      }
      else if (bm == 1){
        b = state[p + 2];
      }
      else{
        b = state[r + state[p + 2]];
      }

      c = a * b;

      if (cm == 0){
        state[state[p + 3]] = c;
      }
      else if (cm == 1){
        state[p + 3] = c;
      }
      else{
        state[r + state[p + 3]] = c;
      }

      p += 4;
    }
    else if (om == 3){
      if (input[id].empty()){
        a = -1;
      }
      else{
        a = input[id].front();
        input[id].pop_front();
      }

      if (am == 0){
        state[state[p + 1]] = a;
      }
      else if (am == 1){
        state[p + 1] = a;
      }
      else{
        state[r + state[p + 1]] = a;
      }

      p += 2;
    }
    else if (op % 100 == 4){
      if (am == 0){
        a = state[state[p + 1]];
      }
      else if (am == 1){
        a = state[p + 1];
      }
      else{
        a = state[r + state[p + 1]];
      }

      output.push_back(a);

      if (output.size() == 3){
        if (output[0] < 50){
          input[output[0]].push_back(output[1]);
          input[output[0]].push_back(output[2]);
        }

        if (output[0] == 255){
          natX = output[1];
          natY = output[2];
        }

        output.clear();
      }

      p += 2;
    }
    else if (om == 5){
      if (am == 0){
        a = state[state[p + 1]];
      }
      else if (am == 1){
        a = state[p + 1];
      }
      else{
        a = state[r + state[p + 1]];
      }

      if (bm == 0){
        b = state[state[p + 2]];
      }
      else if (bm == 1){
        b = state[p + 2];
      }
      else{
        b = state[r + state[p + 2]];
      }

      if (a != 0){
        p = b;
      }
      else{
        p += 3;
      }
    }
    else if (om == 6){
      if (am == 0){
        a = state[state[p + 1]];
      }
      else if (am == 1){
        a = state[p + 1];
      }
      else{
        a = state[r + state[p + 1]];
      }

      if (bm == 0){
        b = state[state[p + 2]];
      }
      else if (bm == 1){
        b = state[p + 2];
      }
      else{
        b = state[r + state[p + 2]];
      }

      if (a == 0){
        p = b;
      }
      else{
        p += 3;
      }
    }
    else if (om == 7){
      if (am == 0){
        a = state[state[p + 1]];
      }
      else if (am == 1){
        a = state[p + 1];
      }
      else{
        a = state[r + state[p + 1]];
      }

      if (bm == 0){
        b = state[state[p + 2]];
      }
      else if (bm == 1){
        b = state[p + 2];
      }
      else{
        b = state[r + state[p + 2]];
      }

      c = (a < b);
      
      if (cm == 0){
        state[state[p + 3]] = c;
      }
      else if (cm == 1){
        state[p + 3] = c;
      }
      else{
        state[r + state[p + 3]] = c;
      }

      p += 4;
    }
    else if (om == 8){
      if (am == 0){
        a = state[state[p + 1]];
      }
      else if (am == 1){
        a = state[p + 1];
      }
      else{
        a = state[r + state[p + 1]];
      }

      if (bm == 0){
        b = state[state[p + 2]];
      }
      else if (bm == 1){
        b = state[p + 2];
      }
      else{
        b = state[r + state[p + 2]];
      }

      c = (a == b);

      if (cm == 0){
        state[state[p + 3]] = c;
      }
      else if (cm == 1){
        state[p + 3] = c;
      }
      else{
        state[r + state[p + 3]] = c;
      }

      p += 4;
    }
    else if (om == 9){
      if (am == 0){
        a = state[state[p + 1]];
      }
      else if (am == 1){
        a = state[p + 1];
      }
      else{
        a = state[r + state[p + 1]];
      }

      r += a;
      p += 2;
    }
    else if (om == 99){
      break;
    }
  }
  
  return ;
}

int main (void){
  cin >> s;

  s += ',';
  s2 = "";

  for (int i = 0; i < (int) s.size(); i++){
    if (s[i] == ','){
      v.push_back(uintaj(s2));
      s2 = "";
    }
    else{
      s2 += s[i];
    }
  }

  vector<long long> state;

  for (int i = 0; i < (int) v.size(); i++){
    state.push_back(v[i]);
  }

  for (int i = (int) v.size(); i < MEMORY; i++){
    state.push_back(0);
  }

  for (int i = 0; i < MAXN; i++){
    input[i].push_back(i);
  }

  for (int i = 0; i < MAXN; i++){
    network.push_back(thread(evaluate, i, (int) v.size(), state));
  }

  bool e;
  while (true){
    e = true;
    for (int i = 0; i < MAXN; i++){
      e &= input[i].empty();
    }

    if (e){
      input[0].push_back(natX);
      input[0].push_back(natY);
      cout << natY << endl;
    }

    this_thread::sleep_for(std::chrono::seconds(1));
  }

  return 0;
}
