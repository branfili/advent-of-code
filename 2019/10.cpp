#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <utility>
#include <cmath>

using namespace std;

string s;
vector<pair<int, int> > aster;
vector<pair<double, pair<int, int> > > hitList;

int maks;
pair<int, int> opt;

int gcd(int a, int b){
  if (b == 0){
    return a;
  }

  return gcd(b, a % b);
}

bool check(pair<int, int> p, pair<int, int> p2){
  if (p.first == p2.first && p.second == p2.second){
    return false;
  }

  int g = gcd(abs(p.first - p2.first), abs(p.second - p2.second));

  for (int i = 1; i < g; i++){
    pair<int, int> tmp = make_pair(p.first + i * (p2.first - p.first) / g, 
                                   p.second + i * (p2.second - p.second) / g);
    for (int j = 0; j < (int) aster.size(); j++){
      if (tmp.first == aster[j].first && tmp.second == aster[j].second){
        return false;
      }
    }
  }

  return true;
}

int count(pair<int, int> p){
  int c = 0;
  for (int i = 0; i < (int) aster.size(); i++){
    c += (check(p, aster[i]) ? 1 : 0);
  }

  return c;
}

int main(void){
  for (int i = 0; cin >> s; i++){
    for (int j = 0; j < (int) s.size(); j++){
      if (s[j] == '#'){
        aster.push_back(make_pair(j, i));
      }
    }
  }

  for (int i = 0; i < (int)aster.size(); i++){
    int c = count(aster[i]);

    if (c > maks){
      maks = c;
      opt.first = aster[i].first;
      opt.second = aster[i].second;
    }
  }

  int c = 200;
  vector<pair<int,int> > aster2;

  while (true){
    hitList.clear();
    aster2.clear();

    for (int i = 0; i < (int) aster.size(); i++){
      if (check(opt, aster[i])){
        double at = atan2(aster[i].second - opt.second,
                          aster[i].first - opt.first);

        if (at < -M_PI / 2){
          at += 2 * M_PI;
        }

        hitList.push_back(make_pair(at, aster[i]));
      }
      else{
        aster2.push_back(aster[i]);
      }
    }

    if (hitList.empty()){
      break;
    }

    sort(hitList.begin(), hitList.end());

    aster = aster2;

    if (c > (int)hitList.size()){
      c -= hitList.size();
    }
    else if (c > 0){
      cout << hitList[c - 1].second.first * 100 + hitList[c - 1].second.second << endl;
      c = 0;
    }
  }

  return 0;
}
