#include <iostream>
#include <vector>
#include <string>

using namespace std;

string s, s2;

int p;

vector <int> v;

int uintaj(string s){
  bool neg = false;
  
  if (s[0] == '-'){
    neg = true;
    s = s.substr(1);
  }

  int z = 0;
  for (int i = 0; i < (int)s.size(); i++){
    z *= 10;
    z += (s[i] - '0');
  }

  z *= (neg ? -1 : 1);

  return z;
}

int main (void){
  cin >> s;

  s += ',';
  s2 = "";

  for (int i = 0; i < (int) s.size(); i++){
    if (s[i] == ','){
      v.push_back(uintaj(s2));
      s2 = "";
    }
    else{
      s2 += s[i];
    }
  }

  int op, a, b, c;
  p = 0;

  while (true){
    op = v[p];

    if (op % 100 == 1){
      if ((op % 1000) / 100 == 0){
        a = v[v[p + 1]];
      }
      else{
        a = v[p + 1];
      }

      if ((op % 10000) / 1000 == 0){
        b = v[v[p + 2]];
      }
      else{
        b = v[p + 2];
      }

      c = a + b;

      if (op / 10000 == 0){
        v[v[p + 3]] = c;
      }
      else{
        v[p + 3] = c;
      }

      p += 4;
    }
    else if (op % 100 == 2){
      if ((op % 1000) / 100 == 0){
        a = v[v[p + 1]];
      }
      else{
        a = v[p + 1];
      }

      if ((op % 10000) / 1000 == 0){
        b = v[v[p + 2]];
      }
      else{
        b = v[p + 2];
      }

      c = a * b;

      if (op / 10000 == 0){
        v[v[p + 3]] = c;
      }
      else{
        v[p + 3] = c;
      }

      p += 4;
    }
    else if (op % 100 == 3){
      cin >> a;

      if ((op % 1000) / 100 == 0){
        v[v[p + 1]] = a;
      }
      else{
        v[p + 1] = a;
      }

      p += 2;
    }
    else if (op % 100 == 4){
      if ((op % 1000) / 100 == 0){
        a = v[v[p + 1]];
      }
      else{
        a = v[p + 1];
      }

      cout << a << endl;

      p += 2;
    }
    else if (op % 100 == 5){
      if ((op % 1000) / 100 == 0){
        a = v[v[p + 1]];
      }
      else{
        a = v[p + 1];
      }

      if ((op % 10000) / 1000 == 0){
        b = v[v[p + 2]];
      }
      else{
        b = v[p + 2];
      }

      if (a != 0){
        p = b;
      }
      else{
        p += 3;
      }
    }
    else if (op % 100 == 6){
      if ((op % 1000) / 100 == 0){
        a = v[v[p + 1]];
      }
      else{
        a = v[p + 1];
      }

      if ((op % 10000) / 1000 == 0){
        b = v[v[p + 2]];
      }
      else{
        b = v[p + 2];
      }

      if (a == 0){
        p = b;
      }
      else{
        p += 3;
      }
    }
    else if (op % 100 == 7){
      if ((op % 1000) / 100 == 0){
        a = v[v[p + 1]];
      }
      else{
        a = v[p + 1];
      }

      if ((op % 10000) / 1000 == 0){
        b = v[v[p + 2]];
      }
      else{
        b = v[p + 2];
      }

      c = (a < b);

      if (op / 10000 == 0){
        v[v[p + 3]] = c;
      }
      else{
        v[p + 3] = c;
      }

      p += 4;
    }
    else if (op % 100 == 8){
      if ((op % 1000) / 100 == 0){
        a = v[v[p + 1]];
      }
      else{
        a = v[p + 1];
      }

      if ((op % 10000) / 1000 == 0){
        b = v[v[p + 2]];
      }
      else{
        b = v[p + 2];
      }

      c = (a == b);

      if (op / 10000 == 0){
        v[v[p + 3]] = c;
      }
      else{
        v[p + 3] = c;
      }

      p += 4;
    }
    else if (op % 100 == 99){
      break;
    }
  }
}
