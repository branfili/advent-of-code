#include <iostream>
#include <cmath>
#include <vector>
#include <cstdio>

using namespace std;
const long long STEP = 1000000;

struct vektor{
  int x, y, z;
};

struct moon{
  vektor p;
  vektor v;
};

vector<moon> v;

int sgn(int x){
  return ((x >= 0) - (x <= 0));
}

bool check(int step){
  bool x0, y0, z0;
  x0 = y0 = z0 = true;

  for (int i = 0; i < (int)v.size(); i++){
    x0 &= (v[i].v.x == 0);
    y0 &= (v[i].v.y == 0);
    z0 &= (v[i].v.z == 0);
  }

  return (x0 || y0 || z0);
}

int main (void){
  int x, y, z;

  while (cin >> x >> y >> z){
    moon tmp;
    tmp.p.x = x;
    tmp.p.y = y;
    tmp.p.z = z;
    tmp.v.x = tmp.v.y = tmp.v.z;
  
    v.push_back(tmp);
  }

  for (long long st = 0; st < STEP; st++){
    if (check(st)){
      cout << "Step: " << st << endl;

      for (int i = 0; i < (int) v.size(); i++){
        printf("%6d %6d %6d\t%6d %6d %6d\n", v[i].p.x, v[i].p.y, v[i].p.z, v[i].v.x, v[i].v.y, v[i].v.z);
      }
      cout << endl;
    }

    for (int i = 0; i < (int)v.size(); i++){
      for (int j = i + 1; j < (int)v.size(); j++){
        v[i].v.x += sgn(v[j].p.x - v[i].p.x);
        v[j].v.x += sgn(v[i].p.x - v[j].p.x);
        
        v[i].v.y += sgn(v[j].p.y - v[i].p.y);
        v[j].v.y += sgn(v[i].p.y - v[j].p.y);
        
        v[i].v.z += sgn(v[j].p.z - v[i].p.z);
        v[j].v.z += sgn(v[i].p.z - v[j].p.z);
      }
    }

    for (int i = 0; i < (int)v.size(); i++){
      v[i].p.x += v[i].v.x;
      v[i].p.y += v[i].v.y;
      v[i].p.z += v[i].v.z;
    }
  }

  return 0;
}
