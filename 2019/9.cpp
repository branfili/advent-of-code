#include <iostream>
#include <vector>
#include <string>
#include <algorithm>

using namespace std;

string s, s2;

vector <long long> v;

long long uintaj(string s){
  bool neg = false;
  
  if (s[0] == '-'){
    neg = true;
    s = s.substr(1);
  }

  long long z = 0;
  for (int i = 0; i < (int)s.size(); i++){
    z *= 10;
    z += (s[i] - '0');
  }

  z *= (neg ? -1 : 1);

  return z;
}

vector<long long> evaluate(vector<long long>& v2, vector<long long>& input){
  vector<long long> v(v2);
  vector<long long> output;
  output.clear();

  int ilen = v.size();

  while (v.size() < 10000){
    v.push_back(0);
  }

  int op;
  long long a, b, c;
  int inind = 0;
  int p = 0;
  int r = 0;

  while (p < ilen){
    op = v[p];

    int om = op % 100;
    int am = (op % 1000) / 100;
    int bm = (op % 10000) / 1000;
    int cm = op / 10000;

    if (om == 1){
      if (am == 0){
        a = v[v[p + 1]];
      }
      else if (am == 1){
        a = v[p + 1];
      }
      else{
        a = v[r + v[p + 1]];
      }

      if (bm == 0){
        b = v[v[p + 2]];
      }
      else if (bm == 1){
        b = v[p + 2];
      }
      else{
        b = v[r + v[p + 2]];
      }

      c = a + b;

      if (cm == 0){
        v[v[p + 3]] = c;
      }
      else if (cm == 1){
        v[p + 3] = c;
      }
      else{
        v[r + v[p + 3]] = c;
      }

      p += 4;
    }
    else if (om == 2){
      if (am == 0){
        a = v[v[p + 1]];
      }
      else if (am == 1){
        a = v[p + 1];
      }
      else{
        a = v[r + v[p + 1]];
      }

      if (bm == 0){
        b = v[v[p + 2]];
      }
      else if (bm == 1){
        b = v[p + 2];
      }
      else{
        b = v[r + v[p + 2]];
      }

      c = a * b;

      if (cm == 0){
        v[v[p + 3]] = c;
      }
      else if (cm == 1){
        v[p + 3] = c;
      }
      else{
        v[r + v[p + 3]] = c;
      }

      p += 4;
    }
    else if (om == 3){
      a = input[inind++];
      
      if (am == 0){
        v[v[p + 1]] = a;
      }
      else if (am == 1){
        v[p + 1] = a;
      }
      else{
        v[r + v[p + 1]] = a;
      }

      p += 2;
    }
    else if (op % 100 == 4){
      if (am == 0){
        a = v[v[p + 1]];
      }
      else if (am == 1){
        a = v[p + 1];
      }
      else{
        a = v[r + v[p + 1]];
      }

      output.push_back(a);

      p += 2;
    }
    else if (om == 5){
      if (am == 0){
        a = v[v[p + 1]];
      }
      else if (am == 1){
        a = v[p + 1];
      }
      else{
        a = v[r + v[p + 1]];
      }

      if (bm == 0){
        b = v[v[p + 2]];
      }
      else if (bm == 1){
        b = v[p + 2];
      }
      else{
        b = v[r + v[p + 2]];
      }

      if (a != 0){
        p = b;
      }
      else{
        p += 3;
      }
    }
    else if (om == 6){
      if (am == 0){
        a = v[v[p + 1]];
      }
      else if (am == 1){
        a = v[p + 1];
      }
      else{
        a = v[r + v[p + 1]];
      }

      if (bm == 0){
        b = v[v[p + 2]];
      }
      else if (bm == 1){
        b = v[p + 2];
      }
      else{
        b = v[r + v[p + 2]];
      }

      if (a == 0){
        p = b;
      }
      else{
        p += 3;
      }
    }
    else if (om == 7){
      if (am == 0){
        a = v[v[p + 1]];
      }
      else if (am == 1){
        a = v[p + 1];
      }
      else{
        a = v[r + v[p + 1]];
      }

      if (bm == 0){
        b = v[v[p + 2]];
      }
      else if (bm == 1){
        b = v[p + 2];
      }
      else{
        b = v[r + v[p + 2]];
      }

      c = (a < b);
      
      if (cm == 0){
        v[v[p + 3]] = c;
      }
      else if (cm == 1){
        v[p + 3] = c;
      }
      else{
        v[r + v[p + 3]] = c;
      }

      p += 4;
    }
    else if (om == 8){
      if (am == 0){
        a = v[v[p + 1]];
      }
      else if (am == 1){
        a = v[p + 1];
      }
      else{
        a = v[r + v[p + 1]];
      }

      if (bm == 0){
        b = v[v[p + 2]];
      }
      else if (bm == 1){
        b = v[p + 2];
      }
      else{
        b = v[r + v[p + 2]];
      }

      c = (a == b);

      if (cm == 0){
        v[v[p + 3]] = c;
      }
      else if (cm == 1){
        v[p + 3] = c;
      }
      else{
        v[r + v[p + 3]] = c;
      }

      p += 4;
    }
    else if (om == 9){
      if (am == 0){
        a = v[v[p + 1]];
      }
      else if (am == 1){
        a = v[p + 1];
      }
      else{
        a = v[r + v[p + 1]];
      }

      r += a;
      p += 2;
    }
    else if (om == 99){
      break;
    }
  }

  return output;
}

int main (void){
  cin >> s;

  s += ',';
  s2 = "";

  for (int i = 0; i < (int) s.size(); i++){
    if (s[i] == ','){
      v.push_back(uintaj(s2));
      s2 = "";
    }
    else{
      s2 += s[i];
    }
  }

  vector<long long> input;
  input.clear();
  input.push_back(2);

  input = evaluate(v, input);

  for (int i = 0; i < (int) input.size(); i++){
    cout << input[i] << endl;
  }

  return 0;
}
