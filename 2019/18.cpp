#include <string>
#include <vector>
#include <iostream>
#include <algorithm>
#include <utility>
#include <queue>

using namespace std;

struct State{
  string keys;

  pair<int, int> pos[4];

  int score;
};

struct StateCompare{
  bool operator()(State& a, State& b){
    if (a.score != b.score){
      return a.score > b.score;
    }
  
    return a.keys.size() < b.keys.size();
  }
};

string s;

pair<int, int> st;
int mini;

vector<string> map;
vector<pair<int, int> > keypos(26);
vector<pair<int, int> > doorpos(26);

int dist[81][81];
queue <pair<int, int> > q;
bool checked[81][81];

int dx[4] = {-1, 0, 1, 0};
int dy[4] = {0, 1, 0, -1};

priority_queue<State, vector<State>, StateCompare> pq;

bool find(char c, string s){
  for (int i = 0; i < (int) s.size(); i++){
    if (s[i] == c){
      return true;
    }
  }

  return false;
}

pair<string, bool> check(pair<int, int> a, string sol, string cur_keys){
  if (a.first < 0 || a.first >= 81 ||
      a.second < 0 || a.second >= 81){
    return make_pair(sol, false);
  }

  if (checked[a.first][a.second]){
    return make_pair(sol, false);
  }

  if ('A' <= map[a.first][a.second] && map[a.first][a.second] <= 'Z'){
    return make_pair(sol, find(map[a.first][a.second] - 'A' + 'a', cur_keys));
  }

  if ('a' <= map[a.first][a.second] && map[a.first][a.second] <= 'z'){
    sol += map[a.first][a.second];
  }

  return make_pair(sol, map[a.first][a.second] != '#');
}

bool compKey(char a, char b){
  pair<int, int> tmp1 = keypos[a - 'a'];
  pair<int, int> tmp2 = keypos[b - 'a'];

  return dist[tmp1.first][tmp1.second] < dist[tmp2.first][tmp2.second];
}

string bfs(pair<int, int> cur, string cur_keys){
  for (int i = 0; i < 81; i++){
    for (int j = 0; j < 81; j++){
      dist[i][j] = 1000000;
      checked[i][j] = false;
    }
  }

  q.push(cur);
  dist[cur.first][cur.second] = 0;
  string sol = "";

  while (!q.empty()){
    pair<int, int> tmp = q.front();
    q.pop();

    if (checked[tmp.first][tmp.second]){
      continue;
    }

    checked[tmp.first][tmp.second] = true;

    for (int i = 0; i < 4; i++){
      pair<int, int> tmp2 = make_pair(tmp.first + dx[i],
                                      tmp.second + dy[i]);
    
      pair<string, bool> tmp3 = check(tmp2, sol, cur_keys);
      sol = tmp3.first;

      if (tmp3.second){
        q.push(tmp2);
        dist[tmp2.first][tmp2.second] = dist[tmp.first][tmp.second] + 1;
      }
    } 
  }

  string sol2 = "";
  for (int i = 0; i < (int) sol.size(); i++){
    if (!find(sol[i], cur_keys)){
      sol2 += sol[i];
    }
  }

  sort(sol2.begin(), sol2.end(), compKey);

  return sol2;
}

bool contains(string s, string s2){
  for (int i = 0; i < (int) s2.size(); i++){
    if (!find(s2[i], s)){
      return false;
    }
  }

  return true;
}

int main(void){
  while (cin >> s){
    map.push_back(s);
  }

  for (int i = 0; i < (int) map.size(); i++){
    for (int j = 0; j < (int) map[i].size(); j++){
      if (map[i][j] == '@'){
        st = make_pair(i, j);
      }
      else if ('a' <= map[i][j] && map[i][j] <= 'z'){
        keypos[map[i][j] - 'a'] = make_pair(i, j);
      }
      else if ('A' <= map[i][j] && map[i][j] <= 'Z'){
        doorpos[map[i][j] - 'A'] = make_pair(i, j);
      }
    }
  }

  map[st.first][st.second] = '#';
  for (int i = 0; i < 4; i++){
    map[st.first + dx[i]][st.second + dy[i]] = '#';
  }

  mini = 1000000;
  State tmp;
  vector<string> score_keys;

  tmp.keys = "";
  tmp.score = 0;

  int dx2[4] = {1, 1, -1, -1};
  int dy2[4] = {-1, 1, -1, 1};

  for (int i = 0; i < 4; i++){
    tmp.pos[i] = make_pair(st.first + dx2[i], st.second + dy2[i]);
  }

  pq.push(tmp);

  while (!pq.empty()){
    if (pq.top().score == 0 || 
        pq.top().score != tmp.score){
      score_keys.clear();
    }

    tmp = pq.top();
    pq.pop();

    bool e = true;
    for (int i = 0; i < (int) score_keys.size(); i++){
      if (tmp.keys.size() <= score_keys[i].size() &&
          contains(score_keys[i], tmp.keys) &&
          tmp.keys[tmp.keys.size() - 1] == score_keys[i][score_keys[i].size() - 1]){
        e = false;
        break;
      }
    }

    if (!e){
      continue;
    }

    score_keys.push_back(tmp.keys);

    if (tmp.keys.size() == 26){
      mini = tmp.score;
      break;
    }

    string sol;

    for (int i = 0; i < 4; i++){
      sol = bfs(tmp.pos[i], tmp.keys);

      for (int j = 0; j < (int) sol.size(); j++){
        pair<int, int> tmp2 = keypos[sol[j] - 'a'];

        State tmp3;
        tmp3.keys = tmp.keys + sol[j];
        tmp3.score = tmp.score + dist[tmp2.first][tmp2.second];
        
        for (int k = 0; k < 4; k++){
          if (i == k){
            tmp3.pos[k] = tmp2;
          }
          else{
            tmp3.pos[k] = tmp.pos[k];
          }
        }
      
        pq.push(tmp3);
      }
    }
  }

  cout << mini << endl;
  return 0;
}
