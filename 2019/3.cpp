#include <iostream>
#include <string>
#include <vector>
#include <cmath>

using namespace std;

const int MAXD = 20000;

short a[MAXD][MAXD];
int b[MAXD][MAXD];

string s1, s2;

vector <string> v, v2;

int uintaj(string s){
  int z = 0;
  for (int i = 0; i < (int) s.size(); i++){
    z *= 10;
    z += (s[i] - '0');
  }
  return z;
}

vector<string> split(string s, char c){
  vector <string> sol;
  sol.clear();

  string s2 = "";

  s += c;
  for (int i = 0; i < (int) s.size(); i++){
    if (s[i] == c){
      sol.push_back(s2);
      s2 = "";
    }
    else{
      s2 += s[i];
    }
  }

  return sol;
}

void traverse(vector<string>& v, int x, int y, int m){
  int l;
  char d;
  int s = 0;

  int curx = x;
  int cury = y;

  for (int i = 0; i < (int) v.size(); i++){
    d = v[i][0];
    l = uintaj(v[i].substr(1));

    for (int j = 0; j < l; j++){
      s++;

      if (d == 'L'){
        cury--;
      }
      else if (d == 'R'){
        cury++;
      }
      else if (d == 'U'){
        curx--;
      }
      else if (d == 'D'){
        curx++;
      }

      if (m > 0){
        a[curx][cury] += m;
      }
      else{
        if (a[curx][cury] == 3){
          b[curx][cury] += s;
        }
      }
    }
  }
}

int main (void){
  cin >> s1 >> s2;

  v = split(s1, ',');
  v2 = split(s2, ',');

  a[MAXD / 2][MAXD / 2] = 4;
  traverse(v, MAXD / 2, MAXD / 2, 1);
  traverse(v2, MAXD / 2, MAXD / 2, 2);

  traverse(v, MAXD / 2, MAXD / 2, 0);
  traverse(v2, MAXD / 2, MAXD / 2, 0);

  int mini = 1000000000;

  for (int i = 0; i < MAXD; i++){
    for (int j = 0; j < MAXD; j++){
      if (b[i][j] != 0){
        mini = min(mini, b[i][j]);
      }
    }
  }

  cout << mini << endl;

  return 0;
}
