#!/usr/bin/python3

import sys
import copy
from functools import reduce
import operator

global isPartTwo
isPartTwo = True

class Item:
    def __init__(self, x, tests):
        if (not isPartTwo):
            self.x = x
        else:
            self.tests = copy.deepcopy(tests)
            self.results = list(map(lambda test: x % test, tests))

    def getResult(self, test):
       if (not isPartTwo):
           return (self.x % test)

       return self.results[self.tests.index(test)]

    def applyOperation(self, oper, am):
        if (not isPartTwo):
            self.x = oper(self.x, am)
            self.x //= 3
            return

        for i in range(len(self.results)):
            self.results[i] = oper(self.results[i], am)
            self.results[i] %= self.tests[i]

class Monkey:
    def __init__(self, items, oper, am, test, trueThrow, falseThrow):
        self.items = copy.deepcopy(items)
        self.oper = oper
        self.am = am
        self.test = test
        self.throws = [trueThrow, falseThrow]
        self.inspected = 0

    def throwItems(self):
        for it in self.items:
            it.applyOperation(self.oper, self.am)
            #it.divideByThree()
            self.inspected += 1

            if (it.getResult(self.test) == 0):
                self.throws[0].catchItem(it)
            else:
                self.throws[1].catchItem(it)

        self.items = []

    def catchItem(self, x):
        self.items.append(x)

MAX_ROUNDS = (10000 if isPartTwo else 20)

lines = list(map(lambda line: line.strip(), sys.stdin.read().split('\n')[:-1]))

monkeys = []
for i in range(0, len(lines), 7):
    lines[i + 1] += ','
    items = list(map(lambda token: int(token[:-1]), lines[i + 1].split()[2:]))

    oper = lines[i + 2].split()[-2]
    am = lines[i + 2].split()[-1]
    if (am == 'old'):
        oper = operator.pow
        am = 2
    elif (oper == '+'):
        oper = operator.add
        am = int(am)
    elif (oper == '*'):
        oper = operator.mul
        am = int(am)

    test = int(lines[i + 3].split()[-1])

    throwsTrue = int(lines[i + 4].split()[-1])
    throwsFalse = int(lines[i + 5].split()[-1])

    monkeys.append(Monkey(items, oper, am, test, throwsTrue, throwsFalse))

monkeyTests = list(map(lambda m: m.test, monkeys))

for m in monkeys:
    for i in range(len(m.items)):
        m.items[i] = Item(m.items[i], monkeyTests)

    for i in range(len(m.throws)):
        m.throws[i] = monkeys[m.throws[i]]

for r in range(MAX_ROUNDS):
    for i, m in enumerate(monkeys):
        m.throwItems()

l = list(map(lambda m: m.inspected, monkeys))
l = sorted(l, reverse = True)
print(l[0] * l[1])
