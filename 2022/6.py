#!/usr/bin/python3

l = input()
N = 14

i = 0
while (i < len(l) - N):
    if (len(set(l[i:i + N])) == N):
        break

    i += 1

print(i + N)
