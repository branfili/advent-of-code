#!/usr/bin/python3

import sys

def sumDir(a, b):
    return (a[0] + b[0], a[1] + b[1])

def calcArea(elves):
    mx, Mx = 10 ** 12, -(10 ** 12)
    my, My = 10 ** 12, -(10 ** 12)

    for e in elves:
        mx = min(mx, e[0])
        Mx = max(Mx, e[0])
        my = min(my, e[1])
        My = max(My, e[1])

    SIZE_X = Mx - mx + 1
    SIZE_Y = My - my + 1

    area = SIZE_X * SIZE_Y
    area -= len(elves)
    return area

DIRECTIONS = [(0, 1), (0, -1), (-1, 0), (1, 0)]
MAX_ROUNDS = 10

lines = sys.stdin.read().split('\n')[:-1]

elves = set([])
for i in range(len(lines)):
    for j in range(len(lines[i])):
        if (lines[i][j] == '#'):
            elves.add((j, -i))

r = 0
while True:
    visited = {}
    banlist = set([])
    newElves = set([])

    for e in elves:
        l = []
        for i in range(len(DIRECTIONS)):
            nE = sumDir(e, DIRECTIONS[(i + r) % 4])

            diag = []
            if ((i + r) % 4 < 2):
                diag = [sumDir(nE, DIRECTIONS[2]), sumDir(nE, DIRECTIONS[3])]
            else:
                diag = [sumDir(nE, DIRECTIONS[0]), sumDir(nE, DIRECTIONS[1])]

            if (nE not in elves and \
                diag[0] not in elves and \
                diag[1] not in elves):
                l.append(nE)

        if (len(l) == 0 or \
            len(l) == 4):
            newElves.add(e)
        elif (l[0] in banlist):
            newElves.add(e)
        elif (l[0] in visited.keys()):
            banlist.add(l[0])
            newElves.add(e)
            newElves.add(visited[l[0]])
            newElves.remove(l[0])
        else:
            newElves.add(l[0])
            visited[l[0]] = e

    r += 1

    if (elves == newElves):
        print(r)
        break

    elves = newElves

    if (r == 10):
        print(calcArea(elves))
