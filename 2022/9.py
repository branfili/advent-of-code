#!/usr/bin/python3

import sys

def diff(x, y):
    return [x[0] - y[0], x[1] - y[1]]

dirs = {
        'R': (1, 0),
        'U': (0, 1),
        'L': (-1, 0),
        'D': (0, -1)
       }
N = 10

instrs = sys.stdin.read().split('\n')[:-1]
instrs = list(map(lambda line: [line.split()[0], int(line.split()[1])], instrs))

knots = []
for k in range(N):
    knots.append([0, 0])

tailVisited = set([])
tailVisited.add(tuple(knots[-1]))

for d, x in instrs:
    for i in range(x):
        knots[0] = [knots[0][0] + dirs[d][0], knots[0][1] + dirs[d][1]]

        for k in range(1, N):
            dif = diff(knots[k - 1], knots[k])

            if (max(map(abs, dif)) == 1):
                continue

            if (dif[0] != 0):
                dif[0] /= abs(dif[0])

            if (dif[1] != 0):
                dif[1] /= abs(dif[1])

            knots[k] = [knots[k][0] + dif[0], knots[k][1] + dif[1]]

        tailVisited.add(tuple(knots[-1]))

print(len(tailVisited))
