#!/usr/bin/python3

import sys
import copy
from math import sqrt
from queue import PriorityQueue
from collections import defaultdict
import time as Time

class Blueprint:
    def __init__(self, ID, bots):
        self.ID = ID
        self.bots = copy.deepcopy(bots)

    def makeBot(self, botID, rBots):
        global MINERALS

        r2 = []
        for i in range(len(MINERALS)):
            r2.append((rBots[i][0] - self.bots[botID][i], rBots[i][1]))
            if (r2[-1][0] < 0):
                return None
        return tuple(r2)

global MINERALS, MAX_TIME, MAX_TRIES, MAX_BOTS, ALPHA
MINERALS = ['ore', 'clay', 'obsidian', 'geodes']
MAX_TIME = 32

MAX_BOTS = [5, 10, 10, 10]
ALPHA = 0.12 #0.18 for part 1

def addUp(a):
    c = []
    for x, y in a:
        c.append((x + y, y))
    return tuple(c)

def calcHeuristic(state, blueprint):
    global ALPHA
    time, rBots = state

    l = [0, 0, 0, 0]
    for i in range(4):
        s = 0
        for j in range(i):
            if (blueprint.bots[i][j] != 0):
                s += l[j] // blueprint.bots[i][j]

        l[i] = rBots[i][0] + (ALPHA * s + rBots[i][1]) * (MAX_TIME - time)

    return (l[3] * 10 ** 6 + l[2] * 10 ** 4 + l[1] * 10 ** 2 + l[0])

def mineGeodes(firstState, blueprint):
    global MAX_BOTS

    pq = PriorityQueue()
    pq.put((-calcHeuristic(firstState, blueprint), firstState))

    visited = set([])
    sol = 0
    while (not pq.empty()):
        state = pq.get()
        _, (time, rBots) = state

        if (time == MAX_TIME):
            sol = rBots[-1][0]
            break

        if (state in visited):
            continue

        visited.add(state)

        for i in range(len(MINERALS)):
            if (rBots[i][1] == MAX_BOTS[i]):
                continue

            rBots2 = blueprint.makeBot(i, rBots)

            if (rBots2 is None):
                continue

            rBots2 = addUp(rBots2)

            rBots2 = list(rBots2)
            rBots2[i] = list(rBots2[i])
            rBots2[i][1] += 1
            rBots2[i] = tuple(rBots2[i])
            rBots2 = tuple(rBots2)

            newState = (time + 1, rBots2)
            pq.put((-calcHeuristic(newState, blueprint), newState))

        rBots = addUp(rBots)

        newState = (time + 1, rBots)
        pq.put((-calcHeuristic(newState, blueprint), newState))

    return sol

t = Time.time()

lines = sys.stdin.read().split('\n')[:-1]

blueprints = []
for l in lines:
    recipes = l.split('.')[:-1]

    ID = None
    bots = []
    for i, r in enumerate(recipes):
        r = r.strip()
        tokens = r.split()

        if (i == 0):
            ID = int(tokens[1][:-1])
            tokens = tokens[2:]

        l = [0, 0, 0, 0]
        for k in range(4, len(tokens), 3):
            l[MINERALS.index(tokens[k + 1])] = int(tokens[k])

        bots.append(tuple(l))

    blueprints.append(Blueprint(ID, bots))

q = 0
if (MAX_TIME == 32):
    blueprints = blueprints[:min(len(blueprints), 3)]
    q = 1

for b in blueprints:
    firstState = (0, ((0, 1), (0, 0), (0, 0), (0, 0)))

    t2 = Time.time()
    sol = mineGeodes(firstState, b)

    print(b.ID, sol)
    print('Time: {:.3f} s'.format(Time.time() - t2))
    if (MAX_TIME == 24):
        q += b.ID * sol
    else:
        q *= sol

print('Total time: {:.3f} s'.format(Time.time() - t))
print(q)
