#!/usr/bin/python3

import sys
from queue import Queue

def add(a, b):
    return (a[0] + b[0], a[1] + b[1])

def mult(c, a):
    return (c * a[0], c * a[1])

def gcd(a, b):
    if (b == 0):
        return a

    return gcd(b, a % b)

def lcm(a, b):
    return a // gcd(a, b) * b

def checkBlizzard(state):
    global blizzards
    t, cur = state

    for i, d in enumerate(DIRECTIONS):
        if (i % 2 == 0):
            cur2 = add(cur, mult(-(t % (M - 2)), d))

            a, b = cur2
            b = ((b - 1) % (M - 2) + (M - 2)) % (M - 2) + 1

            cur2 = (a, b)
        else:
            cur2 = add(cur, mult(-(t % (N - 2)), d))

            a, b = cur2
            a = ((a - 1) % (N - 2) + (N - 2)) % (N - 2) + 1

            cur2 = (a, b)

        if ((cur2, i) in blizzards):
            return True

    return False

def travel(T, start, end):
    q = Queue()
    q.put((T, start))
    visited = set([])
    while (not q.empty()):
        state = q.get()
        t, cur = state

        if (cur == end):
            return t

        state2 = (t % MOD, cur)
        if (state2 in visited):
            continue
        visited.add(state2)

        for d in DIRECTIONS:
            cur2 = add(cur, d)
            if (cur2 != start and \
                cur2 != end and \
                (cur2[0] <= 0 or \
                 cur2[0] >= N - 1 or \
                 cur2[1] <= 0 or \
                 cur2[1] >= M - 1)):
                continue

            if (cur2 == start or \
                cur2 == end or \
                not checkBlizzard((t + 1, cur2))):
                q.put((t + 1, cur2))

        if (cur == start or \
            not checkBlizzard((t + 1, cur))):
            q.put((t + 1, cur))

    return None

DIRECTIONS = [(0, 1), (1, 0), (0, -1), (-1, 0)]
SYMBOLS = '>v<^'

valley = sys.stdin.read().split('\n')[:-1]

N, M = len(valley), len(valley[0])

MOD = lcm(N - 2, M - 2)

start = (0, 1)
end = (N - 1, M - 2)

global blizzards
blizzards = set([])
for i in range(1, N - 1):
    for j in range(1, M - 1):
        if (valley[i][j] in SYMBOLS):
            blizzards.add(((i, j), SYMBOLS.index(valley[i][j])))

t = travel(0, start, end)
print(t)
t2 = travel(t, end, start)
t3 = travel(t2, start, end)
print(t3)
