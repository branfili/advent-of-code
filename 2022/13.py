#!/usr/bin/python3

import sys
import json

from functools import cmp_to_key

def compare(a, b):
    x = isinstance(a, list)
    y = isinstance(b, list)

    if (not x and not y):
        d = a - b
        if (d == 0):
            return d
        else:
            return (d // abs(d))
    elif (not x):
        a = [a]
    elif (not y):
        b = [b]

    for i in range(max(len(a), len(b))):
        if (i == len(a)):
            return -1
        elif (i == len(b)):
            return 1

        c = compare(a[i], b[i])
        if (c != 0):
            return c

    return 0

lines = sys.stdin.read().split('\n')

samples = []
for i in range(0, len(lines), 3):
    samples.append(json.loads(lines[i]))
    samples.append(json.loads(lines[i + 1]))

s = 0
for i in range(0, len(samples), 2):
    c = compare(samples[i], samples[i + 1])
    if (c == -1):
        s += ((i // 2) + 1)

print(s)

d1 = [[2]]
d2 = [[6]]

samples.append(d1)
samples.append(d2)

samples = sorted(samples, key = cmp_to_key(compare))

i1 = samples.index(d1) + 1
i2 = samples.index(d2) + 1

print(i1 * i2)
