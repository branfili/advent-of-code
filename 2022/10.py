#!/usr/bin/python3

import sys

lines = sys.stdin.read().split('\n')[:-1]

x = 1
i = 0

signalStrengths = [x]

for l in lines:
    tokens = l.split()

    if (tokens[0] == 'noop'):
        i += 1
    elif (tokens[0] == 'addx'):
        for j in range(1, 2):
            signalStrengths.append(x)

        x += int(tokens[1])
        i += 2

    signalStrengths.append(x)

s = 0
for i in range(20, 221, 40):
    s += i * signalStrengths[i - 1]

print(s)

im = ''
for i in range(240):
    if (signalStrengths[i] - 1 <= (i % 40) and (i % 40) <= signalStrengths[i] + 1):
        im += '#'
    else:
        im += ' '

for i in range(0, 240, 40):
    print(im[i:i + 40])
