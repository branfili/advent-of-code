#!/usr/bin/python3

import sys

lines = list(map(lambda s: s[:-1], sys.stdin.readlines()))

lines += ['']

s = 0
l = []
for x in lines:
    if (x == ''):
        l.append(s)
        s = 0
    else:
        s += int(x)

l = sorted(l, reverse = True)

print(sum(l[:3]))
