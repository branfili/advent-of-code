#!/usr/bin/python3

import sys

lines = sys.stdin.read().split('\n')[:-1]

x = lines.index('')

stl = len(lines[x - 1].split())
stacks = []

for i in range(stl):
    stacks.append([])

for i in range(x - 2, -1, -1):
    l = lines[i]
    for i in range(0, stl * 4, 4):
        if (l[i] == '['):
            stacks[i // 4].append(l[i + 1])

for l in lines[x + 1:]:
    tokens = l.split()

    x, a, b = map(int, [tokens[1], tokens[3], tokens[5]])

    a -= 1
    b -= 1

    stacks[b] += stacks[a][-x:]
    stacks[a] = stacks[a][:-x]

print(''.join(list(map(lambda l: l[-1], stacks))))
