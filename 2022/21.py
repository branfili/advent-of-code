#!/usr/bin/python3

import sys
import operator

class Node:
    def __init__(self, name, value, op = None, left = None, right = None):
        self.name = name
        self.value = value
        self.left = left
        self.right = right

        if (op == '+'):
            self.op = operator.add
        elif (op == '-'):
            self.op = operator.sub
        elif (op == '*'):
            self.op = operator.mul
        elif (op == '/'):
            self.op = operator.truediv
        else:
            self.op = None

    def calc_value(self):
        if (self.op is None):
            return self.value

        self.value = self.op(self.left.calc_value(), self.right.calc_value())

        return self.value

    def calc_expr(self):
        if (self.op is None):
            return ((1, 0) if (self.value is None) else (0, self.value))

        l = self.left.calc_expr()
        r = self.right.calc_expr()

        if (l[0] == 0 and r[0] == 0):
            return (0, self.value)

        op = ''
        if (self.op == operator.add):
            return (l[0] + r[0], l[1] + r[1])
        elif (self.op == operator.sub):
            return (l[0] - r[0], l[1] - r[1])
        elif (self.op == operator.mul):
            if (r[0] == 0):
                return (r[1] * l[0], r[1] * l[1])
            else:
                return (l[1] * r[0], l[1] * r[1])
        elif (self.op == operator.truediv):
            if (r[0] == 0):
                return (l[0] / r[1], l[1] / r[1])
            else:
                raise Exception
        elif (self.op == operator.eq):
            return (l, r)
        else:
            return (0, self.value)

lines = sys.stdin.read().split('\n')[:-1]

nodes = []
for l in lines:
    tokens = l.split()

    name = tokens[0][:-1]

    if (tokens[1].isdigit()):
        val = int(tokens[1])

        nodes.append(Node(name, val))
    else:
        left = tokens[1]
        right = tokens[3]
        op = tokens[2]

        nodes.append(Node(name, 0, op, left, right))

node_names = list(map(lambda n: n.name, nodes))

for n in nodes:
    if (n.op is not None):
        n.left = nodes[node_names.index(n.left)]
        n.right = nodes[node_names.index(n.right)]

root = nodes[node_names.index('root')]

print(root.calc_value())

humn = nodes[node_names.index('humn')]

root.op = operator.eq
humn.value = None

l, r = root.calc_expr()
if (l[0] == 0):
    l, r = r, l

print((r[1] - l[1]) / l[0])
