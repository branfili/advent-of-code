#!/usr/bin/python3

import sys
import copy
from queue import Queue

class Tile:
    def __init__(self, pos, isWall, facings, face = None):
        self.pos = copy.deepcopy(pos)
        self.isWall = isWall
        self.facings = copy.deepcopy(facings)
        self.face = face

    def move(self, f):
        global LOOKUP2

        cur2 = cur.facings[f]
        if (cur.face != cur2.face):
            f = LOOKUP2[(cur.face, f)]

        return cur2, f

def movePos(pos, d):
    global N, M

    pos2 = ((pos[0] + d[0] + N) % N, (pos[1] + d[1] + M) % M)
    return pos2

lines = sys.stdin.read().split('\n')[:-1]

jungle = lines[:-2]
instructions = lines[-1]

global N, M, DIRECTIONS
N, M = len(jungle), max(map(lambda j: len(j), jungle))
# [RIGHT, DOWN, LEFT, UP]
DIRECTIONS = [(0, 1), (1, 0), (0, -1), (-1, 0)]

IS_CUBE = True
CUBE_SIZE = 50

for i in range(N):
    if (len(jungle[i]) < M):
        jungle[i] += ' ' * (M - len(jungle[i]))

jungle = list(map(lambda j: list(j), jungle))

l = []
s = ''
for d in instructions:
    if (d.isdigit()):
        s += d
    else:
        l.append(int(s))
        l.append(d)
        s = ''

if (s != ''):
    l.append(int(s))

instructions = l

tiles = []
cur = None
for i in range(N):
    for j in range(M):
        if (jungle[i][j] == ' '):
            continue

        pos = (i, j)
        isWall = (jungle[pos[0]][pos[1]] == '#')
        facings = None

        if (not IS_CUBE):
            facings = []
            for d in DIRECTIONS:
                pos2 = movePos(pos, d)
                while (jungle[pos2[0]][pos2[1]] == ' '):
                    pos2 = movePos(pos2, d)

                facings.append(pos2)

        tiles.append(Tile(pos, isWall, facings))

tilePoss = list(map(lambda t: t.pos, tiles))

if (IS_CUBE):
    global CUBE, LOOKUP

    # [UP, FRONT, RIGHT, BACK, LEFT, DOWN]
    CUBE = {
        0: [2, 1, 4, 3],
        1: [2, 5, 4, 0],
        2: [3, 5, 1, 0],
        3: [4, 5, 2, 0],
        4: [1, 5, 3, 0],
        5: [2, 3, 4, 1]
    }

    # [RIGHT, DOWN, LEFT, UP]
    LOOKUP = {
        (0, 0): 1,
        (0, 1): 1,
        (0, 2): 1,
        (0, 3): 1,

        (1, 0): 0,
        (1, 1): 1,
        (1, 2): 2,
        (1, 3): 3,

        (2, 0): 0,
        (2, 1): 2,
        (2, 2): 2,
        (2, 3): 2,

        (3, 0): 0,
        (3, 1): 3,
        (3, 2): 2,
        (3, 3): 1,

        (4, 0): 0,
        (4, 1): 0,
        (4, 2): 2,
        (4, 3): 0,

        (5, 0): 3,
        (5, 1): 3,
        (5, 2): 3,
        (5, 3): 3
    }

    faces = copy.deepcopy(jungle)
    sections = []
    for i in range(6):
        sections.append(None)

    q = Queue()
    q.put((tiles[0].pos, 0, 0))
    visited = set([])

    while (not q.empty()):
        state = q.get()
        cur, f, d = state

        if (state in visited):
            continue

        visited.add(state)

        sections[f] = (cur, d)

        for i in range(CUBE_SIZE):
            for j in range(CUBE_SIZE):
                cur2 = (cur[0] + i, cur[1] + j)
                faces[cur2[0]][cur2[1]] = str(f)
                tiles[tilePoss.index(cur2)].face = f

        for i in range(len(DIRECTIONS)):
            cur2 = (cur[0] + DIRECTIONS[i][0] * CUBE_SIZE, cur[1] + DIRECTIONS[i][1] * CUBE_SIZE)

            if (cur2[0] < 0 or \
                cur2[0] >= N or \
                cur2[1] < 0 or \
                cur2[1] >= M or \
                faces[cur2[0]][cur2[1]] not in ['.', '#']):
                continue

            d2 = (LOOKUP[(f, (i - d + 4) % 4)] - (i - d + 4) + 4 + d) % 4
            f2 = CUBE[f][(d + i) % 4]

            q.put((cur2, f2, d2))

    LOOKUP2 = {}

    for x, t in enumerate(tiles):
        cur = t.pos
        f = t.face

        s, d2 = sections[f]

        facings = []
        for i, d in enumerate(DIRECTIONS):
            cur2 = (cur[0] + d[0], cur[1] + d[1])

            if (cur2[0] >= 0 and
                cur2[0] < N and
                cur2[1] >= 0 and
                cur2[1] < M and
                faces[cur2[0]][cur2[1]] != ' '):
                facings.append(cur2)

                if (f != int(faces[cur2[0]][cur2[1]])):
                    LOOKUP2[(f, i)] = i

                continue

            f2 = CUBE[f][(d2 + i) % 4]

            newStart = (s[0] + CUBE_SIZE * d[0], s[1] + CUBE_SIZE * d[1])

            rel = (cur2[0] - newStart[0], cur2[1] - newStart[1])
            dif = (CUBE[f2].index(f) - sections[f2][1] + 4 - (i + 2) + 4) % 4

            for _ in range(dif):
                rel = (rel[1], CUBE_SIZE - rel[0] - 1)

            cur2 = (rel[0] + sections[f2][0][0], rel[1] + sections[f2][0][1])
            LOOKUP2[(f, i)] = (i + dif) % 4

            facings.append(cur2)

        tiles[x].facings = copy.deepcopy(facings)

for t in tiles:
    for i in range(len(t.facings)):
        t.facings[i] = tiles[tilePoss.index(t.facings[i])]

cur = tiles[0]
f = 0
for i in instructions:
    if (i == 'L'):
        f = (f + 3) % 4
    elif (i == 'R'):
        f = (f + 1) % 4
    else:
        for j in range(i):
            cur2, f2 = cur.move(f)
            if (cur2.isWall):
                break

            cur = cur2
            f = f2

print((cur.pos[0] + 1) * 1000 + (cur.pos[1] + 1) * 4 + f)
