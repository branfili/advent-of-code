#!/usr/bin/python3

from collections import defaultdict

global HEIGHT, MAX_ROCKS
HEIGHT = 100000
MAX_ROCKS = 1000000000000 #2022

global rocks, jets
rocks = [['####'], ['.#.', '###', '.#.'], ['..#', '..#', '###'], ['#', '#', '#', '#'], ['##', '##']]
jets = input()

global chamber
chamber = []
for i in range(HEIGHT):
    chamber.append('.' * 7)

def combine(s1, s2):
    s = ''
    for a, b in zip(s1, s2):
        s += ('#' if (a == '#' or b == '#') else '.')
    return s

def remove(s1, s2):
    s = ''
    for a, b in zip(s1, s2):
        if (a == '#'):
            s += '.'
        else:
            s += b
    return s

def writeRock(x, y, rock):
    for i in range(len(rock)):
        chamber[i + x] = chamber[i + x][:y] + combine(rock[i], chamber[i + x][y:y + len(rock[i])]) + chamber[i + x][y + len(rock[i]):]

def delRock(x, y, rock):
    for i in range(len(rock)):
        chamber[i + x] = chamber[i + x][:y] + remove(rock[i], chamber[i + x][y:y + len(rock[i])]) + chamber[i + x][y + len(rock[i]):]

def canMove(rock, x, y, dx, dy):
    for i in range(len(rock)):
        for j in range(len(rock[i])):
            if (rock[i][j] == '#' and
                (i + dx == len(rock) or
                 i + dx == -1 or
                 j + dy == len(rock[i]) or
                 j + dy == -1 or
                 rock[i + dx][j + dy] == '.') and
                chamber[i + x + dx][j + y + dy] == '#'):
                return False

    return True

tail = None
curRock = 0
curJet = 0
highestRock = HEIGHT

visited = defaultdict(lambda: 0)
sol = defaultdict(lambda: None)
sol[0] = 0

while (curRock < MAX_ROCKS):
    rock = rocks[curRock % len(rocks)]

    x = highestRock - 3 - len(rock)
    y = 2
    writeRock(x, y, rock)

    state = (curRock % len(rocks), curJet % len(jets))
    visited[state] += 1

    if (visited[state] == 2):
        if (tail is None):
            tail = curRock
    elif (visited[state] == 3):
        break

    while True:
        jet = jets[curJet % len(jets)]
        curJet += 1

        if (jet == '>'):
            if (y + len(rock[0]) < 7 and
                canMove(rock, x, y, 0, 1)):
                delRock(x, y, rock)
                y += 1
                writeRock(x, y, rock)
        elif (jet == '<'):
            if (y > 0 and
                canMove(rock, x, y, 0, -1)):
                delRock(x, y, rock)
                y -= 1
                writeRock(x, y, rock)

        if (x + len(rock) == HEIGHT or
            not canMove(rock, x, y, 1, 0)):
            break

        delRock(x, y, rock)
        x += 1
        writeRock(x, y, rock)

    while (highestRock == HEIGHT or
           '#' in chamber[highestRock]):
        highestRock -= 1
    highestRock += 1

    curRock += 1
    sol[curRock] = HEIGHT - highestRock

a = sol[tail]
N = curRock - tail

D, M = (MAX_ROCKS - tail) // N, (MAX_ROCKS - tail) % N

a = sol[tail]
x = sol[curRock] - a
b = sol[tail + M] - a

print (a + D * x + b)
