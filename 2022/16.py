#!/usr/bin/python3

import sys
import copy
import operator
import time as Time
from functools import reduce
from collections import defaultdict
from queue import PriorityQueue

t = Time.time()

global valves, valve_indices
valves = []
valve_indices = {}

ACTORS = 2
MAX_TIME = 34 - 4 * ACTORS

lines = sys.stdin.read().split('\n')[:-1]
all_opened = set([])
flow_valves = []

class Valve:
    def __init__(self, name, flow, pipes = []):
        self.name = name
        self.flow = flow
        self.pipes = copy.deepcopy(pipes)

def calcPressure(history, t):
    s = 0
    for i, tp in enumerate(history):
        if (tp >= t):
            continue

        v = valves[i]
        s += (t - tp) * v.flow

    return s

for i, l in enumerate(lines):
    l += ','
    tokens = l.split()

    name = tokens[1]
    flow = int(tokens[4][:-1].split('=')[1])
    pipes = list(map(lambda t: t[:-1], tokens[9:]))

    valves.append(Valve(name, flow, pipes))

valves = sorted(valves, key = lambda v: v.name)
for i, v in enumerate(valves):
    valve_indices[v.name] = i

    if (v.flow > 0):
        all_opened.add(i)

A = valve_indices['AA']
all_opened.add(A)

for v in valves:
    for i, p in enumerate(v.pipes):
        v.pipes[i] = valves[valve_indices[p]]

min_dist = []
for i in range(len(valves)):
    l = []
    for j in range(len(valves)):
        l.append(10 ** 12)
    min_dist.append(l)

for i, v in enumerate(valves):
    for p in v.pipes:
        j = valve_indices[p.name]
        min_dist[i][j] = 1
        min_dist[j][i] = 1

for k in range(len(valves)):
    for i in range(len(valves)):
        for j in range(len(valves)):
            min_dist[i][j] = min(min_dist[i][j],
                                 min_dist[i][k] + min_dist[k][j])

pq = PriorityQueue()
visited = set([])

last = []
for a in range(ACTORS):
    last.append((0, A))

l = []
for i in range(len(valves)):
    if (i == A):
        l.append(0)
    else:
        l.append(MAX_TIME)

pq.put((0, 0, tuple(l), tuple(last), 0))
sol = 0
maxHistory = None
while (not pq.empty()):
    state = pq.get()
    #print(state)

    _, time, history, last, actor = state

    state2 = (history, last)
    if (state2 in visited):
        continue

    visited.add(state2)

    opened = set(map(lambda t: t[0], filter(lambda t: t[1] < MAX_TIME, enumerate(history))))

    remainingOpened = all_opened - opened

    x = last[actor][1]
    for v in remainingOpened:
        newValve = (time + min_dist[x][v] + 1, v)

        newHistory = list(copy.deepcopy(history))
        newHistory[v] = newValve[0]
        newHistory = tuple(newHistory)

        newLast = list(copy.deepcopy(last))
        newLast[actor] = newValve
        newLast = tuple(newLast)

        newActor = (actor + 1) % ACTORS
        newTime = newLast[newActor][0]

        if (len(remainingOpened) == 1):
            maxFlow = 0
            minDistance = 0
            N = 0
            newTime = MAX_TIME
            newActor = None
        else:
            maxFlow = max(map(lambda v2: valves[v2].flow, remainingOpened - set([v])))
            minDistance = min(filter(lambda v2: v2 in (remainingOpened - set([v])), range(len(valves))))
            N = int(sum(map(lambda t: MAX_TIME - t[0], last)) / (ACTORS * minDistance))

        benefit = calcPressure(newHistory, MAX_TIME) + maxFlow * minDistance * N * (N - 1) / 2

        newState = (-benefit, newTime, newHistory, newLast, newActor)

        if (newTime >= MAX_TIME):
            p = calcPressure(newHistory, MAX_TIME)

            if (p > sol):
                sol = p
                maxHistory = newHistory

                print(sol)

                l = []
                for i, v in enumerate(valves):
                    if (maxHistory[i] < MAX_TIME):
                        l.append((maxHistory[i], v.name))
                l = sorted(l, key = lambda t: t[0])
                print(l)

                print('{:.3f} s'.format(Time.time() - t))
                print()

            continue

        pq.put(newState)
