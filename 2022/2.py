#!/usr/bin/python3

import sys

lines = sys.stdin.read().split('\n')[:-1]

opp = "ABC"
me = "XYZ"

s = 0
for l in lines:
    o, m = l.split(' ')

    o = opp.index(o)
    m = me.index(m)

    m = (o + m - 1) % 3

    s += (m + 1) + 3 * ((m - o + 1) % 3)

print(s)
