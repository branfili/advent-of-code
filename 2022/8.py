#!/usr/bin/python3

import sys
from functools import reduce
import operator

trees = sys.stdin.read().split('\n')[:-1]

trees = list(map(lambda treeline: list(map(int, treeline)), trees))

dirs = [(0, 1), (-1, 0), (0, -1), (1, 0)]

visible = set([])
m = 0

for i in range(len(trees)):
    for j in range(len(trees[i])):
        flag = False
        views = []
        for d in dirs:
            flag2 = True

            cur = [i + d[0], j + d[1]]
            x = 1
            while (cur[0] >= 0 and
                   cur[0] < len(trees) and
                   cur[1] >= 0 and
                   cur[1] < len(trees[i])):

                if (trees[cur[0]][cur[1]] >= trees[i][j]):
                    flag2 = False
                    break

                cur[0] += d[0]
                cur[1] += d[1]
                x += 1

            if (flag2):
                x -= 1

            views.append(x)
            flag = flag2 or flag

        if (flag):
            visible.add((i, j))

        m = max(m, reduce(operator.mul, views, 1))

print(len(visible))
print(m)
