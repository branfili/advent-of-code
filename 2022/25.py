#!/usr/bin/python3

import sys

def fromSNAFU(s):
    global DIGITS
    s = list(reversed(s))
    n, p = 0, 1
    for c in s:
        n += p * (DIGITS.index(c) - 2)
        p *= 5
    return n

def toSNAFU(x):
    global DIGITS
    s = ''
    while (x > 0):
        s += DIGITS[(x % 5 + 2) % 5]
        x -= (x % 5 + 2) % 5 - 2
        x //= 5
    s = ''.join(list(reversed(s)))
    return s

global DIGITS
DIGITS = '=-012'

lines = sys.stdin.read().split('\n')[:-1]

s = 0
for l in lines:
    s += fromSNAFU(l)

s = toSNAFU(s)
print(s)
