#!/usr/bin/python3

import sys

lines = sys.stdin.read().split('\n')[:-1]

MAXX, MAXY = 700, 300

cave = []
for i in range(MAXX):
    l = []
    for j in range(MAXY):
        l.append(0)

    cave.append(l)

floor = 0
for l in lines:
    tokens = l.split(' -> ')
    for i in range(0, len(tokens) - 1):
        x1, y1 = map(int, tokens[i].split(','))
        x2, y2 = map(int, tokens[i + 1].split(','))

        if (y1 == y2):
            floor = max(floor, y1)
            for x in range(min(x1, x2), max(x1, x2) + 1):
                cave[x][y1] = 1

        elif (x1 == x2):
            floor = max(floor, max(y1, y2))
            for y in range(min(y1, y2), max(y1, y2) + 1):
                cave[x1][y] = 1

floor += 2
for i in range(MAXX):
    cave[i][floor] = 1

s = 0
while True:
    cur = (500, 0)
    if (cave[cur[0]][cur[1]] == 1):
        break

    #flag = False

    while True:
        #if (cur[1] == MAXY - 1):
        #    flag = True
        #    break

        if (cave[cur[0]][cur[1] + 1] == 0):
            cur = (cur[0], cur[1] + 1)
        elif (cave[cur[0] - 1][cur[1] + 1] == 0):
            cur = (cur[0] - 1, cur[1] + 1)
        elif (cave[cur[0] + 1][cur[1] + 1] == 0):
            cur = (cur[0] + 1, cur[1] + 1)
        else:
            cave[cur[0]][cur[1]] = 1
            break

    #if (flag):
    #    break

    s += 1

print(s)
