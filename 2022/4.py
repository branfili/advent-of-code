#!/usr/bin/python3

import sys

lines = sys.stdin.read().split('\n')[:-1]

s = 0
for l in lines:
    i1, i2 = l.split(',')

    x1, y1 = map(int, i1.split('-'))
    x2, y2 = map(int, i2.split('-'))

    if (x2 < x1):
        x1, x2 = x2, x1
        y1, y2 = y2, y1
    elif (x2 == x1):
        if ((y2 - x2) > (y1 - x1)):
            x1, x2 = x2, x1
            y1, y2 = y2, y1

    if (x1 <= x2 and x2 <= y1):
        s += 1

print(s)
