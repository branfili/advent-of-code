#!/usr/bin/python3

import sys
import copy
from math import inf

class File:
    def __init__(self, name, size = 0, contents = [], parent = None):
        self.name = name
        self.size = size
        self.parent = parent

        self.contents = []
        for c in contents:
            self.add_file(c)

    def add_file(self, newf):
        self.contents.append(newf)
        newf.parent = self

    def find_file(self, name):
        if (name == '..'):
            return self.parent

        for f in self.contents:
            if (f.name == name):
                return f

        return None

    def calc_size(self):
        if (not self.contents):
            return self.size

        s = 0
        for f in self.contents:
            s += f.calc_size()

        self.size = s
        return s

def find_all(f, max_size = inf):
    if (not f.contents):
        return 0

    s = (f.size if (f.size < max_size) else 0)
    for sub in f.contents:
        s += find_all(sub, max_size = max_size)

    return s

def find_smallest(f, min_needed = 0):
    if (not f.contents):
        return -1

    l = []
    for sub in f.contents:
        x = find_smallest(sub, min_needed = min_needed)
        if (x >= min_needed):
            l.append(x)

    return (f.size if (not l) else min(l))

lines = sys.stdin.read().split('\n')[:-1]

i = 0
curf = None
while (i < len(lines)):
    l = lines[i]

    if (l == '$ ls'):
        j = i + 1
        while (j < len(lines) and lines[j][0] != '$'):
            j += 1

        l2 = lines[i + 1:j]

        for l in l2:
            s, name = l.split()

            if (s.isdigit()):
                curf.add_file(File(name, size = int(s)))
            else:
                curf.add_file(File(name))

        i = j
    elif (l.startswith('$ cd')):
        name = l[5:]

        if (curf is None):
            root = File(name)
            curf = root
        else:
            curf = curf.find_file(name)

        i += 1

root.calc_size()

m = root.size - 40000000

print(find_all(root, max_size = 100000))
print(find_smallest(root, min_needed = m))
