#!/usr/bin/python3

import sys

lines = sys.stdin.read().split('\n')[:-1]

s = 0
for i in range(0, len(lines), 3):
    l1, l2, l3 = lines[i], lines[i + 1], lines[i + 2]

    x = list(set(l1).intersection(set(l2)).intersection(set(l3)))[0]

    if ('a' <= x and x <= 'z'):
        s += (ord(x) - ord('a')) + 1
    else:
        s += (ord(x) - ord('A')) + 27

print(s)
