#!/usr/bin/python3

import sys
import copy
from queue import PriorityQueue

def manhattan(a, b):
    s = 0
    for d in range(len(a)):
        s += abs(a[d] - b[d])
    return s

def existsPath(a, b):
    pq = PriorityQueue()
    pq.put((0, a))
    visited = set([])

    while (not pq.empty()):
        _, x = pq.get()

        if (x in visited):
            continue

        visited.add(x)

        if (x == b):
            break

        for d in range(3):
            for dx in [-1, 1]:
                y = list(copy.deepcopy(x))
                y[d] += dx
                y = tuple(y)

                if (y in droplet):
                    continue

                pq.put((manhattan(y, b), y))

    return (not pq.empty(), visited)

lines = sys.stdin.read().split('\n')[:-1]

mx, my, mz = 10 ** 12, 10 ** 12, 10 ** 12

global droplet
droplet = set([])
for l in lines:
    x, y, z = map(int, l.split(','))
    droplet.add((x, y, z))
    mx = min(mx, x)
    my = min(my, y)
    mz = min(mz, z)

air = (mx - 1, my - 1, mz - 1)

steam = set([])
for c in droplet:
    for d in range(3):
        for dx in [-1, 1]:
            c2 = list(copy.deepcopy(c))
            c2[d] += dx
            c2 = tuple(c2)

            if (c2 not in droplet):
                steam.add(c2)

bubbles = set([])
for c in steam:
    if (c in bubbles):
        continue

    p, b = existsPath(c, air)

    if (not p):
        bubbles |= b

steam -= bubbles

s = 0
for c in droplet:
    for d in range(3):
        for dx in [-1, 1]:
            c2 = list(copy.deepcopy(c))
            c2[d] += dx
            c2 = tuple(c2)

            if (c2 in steam):
                s += 1

print(s)
