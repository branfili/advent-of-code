#!/usr/bin/python3

import sys
import copy

class Sensor:
    def __init__(self, pos, beacon):
        self.pos = copy.deepcopy(pos)
        self.beacon = copy.deepcopy(beacon)
        self.min_dist = self.calc_dist(beacon)

    def calc_dist(self, x):
        return (abs(self.pos[0] - x[0]) + abs(self.pos[1] - x[1]))

lines = sys.stdin.read().split('\n')[:-1]

sensors = []
for l in lines:
    l += '.'
    tokens = l.split()

    x, y, bx, by = map(lambda t: int(t[:-1].split('=')[1]), [tokens[2], tokens[3], tokens[-2], tokens[-1]])

    sensors.append(Sensor(pos = (x, y), beacon = (bx, by)))

for SCANLINE in range(4000000):#[2000000]
    intervals = []
    bs = set([])
    for s in sensors:
        d = s.min_dist - abs(s.pos[1] - SCANLINE)
        if (d < 0):
            continue

        intervals.append((s.pos[0] - d, s.pos[0] + d + 1))

        if (s.beacon[1] == SCANLINE):
            bs.add(s.beacon)

    intervals = sorted(intervals)
    intervals2 = [intervals[0]]
    for i in range(1, len(intervals)):
        x1, y1 = intervals2[-1]
        x2, y2 = intervals[i]

        if (max(x1, x2) <= min(y1, y2)):
            intervals2[-1] = (min(x1, x2), max(y1, y2))
        else:
            intervals2.append(intervals[i])

    intervals = intervals2

    sol = 0
    for x, y in intervals:
        sol += (y - x)

    sol -= len(bs)

    if (len(intervals) == 2):
        x = intervals[0][1]
        y = SCANLINE
        print(x, y)
        print(x * 4000000 + y)
        break

#print(sol)
