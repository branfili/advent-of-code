#!/usr/bin/python3

import sys
from queue import Queue

DIRS = {(0, 1), (-1, 0), (0, -1), (1, 0)}

myMap = list(map(list, sys.stdin.read().split('\n')[:-1]))

start = None
end = None

for i in range(len(myMap)):
    for j in range(len(myMap[i])):
        if (myMap[i][j] == 'S'):
            start = (i, j)
            myMap[i][j] = 'a'
        elif (myMap[i][j] == 'E'):
            end = (i, j)
            myMap[i][j] = 'z'

q = Queue()
dists = {}

#q.put((start, 0))
for i in range(len(myMap)):
    for j in range(len(myMap[i])):
        if (myMap[i][j] == 'a'):
            q.put(((i, j), 0))

while (not q.empty()):
    cur, x = q.get()

    if (cur in dists.keys()):
        continue

    dists[cur] = x

    for d in DIRS:
        nCur = (cur[0] + d[0], cur[1] + d[1])

        if (nCur[0] < 0 or
            nCur[0] >= len(myMap) or
            nCur[1] < 0 or
            nCur[1] >= len(myMap[i])):
            continue

        dif = ord(myMap[nCur[0]][nCur[1]]) - ord(myMap[cur[0]][cur[1]])

        if (dif <= 1):
            q.put((nCur, x + 1))

print(dists[end])
