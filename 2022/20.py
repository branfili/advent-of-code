#!/usr/bin/python3

import sys
import copy

class Node:
    def __init__(self, index = -1, value = None):
        self.value = value
        self.index = index
        self.next = None
        self.prev = None

    def move(self, x, calc = False):
        useNext = (x > 0)
        x = abs(x)

        global N
        x %= (N - (0 if calc else 1))

        sol = self
        for i in range(x):
            if (useNext):
                sol = sol.next
            else:
                sol = sol.prev

        return sol

    def insert(a, x):
        if (x == 0):
            return

        useNext = (x > 0)

        b = a.move(x)

        a.next.prev = a.prev
        a.prev.next = a.next

        if (useNext):
            a.next = b.next
            a.next.prev = a

            b.next = a
            b.next.prev = b

        else:
            a.prev = b.prev
            a.prev.next = a

            b.prev = a
            b.prev.next = b

def printNodes(n):
    n2 = copy.copy(n)

    l = [n2.value]
    n2 = n2.next
    while (n2.value != n.value):
        l.append(n2.value)
        n2 = n2.next

    print(l)

KEY = 811589153 # 1
MIXINGS = 10 # 1

coords = list(map(int, sys.stdin.read().split('\n')[:-1]))
coords = list(map(lambda c: c * KEY, coords))

global N
nodes = []
for i, c in enumerate(coords):
    n = Node(i, c)
    nodes.append(n)

start = nodes[list(map(lambda n: n.value, nodes)).index(0)]
N = len(nodes)

for i in range(N):
    nodes[i].next = nodes[(i + 1) % N]
    nodes[i].prev = nodes[(i - 1) % N]

for _ in range(MIXINGS):
    for i, c in enumerate(coords):
        if (c == 0):
            continue

        sol = nodes[list(map(lambda n: n.index, nodes)).index(i)]

        Node.insert(sol, c)

x = start.move(1000, True).value
y = start.move(2000, True).value
z = start.move(3000, True).value

print(x + y + z)
