#include <iostream>
#include <cstdlib>
#include <utility>
#include <vector>

using namespace std;

int a[2000][2000];
string s;

vector<pair<pair<int, int>, pair<int, int> > > v;

int uintaj(string s){
  int z = (s[0] - '0');
  for (int i = 1; i < (int) s.size(); i++){
    z *= 10;
    z += (s[i] - '0');
  }
  return z;
}

pair<int, int> split(string s, char c){
  int i;
  for (i = 0; s[i] != c; i++);

  return make_pair(uintaj(s.substr(0, i)), uintaj(s.substr(i + 1)));
}

int main (void){
  while (getline(cin, s)){
    int i;
    for (i = 0; s[i] != '@'; i++);
    i += 2;

    int j;
    for (j = i; s[j] != ':'; j++);
    j += 2;

    pair<int, int> x = split(s.substr(i, j - i - 2), ',');
    pair<int, int> y = split(s.substr(j), 'x');

    v.push_back(make_pair(x, y));

    int id = uintaj(s.substr(1, i - 4));

    for (int k = x.second; k < x.second + y.second; k++){
      for (int l = x.first; l < x.first + y.first; l++){
        if (a[k][l] != 0){
          a[k][l] = -1;
          continue;
        }
        a[k][l] = id;
      }
    }
  }

  int sol = 0;
  bool e;

  for (int i = 0; i < (int) v.size(); i++){
    e = true;
    for (int j = v[i].first.second; j < v[i].first.second + v[i].second.second; j++){
      for (int k = v[i].first.first; k < v[i].first.first + v[i].second.first; k++){
        if (a[j][k] == -1){
          e = false;
          break;
        }
      }
      if (!e){
        break;
      }
    }

    if (e){
      sol = a[v[i].first.second][v[i].first.first];
    }
  }
  cout << sol << endl;
  return 0;
}
