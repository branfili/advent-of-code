#include <iostream>
#include <vector>
#include <utility>

using namespace std;

int x;

vector <int> v;

pair<int, int> tree(int x){
  int n, m;
  n = v[x];
  m = v[x + 1];

  x += 2;

  vector <int> tmp2;
  tmp2.clear();

  for (int i = 0; i < n; i++){
    pair<int, int> tmpp = tree(x);
    x = tmpp.second;
    tmp2.push_back(tmpp.first);
  }

  int z = 0;
  for (int j = 0; j < m; j++){
    if (n == 0){
      z += v[x];
    }
    else{
      v[x]--;
      if (v[x] < (int) tmp2.size()){
        z += tmp2[v[x]];
      }
    }
    x++;
  }

  return make_pair(z, x);
}

int main (void){
  while (cin >> x){
    v.push_back(x);
  }

  pair<int, int> tmp = tree(0);

  cout << tmp.first << endl;
  return 0;
}
