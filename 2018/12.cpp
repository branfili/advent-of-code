#include <iostream>
#include <map>

using namespace std;
const int MAXN = 500;
const long long MAXG = 130; //extrapolate to 5e10
string s, s2, s3;

char c[1 << 5];

char pot[MAXN];

int count(void){
  int z = 0;
  for (int i = 0; i < MAXN; i++){
    z += (pot[i] == '#' ? i - MAXN / 2 : 0);
  }
  return z;
}

int from_binary(string s){
  int z = (s[0] == '#');
  for (int i = 1; i < (int) s.size(); i++){
    z *= 2;
    z += (s[i] == '#');
  }
  return z;
}

string get_substr(int x){
  string s2 = "";
  for (int i = x - 2; i <= x + 2; i++){
    s2 += pot[i];
  }
  return s2;
}

int main(void){
  for (int i = 0; i < (1 << 5); i++){
    c[i] = '.';
  }

  for (int i = 0; i < MAXN; i++){
    pot[i] = '.';
  }

  cin >> s;

  for (int i = 0; i < (int) s.size(); i++){
    pot[i + MAXN / 2] = s[i];
  }
  
  while (cin >> s >> s2 >> s3){
    c[from_binary(s)] = s3[0];
  }

  for (long long i = 1; i <= MAXG; i++){
    s = "";
    for (int j = 2; j < MAXN - 2; j++){
      s += c[from_binary(get_substr(j))];
    }

    for (int j = 0; j < (int) s.size(); j++){
      pot[j + 2] = s[j];
    }

    cout << i << ' ' << count() << endl;
    for (int i = 0; i < MAXN; i++){
      cout << pot[i];
    }
    cout << endl;
  }

  cout << count() << endl;
  return 0;
}
