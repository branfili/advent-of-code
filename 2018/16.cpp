#include <iostream>
#include <vector>
#include <cstring>

using namespace std;
string s, s2, s3, s4;

string op[16] = {"addr", "addi", "mulr", "muli",
                 "banr", "bani", "borr", "bori",
                 "setr", "seti", "gtir", "gtri",
                 "gtrr", "eqir", "eqri", "eqrr"};

int opcode[16];

vector<int> pos[16];

int reg[] = {0, 0, 0, 0};

int uintaj(string s){
  int z = (s[0] - '0');
  for (int i = 1; i < (int) s.size(); i++){
    z *= 10;
    z += (s[i] - '0');
  }
  return z;
}

vector<int> split(string s, string c){
  vector<int> v;
  
  s += c;
  while (s.size() > 0){
    int i;
    for (i = 0; i < (int)(s.size() - c.size()) && s.substr(i, c.size()) != c; i++);

    v.push_back(uintaj(s.substr(0, i)));
    s = s.substr(i + c.size());
  }

  return v;
}

void intersect(vector<int>& a, vector<int>& b){
  vector<int> c;
  c.clear();

  for (int i = 0; i < (int) a.size(); i++){
    for (int j = 0; j < (int) b.size(); j++){
      if (a[i] == b[j]){
        c.push_back(a[i]);
        break;
      }
    }
  }

  a = c;
  return ;
}

int main (void){
  memset(opcode, -1, sizeof(opcode));

  for (int i = 0; i < 16; i++){
    for (int j = 0; j < 16; j++){
      pos[i].push_back(j);
    }
  }

  while (getline(cin, s)){
    if (s.substr(0, 6) != "Before"){
      if (opcode[0] == -1){
        bool e = true;
        while (e){
          e = false;
          for (int i = 0; i < 16; i++){
            if (pos[i].size() == 1){
              e = true;
              opcode[i] = pos[i][0];
              
              for (int j = 0; j < 16; j++){
                vector<int> c;
                c.clear();

                for (int k = 0; k < (int) pos[j].size(); k++){
                  if (pos[j][k] != opcode[i]){
                    c.push_back(pos[j][k]);
                  }
                }

                pos[j] = c;
              }
            }
          }
        }
      }
      
      vector<int> v = split(s, " ");
    
      if (op[opcode[v[0]]] == "addr"){
        reg[v[3]] = (reg[v[1]] + reg[v[2]]);
      }
      else if (op[opcode[v[0]]] == "addi"){
        reg[v[3]] = (reg[v[1]] + v[2]);
      }
      else if (op[opcode[v[0]]] == "mulr"){
        reg[v[3]] = (reg[v[1]] * reg[v[2]]);
      }
      else if (op[opcode[v[0]]] == "muli"){
        reg[v[3]] = (reg[v[1]] * v[2]);
      }
      else if (op[opcode[v[0]]] == "banr"){
        reg[v[3]] = (reg[v[1]] & reg[v[2]]);
      }
      else if (op[opcode[v[0]]] == "bani"){
        reg[v[3]] = (reg[v[1]] & v[2]);
      }
      else if (op[opcode[v[0]]] == "borr"){
        reg[v[3]] = (reg[v[1]] | reg[v[2]]);
      }
      else if (op[opcode[v[0]]] == "bori"){
        reg[v[3]] = (reg[v[1]] | v[2]);
      }
      else if (op[opcode[v[0]]] == "setr"){
        reg[v[3]] = reg[v[1]];
      }
      else if (op[opcode[v[0]]] == "seti"){
        reg[v[3]] = v[1];
      }
      else if (op[opcode[v[0]]] == "gtri"){
        reg[v[3]] = (reg[v[1]] > v[2]);
      }
      else if (op[opcode[v[0]]] == "gtir"){
        reg[v[3]] = (v[1] > reg[v[2]]);
      }
      else if (op[opcode[v[0]]] == "gtrr"){
        reg[v[3]] = (reg[v[1]] > reg[v[2]]);
      }
      else if (op[opcode[v[0]]] == "eqri"){
        reg[v[3]] = (reg[v[1]] == v[2]);
      }
      else if (op[opcode[v[0]]] == "eqir"){
        reg[v[3]] = (v[1] == reg[v[2]]);
      }
      else if (op[opcode[v[0]]] == "eqrr"){
        reg[v[3]] = (reg[v[1]] == reg[v[2]]);
      }
    }
    else{
      getline(cin, s2);
      getline(cin, s3);
      getline(cin, s4);

      s = s.substr(9, s.size() - 10);
      s3 = s3.substr(9, s3.size() - 10);

      vector<int> v = split(s, ", ");
      vector<int> v2 = split(s2, " ");
      vector<int> v3 = split(s3, ", ");
    
      int z = 0;
      vector<int> npos;
      npos.clear();
      for (int i = 0; i < 16; i++){
        vector <int> v4 = v;
        
        if (op[i] == "addr"){
          v4[v2[3]] = (v4[v2[1]] + v4[v2[2]]);
        }
        else if (op[i] == "addi"){
          v4[v2[3]] = (v4[v2[1]] + v2[2]);
        }
        else if (op[i] == "mulr"){
          v4[v2[3]] = (v4[v2[1]] * v4[v2[2]]);
        }
        else if (op[i] == "muli"){
          v4[v2[3]] = (v4[v2[1]] * v2[2]);
        }
        else if (op[i] == "banr"){
          v4[v2[3]] = (v4[v2[1]] & v4[v2[2]]);
        }
        else if (op[i] == "bani"){
          v4[v2[3]] = (v4[v2[1]] & v2[2]);
        }
        else if (op[i] == "borr"){
          v4[v2[3]] = (v4[v2[1]] | v4[v2[2]]);
        }
        else if (op[i] == "bori"){
          v4[v2[3]] = (v4[v2[1]] | v2[2]);
        }
        else if (op[i] == "setr"){
          v4[v2[3]] = v4[v2[1]];
        }
        else if (op[i] == "seti"){
          v4[v2[3]] = v2[1];
        }
        else if (op[i] == "gtri"){
          v4[v2[3]] = (v4[v2[1]] > v2[2]);
        }
        else if (op[i] == "gtir"){
          v4[v2[3]] = (v2[1] > v4[v2[2]]);
        }
        else if (op[i] == "gtrr"){
          v4[v2[3]] = (v4[v2[1]] > v4[v2[2]]);
        }
        else if (op[i] == "eqri"){
          v4[v2[3]] = (v4[v2[1]] == v2[2]);
        }
        else if (op[i] == "eqir"){
          v4[v2[3]] = (v2[1] == v4[v2[2]]);
        }
        else if (op[i] == "eqrr"){
          v4[v2[3]] = (v4[v2[1]] == v4[v2[2]]);
        }

        if (v4 == v3){
          npos.push_back(i);
        }
        z += (v4 == v3);
      }

      intersect(pos[v2[0]], npos);
    }
  }

  cout << reg[0] << endl;
  return 0;
}
