#include <iostream>
#include <algorithm>

using namespace std;
const int MAXN = 1000;
string s;

int px, py;

int d[MAXN][MAXN];

int sol;

int smjx[] = {-1, 0, 1, 0};
int smjy[] = {0, 1, 0, -1};

int fnd(char c){
  switch(c){
    case 'N':
      return 0;
    case 'E':
      return 1;
    case 'S':
      return 2;
    case 'W':
      return 3;
    default:
      return -1;
  }
}

void few(string s, int x, int y){
  int i;
  for (i = 0; i < (int) s.size() && s[i] != '('; i++){
    int nx = x + smjx[fnd(s[i])];
    int ny = y + smjy[fnd(s[i])];
  
    d[nx][ny] = min(d[nx][ny], d[x][y] + 1);
  
    x = nx;
    y = ny;
  }

  px = x;
  py = y;
  
  if (i == (int) s.size()){
    return ;
  }

  int j = i + 1;
  int d = 1;
  for (;; j++){
    if (s[j] == '('){
      d++;
    }
    else if (s[j] == ')'){
      d--;
    }

    if (d == 0){
      break;
    }
  }

  string s2 = s.substr(i + 1, j - i - 1);

  while (true){
    int k;
    int d = 0;
    for (k = 0; k < (int) s2.size(); k++){
      if (s2[k] == '('){
        d++;
      }
      else if (s2[k] == ')'){
        d--;
      }
      else if (d == 0 && s2[k] == '|'){
        break;
      }
    }

    few(s2.substr(0, k), x, y);
    
    if (k == (int) s2.size()){
      break;
    }
    
    s2 = s2.substr(k + 1);
  }

  if (j < (int) s.size() - 1){
    few(s.substr(j + 1), px, py);
  }

  return ;  
}

int main (void){
  cin >> s;
  s = s.substr(1, s.size() - 2);

  for (int i = 0; i < MAXN; i++){
    for (int j = 0; j < MAXN; j++){
      d[i][j] = 1e9;
    }
  }

  d[MAXN / 2][MAXN / 2] = 0;
  few(s, MAXN / 2, MAXN / 2);

  for (int i = 0; i < MAXN; i++){
    for (int j = 0; j < MAXN; j++){
      if (d[i][j] == 1e9){
        continue;
      }

      sol += (d[i][j] >= 1000);
    }
  }

  cout << sol << endl;
  return 0;
}
