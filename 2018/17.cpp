#include <iostream>

using namespace std;
string s, s2;

int mnx, mxx;

int sol;

int map[2000][2000];
bool bio[2000][2000];

int uintaj(string s){
  int z = (s[0] - '0');
  for (int i = 1; i < (int) s.size(); i++){
    z *= 10;
    z += (s[i] - '0');
  }
  return z;
}

int get(int x, int y){
  if (x < 0 ||
      x >= 2000 ||
      y < 0 ||
      y >= 2000){
    return -1;
  }

  return map[x][y];
}

bool check(int x, int y){
  return (get(x, y) == 1 ||
          get(x, y) == 3);
}

void flow(int x, int y){
  if (x < 0 ||
      x >= 2000 ||
      y < 0 ||
      y >= 2000 ||
      bio[x][y] ||
      map[x][y] == 1){
    return ;
  }

  bio[x][y] = true;
  map[x][y] = 2;

  if (x == 1999){
    return ;
  }

  if (map[x + 1][y] == 0){
    flow(x + 1, y);

    if (map[x + 1][y] == 3){
      if (!check(x + 1, y - 1) || !check(x + 1, y + 1)){
        for (int i = y; i < 2000 && map[x + 1][i] > 1; i++){
          map[x + 1][i] = 2;
        }

        for (int i = y; i >= 0 && map[x + 1][i] > 1; i--){
          map[x + 1][i] = 2;
        }

        return ;
      }
      
      flow(x, y - 1);
      flow(x, y + 1);

      if ((check(x, y - 1) && check(x, y + 1)) ||
          (check(x, y - 1) && check(x + 1, y)) ||
          (check(x, y + 1) && check(x + 1, y))){
        map[x][y] = 3;
      }
    }
  }
  else if (map[x + 1][y] != 2){
    flow(x, y - 1);
    flow(x, y + 1);

    if (check(x, y - 1)||
        check(x, y + 1)){
      map[x][y] = 3;
    }
  }

  return ;
}

int main (void){
  mnx = 2001;
  while (cin >> s >> s2){
    bool e = (s[0] == 'x');

    s = s.substr(2, s.size() - 3);
    s2 = s2.substr(2, s2.size() - 2);

    int a, b, c;
    int i;
    for (i = 0; s2[i] != '.'; i++);

    a = uintaj(s);
    b = uintaj(s2.substr(0, i));
    c = uintaj(s2.substr(i + 2));

    if (e){
      mnx = min(mnx, b);
      mxx = max(mxx, c);
    }
    else{
      mnx = min(mnx, a);
      mxx = max(mxx, a);
    }

    for (i = b; i <= c; i++){
      if (e){
        map[i][a] = 1; 
      }
      else{
        map[a][i] = 1;
      }
    }
  }

  flow(0, 500);

  for (int i = mnx; i <= mxx; i++){
    for (int j = 0; j < 2000; j++){
      sol += (map[i][j] == 3);
    }
  }

  cout << sol << endl;
  return 0;
}
