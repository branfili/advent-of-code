#include <iostream>
#include <vector>
#include <algorithm>
#include <set>

using namespace std;

string s;

string sol;

vector <int> v[26];

int out;
set <int> pos;
set <int> outs;

int t;
int w[5];
int start[5];

int main (void){
  while (getline(cin, s)){
    v[s[36] - 'A'].push_back(s[5] - 'A');
    
    pos.insert(s[5] - 'A');
    pos.insert(s[36] - 'A');
  }

  for (t = 0; !pos.empty(); t++){
    for (set<int>::iterator it = pos.begin(); it != pos.end(); it++){
      if (v[*it].empty()){
        outs.insert(*it);
      }
    }

    for (int i = 0; i < 5; i++){
      if (w[i] == 0){
        if (outs.empty()){
          continue;
        }

        out = *(outs.begin());
        outs.erase(outs.begin());

        sol += (out + 'A');
        pos.erase(pos.find(out));

        w[i] = out + 1;
        start[i] = t;

        continue;
      }

      if (start[i] + w[i] + 59 != t){
        continue;
      }

      w[i]--;
      for (set<int>::iterator it = pos.begin(); it != pos.end(); it++){
        while (true){
          vector<int>::iterator it2 = find(v[*it].begin(), v[*it].end(), w[i]);

          if (it2 == v[*it].end()){
            break;
          }

          v[*it].erase(it2);
        }
      }

      w[i] = 0;
    }
  }

  int mks = 0;
  for (int i = 0; i < 5; i++){
    if (w[i] == 0){
      continue;
    }
    mks = max(mks, start[i] + 60 + w[i]);
  }

  cout << mks << endl;
  return 0;
}
