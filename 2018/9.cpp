#include <iostream>
#include <vector>
#include <algorithm>
#include <list>

using namespace std;

list<int> marbles;
vector<long long> elves;

list<int>::iterator x;
string s;

int n, m;

long long mks;

int uintaj(string s){
  int z = (s[0] - '0');
  for (int i = 1; i < (int) s.size(); i++){
    z *= 10;
    z += (s[i] - '0');
  }
  return z;
}

int main (void){
  getline(cin, s);

  int i;
  for (i = 0; s[i] != ' '; i++);
  n = uintaj(s.substr(0, i));

  int j;
  for (j = i + 31; s[j] != ' '; j++);
  m = uintaj(s.substr(i + 31, j - i - 31));

  m *= 100;

  for (int i = 0; i < n; i++){
    elves.push_back(0);
  }

  marbles.push_back(0);
  x = marbles.begin();

  for (int i = 1; i <= m; i++){
    if (i % 23 == 0){
      int who = (i - 1) % n;

      elves[who] += i;

      for (int j = 0; j < 7; j++){
        if (x == marbles.begin()){
          x = marbles.end();
        }
        x--;
      }
      elves[who] += *x;

      list<int>::iterator y = x;
      y++;
      if (y == marbles.end()){
        y = marbles.begin();
      }

      marbles.erase(x);

      x = y;
    }
    else{
      for (int j = 0; j < 1; j++){
        x++;
        if (x == marbles.end()){
          x = marbles.begin();
        }
      }
      
      x++;
      marbles.insert(x, i);
      x--;
    }
  }

  for (int i = 0; i < n; i++){
    mks = max(mks, elves[i]);
  }

  cout << mks << endl;
  return 0;
}
