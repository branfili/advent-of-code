#include <iostream>
#include <algorithm>

using namespace std;

string s;
string s2;

int react(string s){
  bool e = true;
  while (e &&
         !s.empty()){
    e = false;
    s2 = "";
    for (int i = 0; i < (int) s.size(); i++){
      if (i < (int) s.size() - 1 &&
          toupper(s[i]) == toupper(s[i + 1]) &&
          ((islower(s[i]) && isupper(s[i + 1])) ||
           (isupper(s[i]) && islower(s[i + 1])))){
          e = true;
          i++;
          continue;
      }
      s2 += s[i];
    }

    s = s2;
  }

  return s.size();
}

int main(void){
  cin >> s;

  int mn = s.size();
  for (int i = 0; i < 26; i++){
    s2 = "";
    for (int j = 0; j < (int) s.size(); j++){
      if (toupper(i + 'a') == toupper(s[j])){
        continue;
      }
      s2 += s[j];
    }

    mn = min(mn, react(s2));
  }

  cout << mn << endl;
  return 0;
}
