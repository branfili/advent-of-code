#include <iostream>
#include <cstring>
#include <vector>

using namespace std;
string s;

vector <string> v;

int diff(string a, string b){
  int z = 0;
  for (int i = 0; i < (int) a.size(); i++){
    z += (a[i] != b[i]);
  }
  return z;
}

string remove(string a, string b){
  string s = "";
  for (int i = 0; i < (int) a.size(); i++){
    if (a[i] == b[i]){
      s += a[i];
    }
  }
  return s;
}

int main (void){
  while (cin >> s){
    v.push_back(s);
  }

  for (int i = 0; i < (int) v.size(); i++){
    for (int j = i + 1; j < (int) v.size(); j++){
      if (diff(v[i], v[j]) == 1){
        cout << remove(v[i], v[j]) << endl;
      }
    }
  }

  return 0;
}
