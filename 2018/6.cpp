#include <iostream>
#include <vector>
#include <utility>
#include <algorithm>

using namespace std;
const int DELTA = 500;

int w, h;

int a[100];
bool b[100];

string s1, s2;

vector<pair<int, int> > v;

int uintaj(string s){
  int z = (s[0] - '0');
  for (int i = 1; i < (int)s.size(); i++){
    z *= 10;
    z += (s[i] - '0');
  }
  return z;
}

int main (void){
  while (cin >> s1 >> s2){
    w = uintaj(s1.substr(0, s1.size() - 1));
    h = uintaj(s2);

    v.push_back(make_pair(w, h));
  }

  pair <int, int> mini, maksi;
  mini = make_pair(1e9, 1e9);
  maksi = make_pair(-1e9, -1e9);
  for (int i = 0; i < (int) v.size(); i++){
    mini.first = min(mini.first, v[i].first);
    maksi.first = max(maksi.first, v[i].first);
    mini.second = min(mini.second, v[i].second);
    maksi.second = max(maksi.second, v[i].second);
  }

  int sol = 0;
  for (int i = mini.first - DELTA; i <= maksi.first + DELTA; i++){
    for (int j = mini.second - DELTA; j <= maksi.second + DELTA; j++){
      int z = 0;

      for (int k = 0; k < (int) v.size(); k++){
        z += abs(v[k].first - i) + abs(v[k].second - j);
      }

      if (z < 10000){
        sol++;
      }
    }
  }

  cout << sol << endl;
  return 0;
}
