#include <iostream>
#include <queue>
#include <vector>

using namespace std;
const int XC = 16807;
const int YC = 48271;
const int MOD = 20183;
const int MAXN = 1000;

class gear{
 public:
  int x, y;
  // Type of gear == Type of cave where it 
  // cannot be used
  int type;
  int dist;
};

class cmpGear{
 public:
  // Returning the reversed comparision, because priority_queue
  // returns the maximal element and Dijkstra needs the minimal
  // distance
  bool operator () (gear a, gear b){
    return a.dist > b.dist;  
  }
};

int d;
int x, y;

int cave[MAXN][MAXN];

int sol[MAXN][MAXN][3];
bool bio[MAXN][MAXN][3];

int smjx[] = {-1, 0, 1, 0};
int smjy[] = {0, 1, 0, -1};

priority_queue<gear, vector<gear>, cmpGear> pq;

int main (void){
  cin >> d;
  cin >> y >> x;

  // Initialize the cave
  for (int i = 0; i < MAXN; i++){
    for (int j = 0; j < MAXN; j++){
      if (i == 0 && j == 0){
        cave[i][j] = 0;
      }
      else if (i == x && j == y){
        cave[i][j] = 0;
      }
      else if (i == 0){
        cave[i][j] = (j * XC) % MOD;
      }
      else if (j == 0){
        cave[i][j] = (i * YC) % MOD;
      }
      else{
        cave[i][j] = (cave[i - 1][j] * cave[i][j - 1]) % MOD;
      }
      cave[i][j] = (cave[i][j] + d) % MOD;
    }
  }

  for (int i = 0; i < MAXN; i++){
    for (int j = 0; j < MAXN; j++){
      cave[i][j] %= 3;
    }
  }

  gear start;
  start.x = 0;
  start.y = 0;
  start.type = 1;
  start.dist = 0;

  pq.push(start);

  while (!pq.empty() &&
         !bio[x][y][1]){
    gear cur = pq.top();
    pq.pop();

    if (bio[cur.x][cur.y][cur.type]){
      continue;
    }

    bio[cur.x][cur.y][cur.type] = true;
    sol[cur.x][cur.y][cur.type] = cur.dist;

    // Move around
    for (int i = 0; i < 4; i++){
      gear tmp;

      tmp.x = cur.x + smjx[i];
      tmp.y = cur.y + smjy[i];
      tmp.type = cur.type;
      tmp.dist = cur.dist + 1;

      if (tmp.x >= 0 &&
          tmp.y >= 0 &&
          cave[tmp.x][tmp.y] != tmp.type &&
          !bio[tmp.x][tmp.y][tmp.type]){
        pq.push(tmp);
      }
    }

    // Switch gear
    for (int i = 0; i < 3; i++){
      gear tmp;

      tmp.x = cur.x;
      tmp.y = cur.y;
      tmp.type = i;
      tmp.dist = cur.dist + 7;

      if (cave[tmp.x][tmp.y] != tmp.type &&
          !bio[tmp.x][tmp.y][tmp.type]){
        pq.push(tmp);
      }
    }
  }
  
  cout << sol[x][y][1] << endl;
  return 0;
}
