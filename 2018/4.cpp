#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;
const int months[] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

int guards[4000];
int minute[4000][1440];
string s;

vector<pair<int, int> > event;

int uintaj(string s){
  int z = (s[0] - '0');
  for (int i = 1; i < (int) s.size(); i++){
    z *= 10;
    z += (s[i] - '0');
  }
  return z;
}

int convert(string date){
  //YYYY-MM-DD HH:MM
  int m = uintaj(date.substr(5, 2));
  int d = uintaj(date.substr(8, 2));
  int h = uintaj(date.substr(11, 2));
  int mi = uintaj(date.substr(14, 2));

  int z = 0;
  for (int i = 1; i < m; i++){
    z += months[i - 1];
  }

  return ((z + d - 1) * 1440 + h * 60 + mi);
}

int main(void){
  while (getline(cin, s)){
    int t = convert(s.substr(1, 16));

    if (s[19] == 'w'){
      event.push_back(make_pair(t, 0));
    }
    else if (s[19] == 'f'){
      event.push_back(make_pair(t, -1));
    }
    else{
      int i = 26;
      for (;s[i] != ' '; i++);

      event.push_back(make_pair(t, uintaj(s.substr(26, i - 26))));
    }
  }

  sort(event.begin(), event.end());

  int g = 0;
  int p;
  for (int i = 0; i < (int) event.size(); i++){
    if (event[i].second > 0){
      g = event[i].second;
    }
    else if (event[i].second == -1){
      p = event[i].first;
    }
    else{
      for (int j = p; j < event[i].first; j++){
        guards[g]++;
        minute[g][j % 1440]++;
      }
    }
  }

  int mx = 0;
  int mg, mm;
  for (int i = 0; i < 4000; i++){
    for (int j = 0; j < 1440; j++){
      if (minute[i][j]> mx){
        mx = minute[i][j];
        mg = i;
        mm = j;
      }
    }
  }

  cout << mg * mm << endl;
  return 0;
}
