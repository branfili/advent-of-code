#include <iostream>
#include <utility>
#include <vector>
#include <cstdlib>
#include <algorithm>
#include <cstring>

using namespace std;

string s;

vector<pair<int, int> > pos, vel;

pair<int, int> mini, maksi;

int xs, ys;
int mint, makst;

vector<pair<int, int> > v;

bool sky[150][150];

int uintaj(string s){
  int z = (s[0] - '0');
  for (int i = 1; i < (int) s.size(); i++){
    z *= 10;
    z += (s[i] - '0');
  }
  return z;
}

int main (void){
  while (getline(cin, s)){
    int xp = uintaj(s.substr(11, 5)) * (s[10] == '-' ? -1 : 1);
    int yp = uintaj(s.substr(19, 5)) * (s[18] == '-' ? -1 : 1);
  
    int xv = uintaj(s.substr(37, 1)) * (s[36] == '-' ? -1 : 1);
    int yv = uintaj(s.substr(41, 1)) * (s[40] == '-' ? -1 : 1);
    
    xs += xp;
    ys += yp;

    pos.push_back(make_pair(xp, yp));
    vel.push_back(make_pair(xv, yv));
  }

  xs /= pos.size();
  ys /= pos.size();

  mint = 1e9;
  for (int i = 0; i < (int) pos.size(); i++){
    mint = min(mint, (xs - pos[i].first) / vel[i].first);
    mint = min(mint, (ys - pos[i].second) / vel[i].second);

    makst = max(makst, (xs - pos[i].first) / vel[i].first + 1);
    makst = max(makst, (ys - pos[i].second) / vel[i].second + 1);
  }

  for (int t = mint; t <= makst; t++){
    memset(sky, 0, sizeof sky);
    v.clear();
    
    mini = make_pair(1e9, 1e9);
    for (int i = 0; i < (int) pos.size(); i++){
      pair<int, int> cur;

      cur.first = pos[i].first + t * vel[i].first;
      cur.second = pos[i].second + t * vel[i].second;

      v.push_back(cur);

      mini.first = min(mini.first, cur.first);
      mini.second = min(mini.second, cur.second);
    }

    for (int i = 0; i < (int) v.size(); i++){
      v[i].first -= mini.first;
      v[i].second -= mini.second;

      if (v[i].first >= 0 &&
          v[i].first < 150 &&
          v[i].second >= 0 &&
          v[i].second < 150){
          sky[v[i].first][v[i].second] = true;
      }
    }

    cout << t << endl;
    for (int i = 0; i < 150; i++){
      for (int j = 0; j < 150; j++){
        cout << (sky[j][i] ? '#' : '.');
      }
      cout << endl;
    }
    cout << endl;
  }

  return 0;
}
