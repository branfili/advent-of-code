#include <iostream>
#include <vector>

using namespace std;
string s;

vector <string> wood, wood2;

int n, m;

int w, l;
int sol;

int smjx[] = {-1, -1, 0, 1, 1, 1, 0, -1};
int smjy[] = {0, 1, 1, 1, 0, -1, -1, -1};

char next(int x, int y){
  int o = 0;
  int w = 0;
  int l = 0;

  for (int i = 0; i < 8; i++){
    int nx = x + smjx[i];
    int ny = y + smjy[i];

    if (nx < 0 ||
        nx >= n ||
        ny < 0 ||
        ny >= m){
      continue;
    }

    if (wood[nx][ny] == '.'){
      o++;
    }
    else if (wood[nx][ny] == '|'){
      w++;
    }
    else{
      l++;
    }
  }

  if (wood[x][y] == '.'){
    return (w >= 3 ? '|' : '.');
  }
  else if (wood[x][y] == '|'){
    return (l >= 3 ? '#' : '|');
  }
  else{
    return (w >= 1 && l >= 1 ? '#' : '.');
  }
}

int main(void){
  while (cin >> s){
    wood.push_back(s);
  }

  n = wood.size();
  m = wood[0].size();

  for (int t = 0; t < 1e9; t++){
    wood2.clear();

    for (int i = 0; i < n; i++){
      s = "";
      for (int j = 0; j < m; j++){
        s += next(i, j);
      }
      wood2.push_back(s);
    }

    wood = wood2;
  }

  for (int i = 0; i < n; i++){
    for (int j = 0; j < m; j++){
      w += (wood[i][j] == '|');
      l += (wood[i][j] == '#');
    }
  }

  sol = w * l;

  cout << sol << endl;
  return 0;
}
