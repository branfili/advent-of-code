#include <iostream>
#include <utility>
#include <algorithm>

using namespace std;

int n;

int fuel[300][300];

int mx;
pair<int, int> mkp;
int mks;

int get(int x, int y){
  if (x < 0 ||
      y < 0 ||
      x >= 300 ||
      y >= 300){
    return 0;
  }
  return fuel[x][y];
}

int main(void){
  cin >> n;

  for (int i = 0; i < 300; i++){
    for (int j = 0; j < 300; j++){
      fuel[i][j] = j + 11;
      fuel[i][j] *= (i + 1);
      fuel[i][j] += n;
      fuel[i][j] *= (j + 11);
      fuel[i][j] = (fuel[i][j] / 100) % 10;
      fuel[i][j] -= 5;
    }
  }

  for (int i = 0; i < 300; i++){
    for (int j = 0; j < 300; j++){
      fuel[i][j] += get(i - 1, j) + get(i, j - 1) - get(i - 1, j - 1);
    }
  }

  mx = -1e9;
  for (int i = 0; i < 300; i++){
    for (int j = 0; j < 300; j++){
      for (int k = 1; k <= min(300 - i, 300 - j); k++){
        int z = get(i + k - 1, j + k - 1) - get(i + k - 1, j - 1) - get(i - 1, j + k - 1) + get(i - 1, j - 1);
        
        if (z > mx){
          mx = z;
          mkp = make_pair(j + 1, i + 1);
          mks = k;
        }
      }
    }
  }

  cout << mkp.first << ' ' << mkp.second << ' ' << mks << endl;
  return 0;
}
