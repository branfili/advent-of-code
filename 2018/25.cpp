#include <iostream>
#include <vector>
#include <cmath>

using namespace std;
struct point{
  int x, y, z;
  int t;
};

string s;

vector<point> v;
vector<bool> bio;
vector<vector<int> > g;

int sol;

int uintaj(string s){
  bool neg = false;
  if (s[0] == '-'){
    neg = true;
    s = s.substr(1);
  }

  int z = 0;
  for (int i = 0; i < (int)s.size(); i++){
    z *= 10;
    z += (s[i] - '0');
  }
  z *= (neg ? -1 : 1);

  return z;
}

int dist(point a, point b){
  return abs(a.x - b.x) +
         abs(a.y - b.y) +
         abs(a.z - b.z) +
         abs(a.t - b.t);
}

void dfs(int x){
  if (bio[x]){
    return ;
  }

  bio[x] = true;

  for (int i = 0; i < (int)g[x].size(); i++){
    dfs(g[x][i]);
  }

  return ;
}

int main (void){
  while (cin >> s){
    point tmp;
    int i;
    for (i = 0; s[i] != ','; i++);
    tmp.x = uintaj(s.substr(0, i));
    s = s.substr(i + 1);

    for (i = 0; s[i] != ','; i++);
    tmp.y = uintaj(s.substr(0, i));
    s = s.substr(i + 1);

    for (i = 0; s[i] != ','; i++);
    tmp.z = uintaj(s.substr(0, i));
    tmp.t = uintaj(s.substr(i + 1));

    vector<int> tmp2;

    v.push_back(tmp);
    bio.push_back(false);
    g.push_back(tmp2);
  }

  for (int i = 0; i < (int)v.size(); i++){
    for (int j = i + 1; j < (int)v.size(); j++){
      if (dist(v[i], v[j]) <= 3){
        g[i].push_back(j);
        g[j].push_back(i);
      }
    }
  }

  for (int i = 0; i < (int)v.size(); i++){
    if (bio[i]){
      continue;
    }

    sol++;
    dfs(i);
  }

  cout << sol << endl;
  return 0;
}
