#include <iostream>
#include <vector>
#include <cstring>
#include <algorithm>

using namespace std;
struct cart{
  int x, y;
  int dir;
  int val;
  bool moved;
};

string s;

vector <string> map;
vector <cart> carts;

int smjx[] = {-1, 0, 1, 0};
int smjy[] = {0, 1, 0, -1};

int recognize(char c){
  switch (c){
    case '^':
      return 0;
    case '>':
      return 1;
    case 'v':
      return 2;
    case '<':
      return 3;
    default:
      return -1;
  }
}

bool cmp(cart a, cart b){
  if (a.x != b.x){
    return a.x < b.x;
  }
  return a.y < b.y;
}

int main (void){
  
  for (int x = 0; getline(cin, s); x++){
    for (int i = 0; i < (int)s.size(); i++){
      if (s[i] == '>' ||
          s[i] == '<' ||
          s[i] == 'v' ||
          s[i] == '^'){
          
          cart tmp;
          tmp.x = x;
          tmp.y = i;
          tmp.dir = recognize(s[i]);
          tmp.val = 0;

          carts.push_back(tmp);

          if (tmp.dir == 1 ||
              tmp.dir == 3){
            s[i] = '-';
          }
          else{
            s[i] = '|';
          }
      }
    }

    map.push_back(s);
  }

  while (carts.size() > 1){
    sort(carts.begin(), carts.end(), cmp);
    for (int i = 0; i < (int) carts.size(); i++){
      carts[i].moved = false;
    }

    for (int i = 0; i < (int) carts.size(); i++){
      if (carts[i].moved){
        continue;
      }

      carts[i].x += smjx[carts[i].dir];
      carts[i].y += smjy[carts[i].dir];
      carts[i].moved = true;

      bool e = false;
      for (int j = 0; j < (int) carts.size(); j++){
        if (i == j){
          continue;
        }

        if (carts[i].x == carts[j].x &&
            carts[i].y == carts[j].y){
            vector <cart> tmp;
          
            for (int k = 0; k < (int) carts.size(); k++){
              if (k != i &&
                  k != j){
                tmp.push_back(carts[k]);
              }
            }

            carts = tmp;
            i = -1;
            e = true;
            break;
        }
      }

      if (e){
        continue;
      }
        
      if (map[carts[i].x][carts[i].y] == '/'){
        switch (carts[i].dir){
          case 0:
            carts[i].dir = 1;
            break;
          case 1:
            carts[i].dir = 0;
            break;
          case 2:
            carts[i].dir = 3;
            break;
          case 3:
            carts[i].dir = 2;
            break;
          default:
            break;
        }
      }
      else if (map[carts[i].x][carts[i].y] == '\\'){
        switch (carts[i].dir){
          case 0:
            carts[i].dir = 3;
            break;
          case 1:
            carts[i].dir = 2;
            break;
          case 2:
            carts[i].dir = 1;
            break;
          case 3:
            carts[i].dir = 0;
            break;
          default:
            break;
        }
      }
      else if (map[carts[i].x][carts[i].y] == '+'){
        carts[i].dir = (carts[i].dir + carts[i].val + 3) % 4;
        carts[i].val = (carts[i].val + 1) % 3;
      }
    }
  }

  cout << carts[0].y << ' ' << carts[0].x << endl;
  return 0;
}
