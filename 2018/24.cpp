#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

struct group{
  int id;
  
  bool is_good;
  long long amount, hp, attack, init;

  string damage_type;

  vector <string> weaknesses;
  vector <string> immunities;

  long long cur_target;
  long long damage;
  long long mult;
};

struct target{
  int id;

  long long damage;
  long long eff_power;
  long long init;

  long long mult;
};

string s;
long long id_c;

vector <group> armies;

long long uintaj(string s){
  long long z = 0;
  for (int i = 0; i < (int) s.size(); i++){
    z *= 10;
    z += (s[i] - '0');
  }
  return z;
}

bool check(vector <group>& armies2){
  bool goods = false;
  bool bads = false;

  for (int i = 0; i < (int) armies2.size(); i++){
    goods |= armies2[i].is_good;
    bads |= !armies2[i].is_good;
  }

  return (goods && bads);
}

bool gr_target_compare(group a, group b){
  long long eff_power_a = a.attack * a.amount;
  long long eff_power_b = b.attack * b.amount;

  if (eff_power_a != eff_power_b){
    return eff_power_a > eff_power_b;
  }

  return a.init > b.init;
}

bool target_compare(target a, target b){
  if (a.damage != b.damage){
    return a.damage > b.damage;
  }

  if (a.eff_power != b.eff_power){
    return a.eff_power > b.eff_power;
  }

  return a.init > b.init;
}

bool gr_attack_compare(group a, group b){
  return a.init > b.init;
}

int sol(int boost){
  vector <target> targets;
  vector <group> armies2, armies3;
  long long eff_power;

  bool f;

  armies2.clear();
  for (int i = 0; i < (int) armies.size(); i++){
    armies2.push_back(armies[i]);

    if (armies2[i].is_good){
      armies2[i].attack += boost;
    }
  }

  while (check(armies2)){
    sort(armies2.begin(), armies2.end(), gr_target_compare);

    for (int i = 0; i < (int) armies2.size(); i++){
      targets.clear();

      eff_power = armies2[i].attack * armies2[i].amount;

      for (int j = 0; j < (int) armies2.size(); j++){
        if (j == i || armies2[j].is_good == armies2[i].is_good){
          continue;
        }

        bool f = false;
        for (int k = 0; k < i; k++){
          f |= (armies2[k].cur_target == armies2[j].id);
        }

        if (f){
          continue;
        }

        target t;
        t.id = armies2[j].id;
        t.eff_power = armies2[j].attack * armies2[j].amount;
        t.init = armies2[j].init;
        t.damage = eff_power;
        t.mult = 1;

        bool found = false;

        for (int k = 0; k < (int) armies2[j].weaknesses.size(); k++){
          if (armies2[i].damage_type == armies2[j].weaknesses[k]){
            t.damage *= 2;
            t.mult = 2;
            found = true;
            break;
          }
        }

        if (found){
          targets.push_back(t);
          continue;
        }

        for (int k = 0; k < (int) armies2[j].immunities.size(); k++){
          if (armies2[i].damage_type == armies2[j].immunities[k]){
            t.damage *= 0;
            t.mult = 0;
            found = true;
            break;
          }
        }

        if (!found){
          targets.push_back(t);
        }
      }

      if (targets.empty()){
        armies2[i].cur_target = -1;
        continue;
      }

      sort(targets.begin(), targets.end(), target_compare);
      armies2[i].cur_target = targets[0].id;
      armies2[i].mult = targets[0].mult;
    
    }

    sort(armies2.begin(), armies2.end(), gr_attack_compare);

    f = false;

    for (int i = 0; i < (int) armies2.size(); i++){
      if (armies2[i].amount <= 0 ||
          armies2[i].cur_target == -1){
        continue;
      }

      armies2[i].damage = armies2[i].amount * armies2[i].attack * armies2[i].mult;
      
      int k = 0;

      for (int j = 0; j < (int) armies2.size(); j++){
        if (armies2[j].id == armies2[i].cur_target){
          k = j;
          break;
        }
      }

      armies2[k].amount -= armies2[i].damage / armies2[k].hp;
    
      f |= (armies2[i].damage / armies2[k].hp != 0);
    }

    if (!f){
      break;
    }

    armies3.clear();
    for (int i = 0; i < (int) armies2.size(); i++){
      if (armies2[i].amount > 0){
        armies3.push_back(armies2[i]);
      }
    }

    armies2.clear();
    for (int i = 0; i < (int) armies3.size(); i++){
      armies2.push_back(armies3[i]);
    }
  }

  if (!f){
    return -10000000;
  }

  int sol = 0;
  for (int i = 0; i < (int) armies2.size(); i++){
    sol += armies2[i].amount;
  }

  sol *= (armies2[0].is_good ? 1 : -1);

  return sol;
}

int main (void){
  string s2;
  vector <string> words;
  int ind = 0;
  int k = 0;

  bool is_good;
  
  while (getline(cin, s)){
    if (s.size() < 2){
      continue;
    }
    else if (s.substr(0, 6) == "Immune"){
      is_good = true;
      continue;
    }
    else if (s.substr(0, 6) == "Infect"){
      is_good = false;
      continue;
    }
    
    group g;
    g.weaknesses.clear();
    g.immunities.clear();
    g.id = ind++;
    g.is_good = is_good;
    
    s += ' ';
    s2 = "";
    words.clear();

    for (int i = 0; i < (int) s.size(); i++){
      if (s[i] == ' '){
        words.push_back(s2);
        s2 = "";
      }
      else{
        s2 += s[i];
      }
    }

    g.amount = uintaj(words[0]);
    g.hp = uintaj(words[4]);

    if (words[7][0] == '('){
      for (k = 0; words[k + 7][words[k + 7].size() - 1] != ')'; k++);

      int i = 9;
      bool e = (words[7] == "(weak");
      for (;i <= 7 + k; i++){
        string word = words[i].substr(0, words[i].size() - 1);

        if (e){
          g.weaknesses.push_back(word);
        }
        else{
          g.immunities.push_back(word);
        }

        if (words[i][words[i].size() - 1] == ';'){
          e ^= 1;
          i += 2;
        }
      }
    }
    else{
      k = 0;
    }

    g.attack = uintaj(words[words.size() - 6]);
    g.damage_type = words[words.size() - 5];
    g.init = uintaj(words[words.size() - 1]);

    armies.push_back(g);
  }

  for (int boost = 1 ;; boost++){
    int x = sol(boost);

    if (x > 0){
      cout << x << endl;
      break;
    }
  }

  return 0;
}
