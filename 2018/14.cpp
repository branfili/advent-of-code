#include <iostream>
#include <vector>

using namespace std;
string s;

int x, y;

vector <int> recipes;

int main (void){
  cin >> s;

  recipes.push_back(3);
  recipes.push_back(7);

  x = 0;
  y = 1;

  bool e = true;
  while (e){
    int z = recipes[x] + recipes[y];

    if (z < 10){
      recipes.push_back(z);
    }
    else{
      recipes.push_back(z / 10);
      recipes.push_back(z % 10);
    }

    x = (x + 1 + recipes[x]) % recipes.size();
    y = (y + 1 + recipes[y]) % recipes.size();
  
    if (recipes.size() >= s.size()){
      string s2;
      
      for (int i = 0; i < 5; i++){
        if (recipes.size() < s.size() + i){
          continue;
        }

        s2 = "";
        for (int j = (int) recipes.size() - s.size() - i; j < (int) recipes.size() - i; j++){
          s2 += (recipes[j] +  '0');
        }

        if (s2 == s){
          cout << recipes.size() - s.size() - i << endl;
          e = false;
          break;
        }
      }
    }
  }

  return 0;
}
