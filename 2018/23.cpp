#include <iostream>
#include <vector>
#include <cmath>
#include <algorithm>

using namespace std;

const int MIN_RANGE = 30;
const int SAMPLE_SIZE = 30;

struct point{
  long long x, y, z;
};

struct nanobot{
  point t;
  long long r;
};

struct event{
  long long d;
  int t;
};

string s, s2;

point center;

vector <nanobot> v;
vector <event> d;

bool a[1000][1000];
bool clique[1000];

int uintaj(string s){
  bool neg = false;
  if (s[0] == '-'){
    neg = true;
    s = s.substr(1);
  }

  int z = 0;
  for (int i = 0; i < (int)s.size(); i++){
    z *= 10;
    z += (s[i] - '0');
  }
  z *= (neg ? -1 : 1);

  return z;
}

int dist(point a, point b){
  return abs(a.x - b.x) +
         abs(a.y - b.y) +
         abs(a.z - b.z);
}

bool comp(event a, event b){
  if (a.d != b.d){
    return a.d < b.d;
  }

  return a.t > b.t;
}

int main (void){
  while (cin >> s >> s2){
    s = s.substr(5, s.size() - 7);
    s2 = s2.substr(2);

    nanobot tmp;

    tmp.r = uintaj(s2);

    int i;
    for (i = 0; s[i] != ','; i++);
    tmp.t.x = uintaj(s.substr(0, i));
    s = s.substr(i + 1);

    for (i = 0; s[i] != ','; i++);
    tmp.t.y = uintaj(s.substr(0, i));
    tmp.t.z = uintaj(s.substr(i + 1));

    v.push_back(tmp);
  }

  for (int i = 0; i < (int) v.size(); i++){
    for (int j = i + 1; j < (int) v.size(); j++){
      if (dist(v[i].t, v[j].t) <= v[i].r + v[j].r){
        a[i][j] = a[j][i] = true;
      }
    }
    a[i][i] = true;
  }

  clique[0] = true;
  bool e = true;
  bool f;

  while (e){
    e = false;

    for (int i = 0; i < (int) v.size(); i++){
      if (clique[i]){
        continue;
      }

      f = true;

      for (int j = 0; j < (int) v.size(); j++){
        if (!clique[j]){
          continue;
        }

        if (!a[j][i]){
          f = false;
          break;
        }
      }

      if (f){
        e = true;
        clique[i] = true;
        break;
      }
    }
  }

  for (int i = 0; i < (int) v.size(); i++){
    if (!clique[i]){
      continue;
    }

    event tmp, tmp2;

    tmp.d = dist(v[i].t, center) - v[i].r;
    tmp2.d = dist(v[i].t, center) + v[i].r;

    tmp.t = 1;
    tmp2.t = -1;

    d.push_back(tmp);
    d.push_back(tmp2);
  }

  sort(d.begin(), d.end(), comp);

  int maxd = 0;
  int cur = 0;
  long long sol;
  for (int i = 0; i < (int) d.size(); i++){
    cur += d[i].t;

    if (cur > maxd){
      sol = d[i].d;
      maxd = cur;
    }
  }

  cout << sol + 1 << endl;
  return 0;
}
