#!/usr/bin/python3

import hashlib

def repet(h, x, c):
    e = (c != '')
    for i in range(len(h) - x + 1):
        if (not e):
            c = h[i]

        if (h[i : i + x] == c * x):
            return c

    return ''

def hsh(s, x):
    e = (s == "abc0")

    for i in range(x):
        s = hashlib.md5(str.encode(s)).hexdigest()

    return s

salt = input()

h = []

for i in range(10 ** 5):
    h.append(hsh(salt + str(i), 2017))

i = 0
keys = []

while (i < 10 ** 6 and len(keys) < 64):
    c = repet(h[i], 3, '')

    if (c != ''):
        e = False
        for j in range(i + 1, i + 1001):
            if (repet(h[j], 5, c) != ''):
                e = True
                break

        if (e):
            keys.append(i)

    i += 1

print(keys[-1])
