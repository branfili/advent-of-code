#!/usr/bin/python3

import sys

o = [-1] * 1000
b = []
tr = []

for i in range(1000):
    b.append([])
    tr.append([])

for ins in sys.stdin:
    l = ins.split()

    if (l[0] == "value"):
        b[int(l[-1])].append(int(l[1]))
    else:
        x = int(l[1])
        tr = tr[:x] + [(l[5][0] + l[6], l[-2][0] + l[-1])] + tr[x + 1:]

e = True
while (e):
    e = False
    for i in range(1000):
        if (len(b[i]) == 2):
            e = True

            if (b[i][0] > b[i][1]):
                b[i][0] ^= b[i][1]
                b[i][1] ^= b[i][0]
                b[i][0] ^= b[i][1]

            if (b[i] == [17, 61]):
                print (i)

            for j in range(2):
                if (tr[i][j][0] == 'o'):
                    o[int(tr[i][j][1:])] = b[i][j]
                else:
                    b[int(tr[i][j][1:])].append(b[i][j])

            b[i] = []

print(o[0] * o[1] * o[2])
