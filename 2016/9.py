#!/usr/bin/python3

def decompress(s):
    i = 0
    z = 0

    while (i < len(s)):
        if (s[i] != '('):
            z += 1
            i += 1
            continue

        x = s[i:].index(')') + i

        a, b = map(int, s[i + 1 : x].split('x'))

        z += b * decompress(s[x + 1: x + a + 1])

        i = x + a + 1

    return z

print(decompress(input()))
