#!/usr/bin/python3

import sys

def abba(s, x):
    for i in range(len(s) - 2):
        if (s[i] == s[i + 2] and
            s[i] != s[i + 1]):
            global l
            l[x].append(s[i : i + 3])
sol = 0

for w in sys.stdin:
    e = True
    global l
    l = [[], []]

    while ('[' in w):
        x = w.index('[')
        y = w.index(']')

        a, b = w[:x], w[x + 1 : y]

        w = w[y + 1:]

        abba(a, 0)
        abba(b, 1)

    if (not e):
        continue

    if (w != ""):
        abba(w, 0)

    e = False
    for i in range(len(l[0])):
        for j in range(len(l[1])):
            if (l[0][i][0] == l[1][j][1] and
                l[1][j][0] == l[0][i][1]):
                    sol += 1
                    e = True
                    break

        if (e):
            break

print(sol)
