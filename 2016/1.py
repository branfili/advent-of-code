#!/usr/bin/python3

l = input().split(', ')

dir = 0
(x, y) = (0, 0)

smj = [(1, 0), (0, 1), (-1, 0), (0, -1)]

t = [(0, 0)]

for a in l:
    if (a[0] == 'R'):
        dir = (dir + 1) % 4
    else:
        dir = (dir + 3) % 4

    for k in range(int(a[1:])):
        (x, y) = (x + smj[dir][0], y + smj[dir][1])

        for loc in t:
            if ((x, y) == loc):
                print(abs(x) + abs(y))
                break

        t.append((x, y))
