#!/usr/bin/python3

import queue

def isWall(x, y, n):
    v = x*x + 3*x + 2*x*y + y + y*y + n
    z = 0

    while (v > 0):
        z += (v % 2)
        v //= 2

    return (z % 2 == 1)

n = int(input())

q = queue.Queue()
q.put((1, 1, 0))

visited = set()

smj = [(1, 0), (0, 1), (-1, 0), (0, -1)]

while not q.empty():
    x, y, d = q.get()

    if ((x, y) in visited):
        continue

    if (d > 50):
        print (len(visited))
        break

    visited.add((x, y))

    for i in range(4):
        nx, ny = x + smj[i][0], y + smj[i][1]

        if (nx >= 0 and
            ny >= 0 and
            not isWall(nx, ny, n)):
            q.put((nx, ny, d + 1))
