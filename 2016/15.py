#!/usr/bin/python3

import sys

def possible(t, d):
    for i in range(len(d)):
        if ((d[i][1] + t + i + 1) % d[i][0] != 0):
            return False

    return True

d = []

for w in sys.stdin:
    l = w.split()

    d.append((int(l[3]), int(l[-1][:-1])))

d.append((11, 0))

t = 0

while (not possible(t, d)):
    t += 1

print (t)
