#!/usr/bin/python3

import queue
import hashlib

password = input()

smj = [(-1, 0), (1, 0), (0, -1), (0, 1)]
p = "UDLR"

q = queue.Queue()
q.put((0, 0, ""))

visited = set()
paths = []

while not q.empty():
    x, y, path = q.get()

    if (path in visited):
        continue

    if ((x, y) == (3, 3)):
        paths.append(path)
        continue

    visited.add(path)

    h = list(map(ord, hashlib.md5(str.encode(password + path)).hexdigest()[:4]))

    for i in range(4):
        nx, ny = x + smj[i][0], y + smj[i][1]

        if (nx >= 0 and
            ny >= 0 and
            nx < 4 and
            ny < 4 and
            h[i] >= ord('b') and
            h[i] <= ord('f')):
            q.put((nx, ny, path + p[i]))

print (len(paths[-1]))
