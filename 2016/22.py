#!/usr/bin/python3

import sys
import queue

h, w = 38, 28
grid = []
l2 = []

def done(x):
    return (x % (40 ** 2) == 0)

def serialize(l):
    z = l[0]
    for i in range(len(l)):
        z *= 40
        z += l[i]

    return z

def deserialize(x):
    l = []
    for i in range(4):
        l.append(x % 40)
        x //= 40

    return list(reversed(l))

for line in sys.stdin:
    l = line.split()

    (u, a) = (int(l[2][:-1]), int(l[3][:-1]))

    l2.append((u, a))

    if (len(l2) == w):
        grid.append(l2)
        l2 = []

grid2 = []
ex, ey = -1, -1

for i in range(h):
    s = ""
    for j in range(w):
        if (grid[i][j][1] > grid[i][j][0]):
            s += '_'
            ex, ey = i, j
        elif (grid[i][j][0] > (grid[h - 1][0][0] + grid[h - 1][0][1])):
            s += '#'
        else:
            s += '.'

    if (i == h - 1):
        s = 'G' + s[1:]

    grid2.append(s)

start = [ex, ey, h - 1, 0]

q = queue.Queue()
q.put((serialize(start), 0))

visited = set()

smj = [(1, 0), (0, 1), (-1, 0), (0, -1)]

while not q.empty():
    st, d = q.get()

    if (st in visited):
        continue

    if (done(st)):
        print(d)
        break

    visited.add(st)

    l = deserialize(st)

    for i in range(4):
        nx, ny = l[0] + smj[i][0], l[1] + smj[i][1]

        if (nx >= 0 and
            nx < h and
            ny >= 0 and
            ny < w and
            grid2[nx][ny] != '#'):

            l2 = [nx, ny]

            if (nx == l[2] and
                ny == l[3]):
                l2 += l[:2]
            else:
                l2 += l[2:]

            q.put((serialize(l2), d + 1))
