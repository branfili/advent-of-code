#!/usr/bin/python3

import sys

sol = 0

for s in sys.stdin:
    check = s[-7:-2]
    l = s[:-8].split('-')

    let = ' '.join(l[:-1])
    sid = int(l[-1])

    f = [0] * 26

    for c in let:
        if (c == ' '):
            continue
        f[ord(c) - ord('a')] += 1

    check2 = ""

    for i in range(5):
        mx = 0
        mc = ''

        for k in range(26):
            if (chr(k + ord('a')) not in check2 and
                f[k] > mx):
                mx = f[k]
                mc = chr(k + ord('a'))

        check2 += mc

    if (check != check2):
        continue

    let2 = ""

    for i in range(len(let)):
        if (let[i] == ' '):
            let2 += ' '
            continue

        let2 += chr((ord(let[i]) - ord('a') + sid) % 26 + ord('a'))

    print(let2, sid)
