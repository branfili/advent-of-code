#!/usr/bin/python3

import sys

disp = [' ' * 50] * 6

for w in sys.stdin:
    l = w.split()

    if (l[0] == "rect"):
        x, y = map(int, l[1].split('x'))

        disp2 = []

        for i in range(6):
            if (i < y):
                disp2.append('#' * x + disp[i][x:])
            else:
                disp2.append(disp[i])

        disp = disp2

    else:
        x = int(l[2].split('=')[1])
        a = int(l[4])

        if (l[1] == "row"):
            disp = disp[:x] + [disp[x][-a:] + disp[x][:-a]] + disp[x + 1 : ]
        else:
            disp2 = []

            for i in range(6):
                disp2.append(disp[i][:x] + disp[((i - a) % 6 + 6) % 6][x] + disp[i][x + 1 :])

            disp = disp2

for i in range(6):
    print(disp[i])
