#!/usr/bin/python3

import queue

def serialize(l):
    z = l[0]
    for i in range(1, len(l)):
        z *= 4
        z += l[i]

    return z

def deserialize(x, n):
    l = []
    for i in range(n):
        l.append(x % 4)
        x //= 4

    l.reverse()

    return l

def done(x, n):
    l = deserialize(x, n)

    for i in range(1, len(l)):
        if (l[i] != 3):
            return False

    return True

def isValid(l):
    for i in range(1, len(l), 2):
        f = l[i]
        for j in range(2, len(l), 2):
            if (l[j] == f and
                l[j - 1] != f):
                return False

    return True

def getSuccessors(x, n):
    l = deserialize(x, n)

    pos = []
    succ = []

    for i in range(1, len(l)):
        if (l[i] == l[0]):
            pos.append(i)

            if (l[0] < 3):
                succ.append([l[0] + 1] + l[1:i] + [l[i] + 1] + l[i + 1:])

            if (l[0] > 0):
                succ.append([l[0] - 1] + l[1:i] + [l[i] - 1] + l[i + 1:])

    for i in range(len(pos)):
        for j in range(i + 1, len(pos)):
            if (l[0] < 3):
                succ.append([l[0] + 1] + l[1:pos[i]] + [l[pos[i]] + 1] + l[pos[i] + 1 : pos[j]] + [l[pos[j]] + 1] + l[pos[j] + 1:])

            if (l[0] > 0):
                succ.append([l[0] - 1] + l[1:pos[i]] + [l[pos[i]] - 1] + l[pos[i] + 1 : pos[j]] + [l[pos[j]] - 1] + l[pos[j] + 1:])

    return map(serialize, filter(isValid, succ))

a = []
n = 0

for i in range(4):
    l = input().split()

    if (i == 0):
        a = [0] * len(l)
        n = len(l)

    for j in range(n):
        if (l[j] != '.'):
            a[j] = i

q = queue.Queue()
visited = set()

q.put((serialize(a), 0))

while not q.empty():
    x, d = q.get()

    if (x in visited):
        continue

    if (done(x, n)):
        print (d)
        break

    visited.add(x)

    succ = getSuccessors(x, n)

    for s in succ:
        q.put((s, d + 1))
