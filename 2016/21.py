#!/usr/bin/python3

import sys

s = "fbgdceah"

inst = []

for ins in sys.stdin:
    inst.append(ins)

inst = reversed(inst)

for ins in inst:
    l = ins.split()

    if (l[0] == "move"):
        y, x = int(l[2]), int(l[5])

        c = s[x]

        s = s[:x] + s[x + 1:]
        s = s[:y] + str(c) + s[y:]

    elif (l[0] == "reverse"):
        x, y = int(l[2]), int(l[4])

        s2 = ''.join(reversed(s[x:y + 1]))

        s = s[:x] + s2 + s[y + 1:]

    elif (l[0] == "swap"):
        x, y = 0, 0
        if (l[1] == "position"):
            x, y = int(l[2]), int(l[5])
        else:
            x, y = s.index(l[2]), s.index(l[5])

        a, b = s[x], s[y]

        s = s[:x] + str(b) + s[x + 1:]
        s = s[:y] + str(a) + s[y + 1:]

    else:
        x = 0
        if (l[1] == "left"):
            x = int(l[2])
        elif (l[1] == "right"):
            x = -int(l[2])
        else:
            y = s.index(l[6])
            for z in range(len(s)):
                if ((2 * z + 1 + (z >= 4)) % len(s) == y):
                    x = z - y

        x = ((x % len(s)) + len(s)) % len(s)

        s = s[-x:] + s[:-x]

print(s)
