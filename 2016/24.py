#!/usr/bin/python3

import sys
import queue
import itertools

grid = []

for line in sys.stdin:
    grid.append(line.strip())

l = []
h, w = len(grid), len(grid[0])

for i in range(h):
    for j in range(w):
        if (grid[i][j] != '.' and
            grid[i][j] != '#'):
            l.append((int(grid[i][j]), i, j))

l = sorted(l)
p = list(itertools.permutations(l[1:]))

sol = 10 ** 18
smj = [(1, 0), (0, 1), (-1, 0), (0, -1)]

for perm in p:
    perm = tuple([l[0]] + list(perm) + [l[0]])

    dist = 0

    for i in range(0, len(perm) - 1):
        q = queue.Queue()
        q.put((perm[i][1], perm[i][2], 0))

        visited = set()

        while not q.empty():
            x, y, d = q.get()

            if ((x, y) in visited):
                continue

            if (x == perm[i + 1][1] and
                y == perm[i + 1][2]):
                dist += d
                break

            visited.add((x, y))

            for j in range(4):
                nx, ny = x + smj[j][0], y + smj[j][1]

                if (nx >= 0 and
                    nx < h and
                    ny >= 0 and
                    ny < w and
                    grid[nx][ny] != '#'):
                    q.put((nx, ny, d + 1))

    sol = min(sol, dist)

print(sol)
