#!/usr/bin/python3

import hashlib

did = input()

i = 0

password = '*' * 8

while (True):
    h = hashlib.md5(str.encode(did + str(i))).hexdigest()

    if (h[:5] == "00000"):
        if (h[5] > '7' or
            password[int(h[5])] != '*'):
            i += 1
            continue

        k = int(h[5])

        password2 = password[:k] + h[6] + password[k + 1:]
        password = password2

    if ('*' not in password):
        break

    i += 1

print(password)
