#!/usr/bin/python3

import sys

key = ["  1  ", " 234 ", "56789", " ABC ", "  D  "]

(x, y) = (1, 1)

sol = ""

for ins in sys.stdin:
    for d in ins:
        if (d == 'L'):
            if (key[x][max(0, y - 1)] == ' '):
                continue

            y = max(0, y - 1)
        elif (d == 'R'):
            if (key[x][min(4, y + 1)] == ' '):
                continue

            y = min(4, y + 1)
        elif (d == 'U'):
            if (key[max(0, x - 1)][y] == ' '):
                continue

            x = max(0, x - 1)
        elif (d == 'D'):
            if (key[min(4, x + 1)][y] == ' '):
                continue

            x = min(4, x + 1)

    sol += key[x][y]

print(sol)
