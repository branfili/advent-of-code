#!/usr/bin/python3

n = int(input())

k = 1

while (k < n):
    k *= 3

k //= 3

if (n == k):
    print k
elif (k + k // 3 <= n):
    print (n - k)
else:
    print (k + 2 * (n - 2 * k))
