#!/usr/bin/python3

import sys

ins = []

for s in sys.stdin:
    ins.append(s.strip().split())

reg = [0] * 4
reg[0] = 12

i = 0

while (i < len(ins)):
    if (ins[i][0] == "inc"):
        reg[ord(ins[i][1]) - ord('a')] += 1
    elif (ins[i][0] == "dec"):
        reg[ord(ins[i][1]) - ord('a')] -= 1
    elif (ins[i][0] == "jnz"):
        val = 0
        if (str.isalpha(ins[i][1])):
            val = reg[ord(ins[i][1]) - ord('a')]
        else:
            val = int(ins[i][1])

        if (val != 0):
            val2 = 0
            if (not str.isalpha(ins[i][2])):
                val2 = int(ins[i][2])
            else:
                val2 = reg[ord(ins[i][2]) - ord('a')]

            if (val2 == 0):
                val2 = 1

            i += val2
            continue
    elif (ins[i][0] == "cpy"):
        if (not str.isalpha(ins[i][2])):
            i += 1
            continue

        if (str.isalpha(ins[i][1])):
            reg[ord(ins[i][2]) - ord('a')] = reg[ord(ins[i][1]) - ord('a')]
        else:
            reg[ord(ins[i][2]) - ord('a')] = int(ins[i][1])
    elif (' '.join(ins[i]) == "mul b d a"):
        reg[0] += reg[1] * reg[3]
    else:
        x = 0

        if (str.isalpha(ins[i][1])):
            x = reg[ord(ins[i][1]) - ord('a')]
        else:
            x = int(ins[i][1])

        if (i + x >= len(ins) or
            i + x < 0):
            i += 1
            continue

        if (len(ins[i + x]) == 2):
            if (ins[i + x][0] == "inc"):
                ins = ins[:i + x] + [["dec"] + ins[i + x][1:]] + ins[i + x + 1:]
            else:
                ins = ins[:i + x] + [["inc"] + ins[i + x][1:]] + ins[i + x + 1:]
        else:
            if (ins[i + x][0] == "jnz"):
                ins = ins[:i + x] + [["cpy"] + ins[i + x][1:]] + ins[i + x + 1:]
            else:
                ins = ins[:i + x] + [["jnz"] + ins[i + x][1:]] + ins[i + x + 1:]

    i += 1

print(reg[0])
