#!/usr/bin/python3

import sys

sol = 0

s = list(sys.stdin)

for i in range(0, len(s), 3):
    a, b, c = map(int, s[i].split())
    d, e, f = map(int, s[i + 1].split())
    g, h, i = map(int, s[i + 2].split())

    sol += (a + d > g and
            d + g > a and
            g + a > d)

    sol += (b + e > h and
            e + h > b and
            h + b > e)

    sol += (c + f > i and
            f + i > c and
            i + c > f)

print (sol)
