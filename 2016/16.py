#!/usr/bin/python3

def inverse(s):
    s2 = ""

    for i in range(len(s)):
        if (s[i] == '0'):
            s2 += '1'
        else:
            s2 += '0'

    return ''.join(reversed(s2))

b = input()

n = 35651584

while (len(b) < n):
    b = b + '0' + inverse(b)

b = b[:n]

while (len(b) % 2 == 0):
    s = ""

    for i in range(0, len(b), 2):
        if (b[i] == b[i + 1]):
            s += '1'
        else:
            s += '0'

    b = s

print (b)
