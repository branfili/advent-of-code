#!/usr/bin/python3

import sys

n = 8

a = []

for i in range(n):
    a.append([0] * 26)

for w in sys.stdin:
    w = w.rstrip()
    for i in range(len(w)):
        a[i][ord(w[i]) - ord('a')] += 1

sol = ""

for i in range(n):
    mn = 10 ** 9
    mc = ''

    for j in range(26):
        if (a[i][j] < mn):
            mn = a[i][j]
            mc = chr(j + ord('a'))

    sol += mc

print (sol)
