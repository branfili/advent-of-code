#!/usr/bin/python3

import sys

def intersect(a, b):
    return (a[0] <= b[0] and
            b[0] <= a[1])

def union(a, b):
    return (min(a[0], b[0]), max(a[1], b[1]))

inter = []

for w in sys.stdin:
    a, b = map(int, w.split('-'))

    inter.append((a, b))
    inter.sort()

    e = True
    while (e):
        e = False
        i = 0
        while (i < len(inter) - 1):
            if (intersect(inter[i], inter[i + 1])):
                e = True
                inter = inter[:i] + [union(inter[i], inter[i + 1])] + inter[i + 2:]

            i += 1


z = 0
for a, b in inter:
    z += b - a + 1

print(2 ** 32 - z)

