#!/usr/bin/python3

import sys

ins = []

for s in sys.stdin:
    ins.append(s.strip().split())

reg = [0] * 4
reg[2] = 1

i = 0

while (i < len(ins)):
    if (ins[i][0] == "inc"):
        reg[ord(ins[i][1]) - ord('a')] += 1
    elif (ins[i][0] == "dec"):
        reg[ord(ins[i][1]) - ord('a')] -= 1
    elif (ins[i][0] == "jnz"):
        val = 0
        if (str.isalpha(ins[i][1])):
            val = reg[ord(ins[i][1]) - ord('a')]
        else:
            val = int(ins[i][1])

        if (val != 0):
            i += int(ins[i][2])
            continue
    else:
        if (str.isalpha(ins[i][1])):
            reg[ord(ins[i][2]) - ord('a')] = reg[ord(ins[i][1]) - ord('a')]
        else:
            reg[ord(ins[i][2]) - ord('a')] = int(ins[i][1])

    i += 1

print(reg[0])
