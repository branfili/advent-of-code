#!/usr/bin/python3

import sys

ins = []

for s in sys.stdin:
    ins.append(s.strip().split())

n = 1

while (s != "01" * 5):
    reg = [0] * 4
    reg[0] = n

    i = 0

    s = ""

    while (i < len(ins) and
            len(s) < 10):
        if (ins[i][0] == "out"):
            if (str.isalpha(ins[i][1])):
                s += str(reg[ord(ins[i][1]) - ord('a')])
            else:
                s += ins[i][1]
        elif (ins[i][0] == "inc"):
            reg[ord(ins[i][1]) - ord('a')] += 1
        elif (ins[i][0] == "dec"):
            reg[ord(ins[i][1]) - ord('a')] -= 1
        elif (ins[i][0] == "jnz"):
            val = 0
            if (str.isalpha(ins[i][1])):
                val = reg[ord(ins[i][1]) - ord('a')]
            else:
                val = int(ins[i][1])

            if (val != 0):
                val2 = 0
                if (not str.isalpha(ins[i][2])):
                    val2 = int(ins[i][2])
                else:
                    val2 = reg[ord(ins[i][2]) - ord('a')]

                if (val2 == 0):
                    val2 = 1

                i += val2
                continue
        else:
            if (not str.isalpha(ins[i][2])):
                i += 1
                continue

            if (str.isalpha(ins[i][1])):
                reg[ord(ins[i][2]) - ord('a')] = reg[ord(ins[i][1]) - ord('a')]
            else:
                reg[ord(ins[i][2]) - ord('a')] = int(ins[i][1])

        i += 1

    n += 1

n -= 1
print (n)
