#!/usr/bin/python3

def count(s):
    z = 0
    for c in s:
        z += (c == '.')

    return z

def generate(s):
    s2 = ""

    for i in range(len(s)):
        s3 = ""

        for j in range(i - 1, i + 2):
            if (j < 0 or
                j >= len(s)):
                s3 += '.'
                continue

            s3 += s[j]

        if (s3 == "^^." or
            s3 == ".^^" or
            s3 == "^.." or
            s3 == "..^"):
            s2 += '^'
        else:
            s2 += '.'

    return s2

row = input()

z = count(row)

for i in range(1, 400000):
    row = generate(row)
    z += count(row)

print(z)
