#!/usr/bin/python3

import sys

def getRange(s):
    a, b = map(int, s.split('-'))

    return list(range(a, b + 1))

lines = sys.stdin.read()[:-1].split('\n')

rules = []
mt = None
t = []

c = 0
for line in lines:
    if (line == ''):
        c += 1
        continue

    if (c == 0):
        tokens = line.split(': ')
        tokens2 = tokens[1].split(' or ')
        rules.append((tokens[0], getRange(tokens2[0]), getRange(tokens2[1])))
    elif (c == 1):
        if (line[0] == 'y'):
            continue

        mt = list(map(int, line.split(',')))
    elif (c == 2):
        if (line[0] == 'n'):
            continue

        t.append(list(map(int, line.split(','))))

t2 = []
for ticket in t:
    e = False
    for field in ticket:
        if (not any(map(lambda r: (field in r[1]) or (field in r[2]), rules))):
            e = True
            break

    if (not e):
        t2.append(ticket)

t = t2

pos = []
for i in range(len(t[0])):
    l = []
    for j, rule in enumerate(rules):
        if (all(map(lambda ticket: ticket[i] in rule[1] or
                                   ticket[i] in rule[2],
                    t))):
            l.append(j)

    pos.append(l)

pos2 = []
while (any(map(lambda comb: len(comb) == 1, pos))):
    x = (list(map(len, pos))).index(1)
    y = pos[x][0]

    pos2.append((x, y))

    for i in range(len(pos)):
        if (y in pos[i]):
            pos[i].remove(y)

pos = list(map(lambda t: t[1], sorted(pos2, key = lambda t: t[0])))

rules2 = []
for x in pos:
    rules2.append(rules[x])
rules = rules2

p = 1
for i, r in enumerate(rules):
    if (r[0].startswith('departure')):
        p *= mt[i]

print(p)
