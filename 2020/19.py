#!/usr/bin/python3

import sys
import itertools

def rek(r):
    curRule = rules[r]

    if (isinstance(curRule, str)):
        return [curRule]

    result = []
    for variants in curRule:
        cur = None
        for rule in variants:
            res = rek(rule)
            if (cur is None):
                cur = res
            else:
                cur = list(map(lambda t: t[0] + t[1], itertools.product(cur, res)))

        result += cur

    dic[r] = result
    return result

def check(w, a, b):
    x = 0
    while (w[:a] in dic[42]):
        w = w[a:]
        x += 1

    y = 0
    while (w[len(w) - b:] in dic[31]):
        w = w[:len(w) - b]
        y += 1

    return (len(w) == 0 and
            x > y and
            x >= 2 and
            y >= 1)

def solve(w):
    for i in range(1, len(w)):
        for j in range(1, len(w) - i):
            if (check(w, i, j)):
                return True

    return False

lines = sys.stdin.read()[:-1].split('\n')

x = lines.index('')

words = lines[x + 1:]
lines = lines[:x]

rules = {}
for line in lines:
    [key, values] = line.split(': ')

    if (values[0] == '\"'):
        values = values[1]
    else:
        values = list(map(lambda rule: list(map(int, rule.split())),
                                 values.split(' | ')))

    rules[int(key)] = values

dic = {}
rek(0)

s = 0
for word in words:
    s += int(solve(word))

print(s)
