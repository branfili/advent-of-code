#!/usr/bin/python3

nums = list(map(int, input().split(',')))

state = {}

for i in range(len(nums) - 1):
    state[nums[i]] = i

cur = nums[-1]

for x in range(len(nums) - 1, 30000000 - 1):
    if (cur not in state.keys()):
        state[cur] = x
        cur = 0
    else:
        y = x - state[cur]
        state[cur] = x
        cur = y

print(cur)
