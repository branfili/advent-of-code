#!/usr/bin/python3

import sys

def valid(nums, index):
    for i in range(index - 25, index):
        for j in range(i + 1, index):
            if (nums[i] + nums[j] == nums[index]):
                return True

    return False

nums = list(map(int, sys.stdin.read()[:-1].split('\n')))

weak = 0
for i in range(26, len(nums)):
    if (not valid(nums, i)):
        weak = nums[i]
        break

e = True
for i in range(len(nums)):
    for j in range(i + 1, len(nums)):
        if (sum(nums[i:j]) == weak):
            print(min(nums[i:j]) + max(nums[i:j]))
            e = False
            break

    if (not e):
        break
