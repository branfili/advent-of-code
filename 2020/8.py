#!/usr/bin/python3

import sys

def run(inst, index = -1):
    executed = set([])
    acc = 0
    pc = 0

    while (pc >= 0 and pc < len(inst)):
        if (pc in executed):
            return (acc, False)

        executed.add(pc)

        if (inst[pc][0] == 'acc'):
            acc += inst[pc][1]
            pc += 1
        elif (inst[pc][0] == 'jmp'):
            pc += inst[pc][1]
        else:
            pc += 1

    return (acc, True)

def switch(inst, index):
    if (inst[index][0] == 'jmp'):
        inst[index] = ('nop', inst[index][1])
    else:
        inst[index] = ('jmp', inst[index][1])

lines = sys.stdin.read()[:-1].split('\n')
inst = list(map(lambda line: tuple([line.split()[0], int(line.split()[1])]), lines))

print(run(inst)[0])

for i in range(len(inst)):
    if (inst[i][0] == 'acc'):
        continue

    switch(inst, i)
    x, v = run(inst, i)
    switch(inst, i)

    if (v):
        print(x)
        break
