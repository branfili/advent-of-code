#!/usr/bin/python3

def traverse(a, b):
    x = 0
    y = 0
    n = 0

    while (x < len(treesMap)):
        n += int(treesMap[x][y] == '#')

        x += a
        y += b

        y %= len(treesMap[0])

    return n

import sys

treesMap = sys.stdin.read()[:-1].split('\n')

print(traverse(1, 3) * traverse(1, 1) * traverse(1, 5) * traverse(1, 7) * traverse(2, 1))
