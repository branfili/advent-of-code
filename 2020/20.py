#!/usr/bin/python3

import sys
import re
import itertools
import copy
import queue

def rotate(data):
    ns = data[0]
    tile = copy.deepcopy(data[1])

    ns2 = []
    tile2 = []

    for n in ns:
        c = ''
        if (n[2] == 'L'):
            c = 'UR'
        elif (n[2] == 'U'):
            c = 'R'
        elif (n[2] == 'R'):
            c = 'DR'
        elif (n[2] == 'D'):
            c = 'L'
        elif (n[2] == 'LR'):
            c = 'U'
        elif (n[2] == 'UR'):
            c = 'RR'
        elif (n[2] == 'RR'):
            c = 'D'
        elif (n[2] == 'DR'):
            c = 'LR'

        ns2.append((n[0], n[1], c))

    for i in range(len(tile)):
        tile2.append(''.join(list(map(lambda line: line[i], tile))[::-1]))

    return (ns2, tile2)

def flipV(data):
    ns = data[0]
    tile = copy.deepcopy(data[1])

    ns2 = []
    tile2 = tile[::-1]

    for n in ns:
        c = ''
        if (n[2] == 'L'):
            c = 'LR'
        elif (n[2] == 'U'):
            c = 'D'
        elif (n[2] == 'R'):
            c = 'RR'
        elif (n[2] == 'D'):
            c = 'U'
        elif (n[2] == 'LR'):
            c = 'L'
        elif (n[2] == 'UR'):
            c = 'DR'
        elif (n[2] == 'RR'):
            c = 'R'
        elif (n[2] == 'DR'):
            c = 'UR'

        ns2.append((n[0], n[1], c))

    return (ns2, tile2)

def flipH(data):
    ns = data[0]
    tile = copy.deepcopy(data[1])

    ns2 = []
    tile2 = list(map(lambda line: line[::-1], tile))

    for n in ns:
        c = ''
        if (n[2] == 'L'):
            c = 'R'
        elif (n[2] == 'U'):
            c = 'UR'
        elif (n[2] == 'R'):
            c = 'L'
        elif (n[2] == 'D'):
            c = 'DR'
        elif (n[2] == 'LR'):
            c = 'RR'
        elif (n[2] == 'UR'):
            c = 'U'
        elif (n[2] == 'RR'):
            c = 'LR'
        elif (n[2] == 'DR'):
            c = 'D'

        ns2.append((n[0], n[1], c))

    return (ns2, tile2)

def augment(data, r, f):
    for blah in range(r):
        data = rotate(data)

    if (f & 1 != 0):
        data = flipH(data)

    if (f & 2 != 0):
        data = flipV(data)

    return data

def reconstruct(x, par):
    if (x == -1):
        return []

    return ([x] + reconstruct(par[x], par))

def bfs(x, y):
    q = queue.Queue()
    par = {}
    visited = set()

    q.put((x, -1))
    while (not q.empty()):
        cur, curP = q.get()
        visited.add(cur)
        par[cur] = curP

        for n in tileMap[cur]:
            if (n[0] not in visited):
                q.put((n[0], cur))

    path = reconstruct(y, par)
    return path[::-1]

def check(n1, n2):
    n1 = sorted(n1)
    n2 = sorted(n2)

    for a, b in zip(n1, n2):
        if (a[0] != b[0] or
            a[1][0] != b[1][0]):
            return False

    return True

def findSM(seaMap):
    monsterTiles = set()
    for i in range(len(seaMap) - len(monster)):
        for j in range(len(seaMap[0]) - len(monster[0])):
            flag = True
            for k in range(len(monster)):
                for l in range(len(monster[0])):
                    if (monster[k][l] == '#' and
                        (seaMap[i + k][j + l] != '#' or
                         ((i + k, j + l) in monsterTiles))):
                        flag = False
                        break

                if (not flag):
                    break

            if (not flag):
                continue

            for k in range(len(monster)):
                for l in range(len(monster[0])):
                    if (monster[k][l] == '#'):
                        monsterTiles.add((i + k, j + l))

    return monsterTiles

lines = sys.stdin.read()[:-1].split('\n')

borderIDs = ['U', 'UR', 'D', 'DR', 'L', 'LR', 'R', 'RR']

dirs = {}
dirs['L'] = (0, -1)
dirs['R'] = (0, 1)
dirs['U'] = (-1, 0)
dirs['D'] = (1, 0)

monster = ['                  # ',
           '#    ##    ##    ###',
           ' #  #  #  #  #  #   ']

tiles = {}
l = []
tile = None
for line in lines:
    if (line == ''):
        tiles[tile] = l
        l = []
    elif (line.startswith('Tile')):
        tile = int(re.findall('\d+', line)[0])
    else:
        l.append(line)

borders = {}
for ID, tile in tiles.items():
    l = []
    l.append(tile[0])
    l.append(tile[-1])
    l.append(''.join(map(lambda line: line[0], tile)))
    l.append(''.join(map(lambda line: line[-1], tile)))

    l2 = list(map(lambda border: border[::-1], l))

    borders[ID] = []
    for i in range(len(l)):
        borders[ID].append(l[i])
        borders[ID].append(l2[i])

IDs = sorted(list(tiles.keys()))
for ID in IDs:
    tiles[ID] = list(map(lambda line: line[1:-1], tiles[ID][1:-1]))

tileMap = {}
for ID in IDs:
    tileMap[ID] = []

for i in range(len(IDs)):
    for j in range(i + 1, len(IDs)):
        for x, y in itertools.product(list(range(8)), list(range(8))):
            v1 = borders[IDs[i]][x]
            v2 = borders[IDs[j]][y]
            if (v1 == v2):
                tileMap[IDs[i]].append((IDs[j], v1, borderIDs[x]))
                tileMap[IDs[j]].append((IDs[i], v2, borderIDs[y]))
                break

corners = []
for ID, neighbours in tileMap.items():
    if (len(neighbours) == 2):
        corners.append(ID)

paths = {}
for i in range(4):
    for j in range(i + 1, 4):
        mn = min(corners[i], corners[j])
        mx = max(corners[i], corners[j])

        paths[(mn, mx)] = bfs(mn, mx)

short = min(map(len, paths.values()))
edges = sorted(list(filter(lambda t: len(t[1]) == short,
                           paths.items())),
               key = lambda t: t[0])

if (edges[0][0][1] == edges[2][0][1]):
    edges[2] = (edges[2][0], edges[2][1][::-1])
elif (edges[0][0][1] != edges[2][0][0]):
    edges[2], edges[3] = edges[3], edges[2]

    if (edges[0][0][1] == edges[2][0][1]):
        edges[2] = (edges[2][0], edges[2][1][::-1])

if (edges[1][0][1] == edges[3][0][1]):
    edges[3] = (edges[3][0], edges[3][1][::-1])

finalMap = []
for i in range(short):
    l = []
    for j in range(short):
        l.append(-1)
    finalMap.append(l)

done = set()
for i in range(short):
    finalMap[0][i] = edges[0][1][i]
    finalMap[i][0] = edges[1][1][i]
    finalMap[i][-1] = edges[2][1][i]
    finalMap[-1][i] = edges[3][1][i]

    done.add(edges[0][1][i])
    done.add(edges[1][1][i])
    done.add(edges[2][1][i])
    done.add(edges[3][1][i])

for i in range(1, short - 1):
    for j in range(1, short - 1):
        ns = []
        for d in ['U', 'R', 'D', 'L']:
            nx, ny = i + dirs[d][0], j + dirs[d][1]

            if (nx >= 0 and nx < short and \
                ny >= 0 and ny < short and
                finalMap[nx][ny] != -1):
                ns.append(finalMap[nx][ny])

        sol = []
        for ID in IDs:
            if (set(ns).issubset(set(map(lambda n: n[0], tileMap[ID]))) and
                ID not in done):
                sol.append(ID)

        done.add(sol[0])
        finalMap[i][j] = sol[0]

finalMap = list(map(lambda line: line[::-1], finalMap))[::-1]

for ID in IDs:
    x, y = 0, 0
    flag = False
    for i in range(short):
        for j in range(short):
            if (finalMap[i][j] == ID):
                x, y = i, j
                flag = True
                break
        if (flag):
            break

    goal = []
    for d in ['U', 'R', 'D', 'L']:
        nx, ny = x + dirs[d][0], y + dirs[d][1]

        if (nx < 0 or nx >= short or
            ny < 0 or ny >= short):
            continue

        goal.append((finalMap[nx][ny], d))

    ns = copy.deepcopy(tileMap[ID])
    tile = copy.deepcopy(tiles[ID])

    flag = False
    for r in range(4):
        for f in range(4):
            ns2, tile2 = augment((ns, tile), r, f)
            if (check(list(map(lambda n: (n[0], n[2]), ns2)),
                      goal)):
                tileMap[ID] = ns2
                tiles[ID] = tile2
                flag = True
                break

        if (flag):
            break

N = len(tiles[corners[0]])
seaMap = []
for i in range(N * short):
    seaMap.append('')

for i in range(short):
    for j in range(short):
        for k in range(N):
            seaMap[i * N + k] += tiles[finalMap[i][j]][k]

for r in range(4):
    for f in range(4):
        tmp, sm2 = augment(([], seaMap), r, f)
        mTiles = findSM(sm2)
        if (mTiles != set()):
            print(sum(map(lambda line: line.count('#'), seaMap)) - len(mTiles))
