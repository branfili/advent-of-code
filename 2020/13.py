#!/usr/bin/python3

def ext_gcd(a, b):
    if (a == 0):
        return (b, 0, 1)

    t = ext_gcd(b % a, a)

    return (t[0], t[2] - (b // a) * t[1], t[1])

n = int(input())

buses = list(map(lambda t: (int(t[1]) - t[0], int(t[1])), filter(lambda bus : bus[1] != 'x', enumerate(input().split(',')))))

while (len(buses) > 1):
    t = ext_gcd(buses[0][1], buses[1][1])

    x = buses[0][0] * t[2] * buses[1][1] + \
        buses[1][0] * t[1] * buses[0][1]
    y = buses[0][1] * buses[1][1]

    x %= y

    buses = [(x, y)] + buses[2:]

print(buses[0][0])
