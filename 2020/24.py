#!/usr/bin/python3

import sys

def countNeighbours(x, y, z):
    c = 0
    for d in dirs.values():
        nx, ny, nz = x + d[0], y + d[1], z + d[2]

        c += int((nx, ny, nz) in flipped)

    return c

lines = sys.stdin.read()[:-1].split('\n')

dirs = {}
dirs['e'] = (1, -1, 0)
dirs['se'] = (0, -1, 1)
dirs['sw'] = (-1, 0, 1)
dirs['w'] = (-1, 1, 0)
dirs['nw'] = (0, 1, -1)
dirs['ne'] = (1, 0, -1)

flipped = set()
for line in lines:
    start = (0, 0, 0)
    i = 0
    while i < len(line):
        s = line[i]
        if (s == 'n' or s == 's'):
            s += line[i + 1]
            i += 1

        start = (start[0] + dirs[s][0],
                 start[1] + dirs[s][1],
                 start[2] + dirs[s][2])
        i += 1

    if (start in flipped):
        flipped.remove(start)
    else:
        flipped.add(start)

for t in range(100):
    flipped2 = set()
    for f in flipped:
        for d in (list(dirs.values()) + [(0, 0, 0)]):
            x, y, z = f[0] + d[0], f[1] + d[1], f[2] + d[2]
            c = countNeighbours(x, y, z)

            if (((x, y, z not in flipped) and c == 2) or
                ((x, y, z) in flipped) and (c == 1 or c == 2)):
                flipped2.add((x, y, z))

    flipped = flipped2.copy()

print(len(flipped))
