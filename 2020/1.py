#!/usr/bin/python3

import sys

l = list(map(int, sys.stdin.read()[:-1].split('\n')))

for i in range(len(l)):
    for j in range(i + 1, len(l)):
        for k in range(j + 1, len(l)):
            if (l[i] + l[j] + l[k] == 2020):
                print (l[i] * l[j] * l[k])
