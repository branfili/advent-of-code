#!/usr/bin/python3

class Node:
    def __init__(self, x, left = None, right = None):
        self.x = x
        self.left = left
        self.right = right

    def getX(self):
        return self.x

    def getLeft(self):
        return self.left

    def getRight(self):
        return self.right

    def setLeft(self, left):
        self.left = left

    def setRight(self, right):
        self.right = right

cups = list(map(int, list(input())))
N = 10 ** 6
T = 10 ** 7

l = []
for i in range(N):
    l.append(Node(cups[i] if i < len(cups) else (i + 1)))

for i in range(N):
    l[i].setLeft(l[(i - 1 + N) % N])
    l[i].setRight(l[(i + 1) % N])

cur = l[0]
l = sorted(l, key = lambda node: node.getX())

for t in range(T):
    a = cur.getRight()
    b = a.getRight()
    c = b.getRight()
    y = c.getRight()

    g = cur.getX() - 1
    if (g == 0):
        g = N
    while (g in list(map(lambda node: node.getX(), [a, b, c]))):
        g -= 1
        if (g == 0):
            g = N

    cur2 = l[g - 1]

    cur3 = cur2.getRight()
    cur2.setRight(a)
    a.setLeft(cur2)
    c.setRight(cur3)

    cur.setRight(y)
    y.setLeft(cur)

    cur = y

cur = l[0]
print(cur.getRight().getX() * cur.getRight().getRight().getX())
