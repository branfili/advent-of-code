#!/usr/bin/python3

import sys

class Bag:
    def __init__(self, color):
        self.color = color
        self.children = {}
        self.parents = []

    def addBags(self, bag, num):
        self.children[bag] = num

    def addParent(self, bag):
        self.parents.append(bag)

def findBag(color, bags):
    for bag in bags:
        if (bag.color == color):
            return bag

    return None

def traverse(bag):
    c = bag.children
    result = 1
    for k, v in c.items():
        result += v * traverse(k)

    return result

lines = sys.stdin.read()[:-1].split('\n')
bags = []
for line in lines:
    tokens = line.split()

    color = tokens[0] + ' ' + tokens[1]

    b = findBag(color, bags)
    if (b is None):
        b = Bag(color)
        bags.append(b)

    if (tokens[4] == 'no'):
        continue

    for i in range(4, len(tokens), 4):
        x = int(tokens[i])
        color2 = tokens[i + 1] + ' ' + tokens[i + 2]

        b2 = findBag(color2, bags)
        if (b2 is None):
            b2 = Bag(color2)
            bags.append(b2)

        b.addBags(b2, x)
        b2.addParent(b)

print(traverse(findBag('shiny gold', bags)) - 1)
