#!/usr/bin/python3

import sys
import copy

def countActiveNeighbours(x, y, z, w, board):
    s = 0
    for dx in [-1, 0, 1]:
        for dy in [-1, 0, 1]:
            for dz in [-1, 0, 1]:
                for dw in [-1, 0, 1]:
                    if (dx == 0 and
                        dy == 0 and
                        dz == 0 and
                        dw == 0):
                        continue

                    if (x + dx < 0 or
                        x + dx >= N or
                        y + dy < 0 or
                        y + dy >= N or
                        z + dz < 0 or
                        z + dz >= N or
                        w + dw < 0 or
                        w + dw >= N):
                        continue

                    s += int(board[x + dx][y + dy][z + dz][w + dw] == '#')

    return s

def countActive(board):
    s = 0
    for i in range(N):
        for j in range(N):
            for k in range(N):
                for l in range(N):
                    s += int(board[i][j][k][l] == '#')

    return s

N = 30

init = list(map(list, sys.stdin.read()[:-1].split('\n')))

board = []
for i in range(N):
    l1 = []
    for j in range(N):
        l2 = []
        for k in range(N):
            l3 = []
            for l in range(N):
                l3.append('.')
            l2.append(l3)
        l1.append(l2)
    board.append(l1)

for i in range(len(init)):
    for j in range(len(init[0])):
        board[N // 2][N // 2][N // 2 + i][N // 2 + j] = init[i][j]

for t in range(6):
    board2 = copy.deepcopy(board)
    for i in range(N):
        for j in range(N):
            for k in range(N):
                for l in range(N):
                    x = countActiveNeighbours(i, j, k, l, board)

                    if (board[i][j][k][l] == '#' and
                        not (x == 2 or x == 3)):
                        board2[i][j][k][l] = '.'
                    elif (board[i][j][k][l] == '.' and
                          x == 3):
                        board2[i][j][k][l] = '#'

    board = board2

print(countActive(board))
