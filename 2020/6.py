#!/usr/bin/python3

import sys

def convertToBitvector(answer):
    result = 0
    for i in range(26):
        c = chr(i + ord('a'))

        if (c in answer):
            result = result | (1 << i)

    return result

def countOnes(bitvector):
    count = 0
    for i in range(26):
        count += int(bitvector & (1 << i) != 0)

    return count

lines = sys.stdin.read().split('\n')

groups = []
g = (1 << 26) - 1
for line in lines:
    if (line == ''):
        groups.append(g)
        g = (1 << 26) - 1
    else:
        g = g & convertToBitvector(line)

print(sum(map(lambda g : countOnes(g), groups)))
