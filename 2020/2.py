#!/usr/bin/python3

import sys

def xor(a, b):
    return ((a or b) and not(a and b))

l = sys.stdin.read()[:-1].split('\n')

n = 0

for line in l:
    tokens = line.split()

    a, b = map(lambda x : int(x) - 1, tokens[0].split('-'))

    if xor(tokens[2][a] == tokens[1][0], tokens[2][b] == tokens[1][0]):
        n += 1

print(n)
