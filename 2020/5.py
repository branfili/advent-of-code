#!/usr/bin/python3

import sys

def seatNumber(boardPass):
    r = int(boardPass[:7].replace('F', '0').replace('B', '1'), 2)
    c = int(boardPass[7:].replace('L', '0').replace('R', '1'), 2)

    return (r, c)

def getID(pos):
    return (pos[0] * 8 + pos[1])

lines = sys.stdin.read()[:-1].split('\n')

A = set([])
B = set(list(range(886)))

for line in lines:
    A.add(getID(seatNumber(line)))

print(B - A)
