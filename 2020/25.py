#!/usr/bin/python3

import math

def exp(a, b, MOD):
    if (b == 0):
        return 1

    if (b == 1):
        return a % MOD

    h = exp(a, b >> 1, MOD)
    m = exp(a, b & 1, MOD)

    return (h * h * m) % MOD

def babyStep(a, b):
    table = {}
    x = 1
    for i in range(0, sM + 1):
        table[x] = i
        x = (x * a) % MOD

    inv = exp(a, MOD - 1 - sM, MOD)
    gama = b

    for i in range(0, sM):
        if (gama in table):
            return (i * sM + table[gama])

        gama = (gama * inv) % MOD

    return None

def extended_gcd(a, b):
    if (a == 0):
        return (b, 0, 1)

    t = extended_gcd(b % a, a)

    return (t[0], t[2] - (b // a) * t[1], t[1])

def getLog(a, b):
    factors2 = factors.copy()

    solutions = []
    for f in factors2:
        n = (MOD - 1) // f

        ai = exp(a, n, MOD)
        bi = exp(b, n, MOD)

        solutions.append(babyStep(ai, bi))

    if (None in solutions):
        return None

    while (len(factors2) > 1):
        t = extended_gcd(factors2[0], factors2[1])

        x = solutions[0] * t[2] * factors2[1] + \
            solutions[1] * t[1] * factors2[0]
        y = factors2[0] * factors2[1]
        x %= y

        solutions = [x] + solutions[2:]
        factors2 = [y] + factors2[2:]

    return solutions[0]

a = int(input())
b = int(input())

MOD = 20201227
sM = int(math.ceil(math.sqrt(MOD)))

N = 10 ** 6
sN = int(math.ceil(math.sqrt(N)))

primes = set(range(2, N))
for x in range(2, sN):
    if (x not in primes):
        continue

    for y in range(x * x, N, x):
        if (y in primes):
            primes.remove(y)

factors = []
n = MOD - 1
for p in primes:
    while (n % p == 0):
        n //= p
        factors.append(p)

if (n != 1):
    factors.append(n)

for p in primes:
    x = getLog(p, a)
    if (x is None):
        continue

    y = getLog(p, b)
    if (y is not None):
        print(p, x, y)
        print(exp(p, x * y, MOD))
        break
