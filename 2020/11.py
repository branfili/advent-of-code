#!/usr/bin/python3

import sys

def countOccupiedNeighbours(x, y, seats):
    pomakX = [-1, -1, 0, 1, 1, 1, 0, -1]
    pomakY = [0, 1, 1, 1, 0, -1, -1, -1]

    count = 0
    for i, j in zip(pomakX, pomakY):
        dx = i
        dy = j

        while (0 <= x + dx and x + dx < len(seats) and
               0 <= y + dy and y + dy < len(seats[0]) and
               seats[x + dx][y + dy] == '.'):
            dx += i
            dy += j

        if (x + dx < 0 or x + dx >= len(seats)):
            continue

        if (y + dy < 0 or y + dy >= len(seats[0])):
            continue

        count += int(seats[x + dx][y + dy] == '#')

    return count

def move(seats):
    seats2 = []
    for i in range(len(seats)):
        l = []
        for j in range(len(seats[i])):
            if (seats[i][j] == 'L' and
                countOccupiedNeighbours(i, j, seats) == 0):
                l.append('#')
            elif (seats[i][j] == '#' and
                  countOccupiedNeighbours(i, j, seats) >= 5):
                l.append('L')
            else:
                l.append(seats[i][j])

        seats2.append(l)

    return seats2

def countOccupiedSeats(seats):
    count = 0
    for i in range(len(seats)):
        for j in range(len(seats[i])):
            count += int(seats[i][j] == '#')

    return count

seats = list(map(lambda line: list(line), sys.stdin.read()[:-1].split('\n')))

while True:
    print(countOccupiedSeats(seats))
    seats = move(seats)
