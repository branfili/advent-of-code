#!/usr/bin/python3

import sys

lines = sys.stdin.read()[:-1].split('\n')

food = []

allIng = set()
allAl = set()

for line in lines:
    tokens = line.split()
    x = tokens.index('(contains')

    ing = tokens[:x]
    alr = list(map(lambda token: token[:-1], tokens[x + 1:]))

    allIng = allIng.union(set(ing))
    allAl = allAl.union(set(alr))

    food.append((set(ing), set(alr)))

imposFood = set()
aler = []
for al in allAl:
    s = allIng.copy()
    for i in range(len(food)):
        if (al in food[i][1]):
            s = s.intersection(food[i][0])
    aler.append((al, s))

    imposFood = imposFood.union(s)

c = 0
for ing, alr in food:
    for i in ing:
        if (i not in imposFood):
            c += 1

print(c)

aler = sorted(aler, key = lambda t: t[0])
puzzle = list(map(lambda t: list(t[1]), aler))
solution = list(map(lambda t: '*', puzzle))

while (any(map(lambda pos: len(pos) > 0, puzzle))):
    x = list(map(len, puzzle)).index(1)
    solution[x] = puzzle[x][0]
    out = puzzle[x][0]

    for i in range(len(puzzle)):
        if (out in puzzle[i]):
            puzzle[i].remove(out)

print(','.join(solution))
