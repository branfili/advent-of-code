#!/usr/bin/python3

import sys
import copy

def score(l):
    s = 0
    for i in range(len(l)):
        s += (len(l) - i) * l[i]

    return s

def combat(p1, p2):
    conf = set()

    while (len(p1) > 0 and len(p2) > 0):
        if ((tuple(p1), tuple(p2)) in conf):
            return score(p1)

        conf.add((tuple(p1), tuple(p2)))

        x = p1[0]
        y = p2[0]

        p1 = p1[1:]
        p2 = p2[1:]

        if (len(p1) >= x and
            len(p2) >= y):
            s = combat(copy.deepcopy(p1[:x]),
                       copy.deepcopy(p2[:y]))

            if (s > 0):
                p1 = p1 + [x, y]
            else:
                p2 = p2 + [y, x]

            continue

        if (x > y):
            p1 = p1 + [x, y]
        else:
            p2 = p2 + [y, x]

    if (len(p2) == 0):
        return score(p1)
    else:
        return -score(p2)

lines = sys.stdin.read()[:-1].split('\n')
x = lines.index('')

p1 = list(map(int, lines[1:x]))
p2 = list(map(int, lines[x + 2:]))

print(abs(combat(p1, p2)))
