#!/usr/bin/python3

import sys

def appli(x, mask):
    nmask = ''
    z = 0
    for i, c in enumerate(mask[::-1]):
        if (c == '0'):
            nmask += str(x % 2)
        else:
            nmask += c
            z += int(c == 'X')

        x //= 2

    return (nmask[::-1], z)

lines = sys.stdin.read()[:-1].split('\n')

mask = ''
mem = {}
for line in lines:
    tokens = line.split()

    if (tokens[0].startswith('mask')):
        mask = tokens[2]
    elif (tokens[0].startswith('mem')):
        l = tokens[0].find('[')
        r = tokens[0].find(']')

        ap, c = appli(int(tokens[0][l + 1 : r]), mask)

        for i in range(1 << c):
            y = i
            cp = ''

            for j, c in enumerate(ap):
                if (c == 'X'):
                    cp += str(y % 2)
                    y //= 2
                else:
                    cp += c

            mem[int(cp[::-1], 2)] = int(tokens[2])

print(sum(mem.values()))
