#!/usr/bin/python3

import sys

def operate(op, a, b):
    if (a is None):
        return b

    if (op == '+'):
        return (a + b)
    elif (op == '-'):
        return (a - b)
    elif (op == '*'):
        return (a * b)
    elif (op == '/'):
        return (a / b)

def evaluate(s):
    p = None
    op = None
    s2 = ''
    i = 0
    while (i < len(s)):
        if (s[i] == '+'):
            op = '+'
        elif (s[i] == '-'):
            op = '-'
        elif (s[i] == '*'):
            s2 += str(p) + '*'
            p = None
        elif (s[i] == '/'):
            s2 += str(p) + '/'
            p = None
        elif (s[i] == '('):
            d = 1
            j = i + 1
            while (d > 0):
                if (s[j] == '('):
                    d += 1
                elif (s[j] == ')'):
                    d -= 1
                j += 1

            p = operate(op, p, evaluate(s[i + 1 : j - 1]))
            i = j - 1
        elif (s[i].isdigit()):
            j = i
            while (j < len(s) and
                   s[j].isdigit()):
                j += 1

            p = operate(op, p, int(s[i : j]))
            i = j - 1

        i += 1

    if (p is not None):
        s2 += str(p)

    p = None
    op = None
    i = 0
    while (i < len(s2)):
        if (s2[i] == '*'):
            op = '*'
        elif (s2[i] == '/'):
            op = '/'
        elif (s2[i].isdigit()):
            j = i
            while (j < len(s2) and
                   s2[j].isdigit()):
                j += 1

            p = operate(op, p, int(s2[i : j]))
            i = j - 1

        i += 1

    return p

tasks = sys.stdin.read()[:-1].split('\n')

s = 0
for task in tasks:
    s += evaluate(''.join(task.split()))
print(s)
