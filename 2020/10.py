#!/usr/bin/python3

import sys

def rek(x):
    if (x < 0):
        return 0

    if (x == 0):
        return 1

    if (x not in nums):
        return 0

    if (dp[x] != -1):
        return dp[x]

    dp[x] = rek(x - 1) + rek(x - 2) + rek(x - 3)

    return dp[x]

nums = sorted(list(map(int, sys.stdin.read()[:-1].split('\n'))))

ad = max(nums) + 3
nums = [0] + nums + [ad]

dp = []
for i in range(ad + 1):
    dp.append(-1)

print(rek(ad))
