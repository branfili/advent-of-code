#!/usr/bin/python3

import sys

def rotate(dx, dy):
    return (-dy, dx)

dirs = list(map(lambda line: (line[0], int(line[1:])), sys.stdin.read()[:-1].split('\n')))

x = 0
y = 0
v = 1

wx = 10
wy = 1

dx = [0, 1, 0, -1]
dy = [1, 0, -1, 0]
sides = ['N', 'E', 'S', 'W']

for d, mag in dirs:
    if (d == 'L'):
        for i in range(mag // 90):
            wx, wy = rotate(wx, wy)
    elif (d == 'R'):
        for i in range((4 - mag // 90) % 4):
            wx, wy = rotate(wx, wy)
    elif (d != 'F'):
        curV = sides.index(d)

        wx += dx[curV] * mag
        wy += dy[curV] * mag
    else:
        x += wx * mag
        y += wy * mag

print(abs(x) + abs(y))
