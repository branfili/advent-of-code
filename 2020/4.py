#!/usr/bin/python3

import sys

def validate(hexcode):
    hexsigns = ['0',
                '1',
                '2',
                '3',
                '4',
                '5',
                '6',
                '7',
                '8',
                '9',
                'a',
                'b',
                'c',
                'd',
                'e',
                'f']

    for c in hexcode:
        if (c not in hexsigns):
            return False

    return True

def isGood(passport):
    fields = set(passport.keys())

    fields = fields - {'cid'}

    if (fields != goodPassportFields):
        return False

    b = int(passport['byr'])
    if (b < 1920 or b > 2002):
        return False

    i = int(passport['iyr'])
    if (i < 2010 or i > 2020):
        return False

    e = int(passport['eyr'])
    if (e < 2020 or e > 2030):
        return False

    if (passport['hgt'][-2:] not in ['in', 'cm']):
        return False

    h = int(passport['hgt'][:-2])
    if (passport['hgt'][-2:] == 'cm'):
        if (h < 150 or h > 193):
            return False
    else:
        if (h < 59 or h > 76):
            return False

    if (passport['hcl'][0] != '#' or
        not validate(passport['hcl'][1:])):
        return False

    if (passport['ecl'] not in goodEyeColours):
        return False

    return (len(passport['pid']) == 9)

goodPassportFields = set(['byr',
                          'iyr',
                          'eyr',
                          'hgt',
                          'hcl',
                          'ecl',
                          'pid'])

goodEyeColours = ['amb',
                  'blu',
                  'brn',
                  'gry',
                  'grn',
                  'hzl',
                  'oth']

fields = sum(list(map(lambda line: line.split() if line != '' else ['X'], sys.stdin.read().split('\n'))), [])

passport = {}
n = 0

for field in fields:
    if (field == 'X'):
        n += int(isGood(passport))
        passport = {}
    else:
        key, value = field.split(':')
        passport[key] = value

print(n)
