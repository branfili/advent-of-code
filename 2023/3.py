#!/usr/bin/python3

import sys
import re
from collections import defaultdict

schematic = sys.stdin.read()[:-1].split('\n')

W, H = len(schematic[0]), len(schematic)

schematic = [' ' * W] + schematic + [' ' * W]
schematic = list(map(lambda s: ' ' + s + ' ', schematic))
schematic = ''.join(schematic).replace('.', ' ')

pointer = -1
s = 0
gearnos = defaultdict(lambda: [])
while True:
    pointer += 1
    no = re.search('(\d+)', schematic[pointer:])
    if (no is None):
        break

    a, b = no.span()
    a += pointer
    b += pointer
    pointer = b
    nolen = b - a

    nhood = schematic[a - (W + 2) - 1:b - (W + 2) + 1] + schematic[a - 1] + schematic[b] + schematic[a + (W + 2) - 1:b + (W + 2) + 1]
    nhood2 = nhood.strip()

    if (nhood2 == '*'):
        x = nhood.index('*')
        if (x < nolen + 2):
            x += a - (W + 2) - 1
        else:
            x -= (nolen + 2)
            if (x == 0):
                x = a - 1
            elif (x == 1):
                x = b
            else:
                x -= 2
                x += a + (W + 2) - 1
        gearnos[x].append(int(no[0]))

    if (len(nhood2) > 0):
        s += int(no[0])

print(s)

s2 = 0
for l in gearnos.values():
    if (len(l) == 2):
        s2 += l[0] * l[1]
print(s2)
