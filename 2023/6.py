#!/usr/bin/python3

from math import sqrt

def joinList(l):
    return [int(''.join(map(str, l)))]

times = list(map(int, input().split()[1:]))
distances = list(map(int, input().split()[1:]))

times = joinList(times)
distances = joinList(distances)

sol = 1
for t, d in zip(times, distances):
    b = -t
    c = d + 1

    s = int(sqrt(b * b - 4 * c))
    if (s % 2 != b % 2):
        s -= 1

    x1 = (-b - s) // 2
    x2 = (-b + s) // 2

    mode = t // 2
    maks = (t - mode) * mode

    sol *= (x2 - x1 + 1)

print(sol)
