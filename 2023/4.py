#!/usr/bin/python3

import sys

games = sys.stdin.read()[:-1].split('\n')

cards = [1] * len(games)

s = 0
for i, l in enumerate(games):
    l1, l2 = l.split(':')[1].split('|')
    l1 = set(list(map(int, l1.strip().split())))
    l2 = set(list(map(int, l2.strip().split())))
    ln = len(l1 & l2)

    if (ln > 0):
        s += 1 << (ln - 1)

    for j in range(i + 1, i + ln + 1):
        cards[j] += cards[i]

print(s)
print(sum(cards))
