#!/usr/bin/python3

import sys

sys.setrecursionlimit(20000)
dirs = [(0, 1), (1, 0), (0, -1), (-1, 0)]

cave = sys.stdin.read()[:-1].split('\n')
H, W = len(cave), len(cave[0])

def add(a, b):
    return (a[0] + b[0], a[1] + b[1])

def beam(p, d):
    global visited, cave, dirs
    global H, W
    if ((p, d) in visited):
        return

    visited.add((p, d))

    sgn = cave[p[0]][p[1]]
    ds = []

    if (sgn == '.'):
        ds = [d]
    elif (sgn == '-'):
        if (d in [0, 2]):
            ds = [d]
        else:
            ds = [0, 2]
    elif (sgn == '|'):
        if (d in [1, 3]):
            ds = [d]
        else:
            ds = [1, 3]
    elif (sgn == '/'):
        ds = [3 - d]
    elif (sgn == '\\'):
        ds = [d ^ 1]
    else:
        ds = [d]

    for d2 in ds:
        np = add(p, dirs[d2])

        if (np[0] >= 0 and np[0] < H and
            np[1] >= 0 and np[1] < W and
            (np, d2) not in visited):
            beam(np, d2)


def run(p, d):
    global visited
    visited = set()

    beam(p, d)

    sol = len(set(map(lambda s: s[0], visited)))
    return sol

m = 0
for i in range(H):
    m = max(m, run((i, 0), 0))
    m = max(m, run((i, W - 1), 2))

for j in range(W):
    m = max(m, run((0, j), 1))
    m = max(m, run((H - 1, j), 3))

print(run((0, 0), 0))
print(m)
