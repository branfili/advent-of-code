#!/usr/bin/python3

import sys
from collections import defaultdict

neighbours = [(0, 1), (1, 0), (0, -1), (-1, 0)]
allowedSigns = ['-7J', '|LJ', '-FL', '|F7']

def add(a, b):
    return (a[0] + b[0], a[1] + b[1])

def diff(a, b):
    return (a[0] - b[0], a[1] - b[1])

pipes = sys.stdin.read()[:-1].split('\n')
H, W = len(pipes), len(pipes[0])

for i in range(H):
    for j in range(W):
        if (pipes[i][j] == 'S'):
            cur = (i, j)

passed = []
while True:
    sgn = pipes[cur[0]][cur[1]]

    if (sgn == 'S' and passed):
        break

    if (sgn == 'S'):
        for i, d in enumerate(neighbours):
            ncur = add(cur, d)
            if (ncur[0] < 0 or ncur[0] >= H or
                ncur[1] < 0 or ncur[1] >= W):
                continue

            nsgn = pipes[ncur[0]][ncur[1]]

            if (nsgn in allowedSigns[i]):
                break

    elif (sgn == '-'):
        ncur = add(cur, neighbours[0])
        if (ncur == passed[-1]):
            ncur = add(cur, neighbours[2])

    elif (sgn == '|'):
        ncur = add(cur, neighbours[1])
        if (ncur == passed[-1]):
            ncur = add(cur, neighbours[3])

    elif (sgn == '7'):
        ncur = add(cur, neighbours[1])
        if (ncur == passed[-1]):
            ncur = add(cur, neighbours[2])

    elif (sgn == 'J'):
        ncur = add(cur, neighbours[2])
        if (ncur == passed[-1]):
            ncur = add(cur, neighbours[3])

    elif (sgn == 'F'):
        ncur = add(cur, neighbours[0])
        if (ncur == passed[-1]):
            ncur = add(cur, neighbours[1])

    elif (sgn == 'L'):
        ncur = add(cur, neighbours[0])
        if (ncur == passed[-1]):
            ncur = add(cur, neighbours[3])

    passed.append(cur)
    cur = ncur

print(len(passed) // 2)

pos = None
for i in range(H):
    for j in range(W):
        cur = (i, j)
        if (cur in passed):
            pos = cur
            break

    if (pos is not None):
        break

x = passed.index(pos)
d = diff(passed[(x + 1) % len(passed)], pos)
if (d == (0, 1)):
    out = (-1, 0)
else:
    out = (0, -1)

allowed = defaultdict(lambda: neighbours)
for i in range(x + 1, x + len(passed)):
    i %= len(passed)

    cur = passed[i]
    sgn = pipes[cur[0]][cur[1]]

    d1 = diff(passed[i - 1], cur)
    d2 = diff(passed[(i + 1) % len(passed)], cur)

    if (sgn in '-|' or
        (sgn == 'S' and
            (d1[0] == d2[0] or
             d1[1] == d2[1]))):
        allowed[cur] = set([out, d1, d2])
    elif (d2 == out):
        out = d1
        allowed[cur] = set([d1, d2])
    else:
        out = diff((0, 0), d1)

q = []
for i in range(H):
    q.append((i, 0))
    q.append((i, W - 1))

for i in range(W):
    q.append((0, i))
    q.append((0, H - 1))

visited = set()
while q:
    cur = q[0]
    q = q[1:]

    if (cur in visited):
        continue

    visited.add(cur)

    for d in allowed[cur]:
        nCur = add(cur, d)
        if (nCur[0] >= 0 and nCur[0] < H and
            nCur[1] >= 0 and nCur[1] < W and
            nCur not in visited):
            q.append(nCur)

print(H * W - len(visited))
