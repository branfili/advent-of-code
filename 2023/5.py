#!/usr/bin/python3

import sys

lines = sys.stdin.read()[:-1].split('\n')

ids = list(map(int, lines[0].split(': ')[1].split()))

pointer = 0
l = []
while True:
    try:
        x = lines[pointer:].index('')
        pointer += x + 1
        l.append(pointer)
    except ValueError:
        break

l.append(len(lines))

for i in range(len(l) - 1):
    intervals = list(map(lambda line: list(map(int, line.split())), lines[l[i] + 1:l[i + 1] - 1]))
    intervals = sorted(intervals, key = lambda i: i[1])

    ids2 = []
    #for oid in ids:
    #    nid = oid
    #    for d, s, ln in intervals:
    #        if (s <= oid and oid < s + ln):
    #            nid = oid - s + d
    #
    #    ids2.append(nid)
    for i in range(0, len(ids), 2):
        a, ln = ids[i], ids[i + 1]

        cur = a
        for d, s, ln2 in intervals:
            if (cur < s):
                if (a + ln <= s):
                    break

                ids2 += [cur, s - cur]
                cur = s
            elif (cur < s + ln2):
                ids2 += [cur - s + d, min(s + ln2, a + ln) - cur]
                cur = min(s + ln2, a + ln)
                if (cur == a + ln):
                    break
            else:
                continue

            if (a + ln <= s + ln2):
                ids2 += [cur - s + d, a + ln - cur]
                cur = a + ln
                break
            elif (cur < s + ln2):
                ids2 += [cur - s + d, ln2]
                cur = s + ln2

        if (cur < a + ln):
            ids2 += [cur, a + ln - cur]

    ids = ids2

#print(min(ids))
print(min(ids[::2]))
