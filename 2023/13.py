#!/usr/bin/python3

import sys
import copy

def column(m, x):
    s = ''
    for l in m:
        s += l[x]
    return s

def invert(c):
    if (c == '.'):
        return '#'
    else:
        return '.'

def findMirror(m, x):
    H, W = len(m), len(m[0])

    m2 = []
    for i in range(W):
        m2.append(column(m, i))

    for i in range(H - 1):
        e = True
        for j in range(min(i + 1, H - i - 1)):
            if (m[i - j] != m[i + 1 + j]):
                e = False
                break

        if (e):
            out = (i + 1) * 100
            if (out == x):
                continue

            return out

    for i in range(W - 1):
        e = True
        for j in range(min(i + 1, W - i - 1)):
            if (m2[i - j] != m2[i + 1 + j]):
                e = False
                break

        if (e):
            out = i + 1
            if (out == x):
                continue

            return out

    return 0

lines = sys.stdin.read().split('\n')

cur = -1
sol = 0
while True:
    cur += 1
    x = lines[cur:].index('')
    x += cur

    m = lines[cur:x]
    cur = x

    H, W = len(m), len(m[0])

    sol1 = findMirror(m, 0)

    e = False
    for i in range(H):
        for j in range(W):
            m2 = copy.deepcopy(m)
            m2[i] = m2[i][:j] + invert(m2[i][j]) + m2[i][j + 1:]
            sol2 = findMirror(m2, sol1)

            if (sol2 != 0):
                sol += sol2
                e = True
                break

        if (e):
            break

    if (cur >= len(lines) - 1):
        break

print(sol)
