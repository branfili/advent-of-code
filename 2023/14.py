#!/usr/bin/python3

import sys

dirs = {'N': (-1, 0), 'E': (0, 1), 'S': (1, 0), 'W': (0, -1)}

def add(a, b):
    return (a[0] + b[0], a[1] + b[1])

def sub(a, b):
    return (a[0] - b[0], a[1] - b[1])

def withinBorders(p, H, W):
    return (p[0] >= 0 and p[0] < H and
            p[1] >= 0 and p[1] < W)

def move(platform, ds):
    H, W = len(platform), len(platform[0])

    for d in ds:
        d = dirs[d]

        start = []
        for i in range(H):
            for j in range(W):
                cur = (i, j)
                nCur = add(cur, d)
                if (not withinBorders(nCur, H, W)):
                    start.append(cur)

        for p in start:
            cur = p
            while withinBorders(cur, H, W):
                if (platform[cur[0]][cur[1]] == 'O'):
                    cur2 = cur
                    while True:
                        nCur = add(cur2, d)
                        if (withinBorders(nCur, H, W) and platform[nCur[0]][nCur[1]] == '.'):
                            platform[nCur[0]][nCur[1]] = 'O'
                            platform[cur2[0]][cur2[1]] = '.'
                            cur2 = nCur
                        else:
                            break

                cur = sub(cur, d)

    return platform

def serialize(platform):
    H, W = len(platform), len(platform[0])
    s = ''
    for l in platform:
        s += ''.join(l)
    s += '_' + str(H) + '_' + str(W)
    return s

def deserialize(s):
    s, H, W = s.split('_')
    H, W = int(H), int(W)
    l = []
    for i in range(0, H * W, W):
        l.append(list(s[i: i + W]))
    return l

def calcLoad(s):
    platform = deserialize(s)
    H, W = len(platform), len(platform[0])

    sol = 0
    for i in range(H):
        for j in range(W):
            if (platform[i][j] == 'O'):
                sol += (H - i)
    return sol

platform = list(map(list, sys.stdin.read()[:-1].split('\n')))

H, W = len(platform), len(platform[0])
N = 10 ** 9

history = []
for kh in range(N):
    platform = move(platform, 'NWSE')
    s = serialize(platform)

    if (s in history):
        x = history.index(s)
        y = len(history) - x

        print(calcLoad(history[(N - x - 1) % y + x]))
        break
    else:
        history.append(s)

if (len(history) == N):
    print(calcLoad(history[-1]))
