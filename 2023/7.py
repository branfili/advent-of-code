#!/usr/bin/python3

import sys
from functools import cmp_to_key

order = 'J23456789TQKA'

def sgn(x):
    return int(x / abs(x))

def countHands(h):
    out = [0] * len(order)
    for c in h:
        out[order.index(c)] += 1

    if (out[0] > 0):
        m = max(out[1:])
        for i in range(1, len(out)):
            if (out[i] == m):
                out[i] += out[0]
                out[0] = 0
                break

    return sorted(out)

def compareCamelHands(h1, h2):
    freq1 = countHands(h1)
    freq2 = countHands(h2)

    m1 = freq1[-1]
    m2 = freq2[-1]

    if (freq1[-2] == 2):
        m1 += 0.5

    if (freq2[-2] == 2):
        m2 += 0.5

    if (m1 != m2):
        return sgn(m1 - m2)

    for c1, c2 in zip(h1, h2):
        if (c1 != c2):
            return sgn(order.index(c1) - order.index(c2))

    return 0

hands = []
bids = {}

for line in sys.stdin:
    hand, bid = line.split()
    hands.append(hand)
    bids[hand] = int(bid)

hands = sorted(hands, key = cmp_to_key(compareCamelHands))

s = 0
for i, h in enumerate(hands):
    s += (i + 1) * bids[h]
print(s)
