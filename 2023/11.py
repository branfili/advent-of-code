#!/usr/bin/python3

import sys
from bisect import bisect_left

def getStrip(m, i):
    s = ''
    for l in m:
        s += l[i]
    return s

N = 10 ** 6

galaxies = sys.stdin.read()[:-1].split('\n')

emptyR = []
for i in range(len(galaxies)):
    if ('#' not in galaxies[i]):
        emptyR.append(i)

emptyC = []
for j in range(len(galaxies[0])):
    if ('#' not in getStrip(galaxies, j)):
        emptyC.append(j)

gs = set()
for i in range(len(galaxies)):
    for j in range(len(galaxies[0])):
        if (galaxies[i][j] == '#'):
            r = i
            c = j
            x = bisect_left(emptyR, r)
            y = bisect_left(emptyC, c)
            gs.add((r + (N - 1) * x, c + (N - 1) * y))

s = 0
for g in gs:
    for g2 in gs:
        s += abs(g[0] - g2[0]) + abs(g[1] - g2[1])

s //= 2
print(s)
