#!/usr/bin/python3

import sys
import copy

class Node:
    state = None
    inputs = None

    def __init__(self, name, typ, nds):
        self.name = name
        self.typ = typ
        self.nds = copy.deepcopy(nds)

        if (self.typ == '&'):
            self.state = set()
            self.inputs = set()
        else:
            self.state = False

    def getPulse(self, origin, isHigh):
        l = []
        if (not self.nds):
            return l

        if (self.typ is None):
            for n in self.nds:
                l.append((self.name, isHigh, n))
        elif (self.typ == '%'):
            if (not isHigh):
                self.state = not self.state
                for n in self.nds:
                    l.append((self.name, self.state, n))
        elif (self.typ == '&'):
            if (isHigh):
                self.state.add(origin)
            elif (not isHigh and origin in self.state):
                self.state.remove(origin)

            for n in self.nds:
                l.append((self.name, self.state != self.inputs, n))

        return l

def gcd(a, b):
    if (b == 0):
        return a

    return gcd(b, a % b)

def lcm(a, b):
    return a * b // gcd(a, b)

N = 1000

nodes = {}
for line in sys.stdin:
    name, nds = line[:-1].split(' -> ')
    nds = nds.split(', ')

    if (name != 'broadcaster'):
        typ, name = name[0], name[1:]
    else:
        typ = None

    nodes[name] = Node(name, typ, nds)

nodes['button'] = Node('button', None, ['broadcaster'])

to_add = []
for n in nodes.values():
    for i, nd in enumerate(n.nds):
        if (nd not in nodes.keys()):
            nn = Node(nd, None, None)
            to_add.append(nn)
        else:
            nn = nodes[nd]

        n.nds[i] = nn
        if (nn.typ == '&'):
            nn.inputs.add(n.name)

for n in to_add:
    nodes[n.name] = n

states = {}
for n in nodes.values():
    if (n.typ == '&'):
        states[n.name] = {}
        for nn in n.inputs:
            states[n.name][nn] = []

memory = {}
visited = []
i = 0
l, h = 0, 0
while True:
    i += 1
    q = [(None, False, nodes['button'])]
    while q:
        origin, pulse, dest = q[0]
        q = q[1:]

        out = dest.getPulse(origin, pulse)
        for _, p, _ in out:
            if (p):
                h += 1
            else:
                l += 1

        q += out

        if (dest.typ == '&' and
            pulse and
            len(states[dest.name][origin]) < 2):
            states[dest.name][origin].append(i)

    st = []
    for n in nodes.values():
        if (n.state):
            st.append(n.name)

    st = ','.join(sorted(st))

    visited.append(st)
    memory[st] = (l, h)

    if (i == N):
        sl, sh = memory[st]
        print(sl * sh)

    done = True
    for n in nodes.values():
        if (n.typ != '&'):
            continue

        for nn in n.inputs:
            if (len(states[n.name][nn]) < 2):
                done = False
                break

        if (not done):
            break

    if (done):
        break

for n in nodes.values():
    if (n.typ != '&'):
        continue

    for nn in n.inputs:
        l = states[n.name][nn]
        states[n.name][nn] = (l[0], l[1] - l[0])

z = 1
for a, n in states['ns'].values():
    z = lcm(z, n)

print(z)
