#!/usr/bin/python3

import sys
import heapq
from collections import defaultdict

def add(a, b):
    return (a[0] + b[0], a[1] + b[1])

def dfs(cur, d, prev, slope, last):
    global graph, hikeMap, dirs, dirsReverse
    x, y = cur

    if (hikeMap[x][y] == '.'):
        l = dirs.values()
    else:
        l = [dirs[hikeMap[x][y]]]

    l2 = []
    for dr in l:
        nCur = add(cur, dr)

        if (nCur[0] >= 0 and nCur[0] < H and
            nCur[1] >= 0 and nCur[1] < W and
            hikeMap[nCur[0]][nCur[1]] in ['.', dirsReverse[dr]] and
            nCur != prev):
            l2.append(nCur)

    if (hikeMap[x][y] == '>'):
        slope = True

    if (len(l2) != 1):
        if ((d, cur) in graph[last]):
            return

        graph[last].append((d, cur))
        if (not slope):
            graph[cur].append((d, last))

        last = cur
        d = 0

    for x in l2:
        dfs(x, d + 1, cur, slope, last)

dirs = {'>': (0, 1), 'v': (1, 0), '<': (0, -1), '^':(-1, 0)}

hikeMap = list(map(list, sys.stdin.read()[:-1].split('\n')))
H, W = len(hikeMap), len(hikeMap[0])

start = (0, hikeMap[0].index('.'))
end = (H - 1, hikeMap[H - 1].index('.'))

dirsReverse = {}
for k, v in dirs.items():
    dirsReverse[v] = k

for i in range(H):
    for j in range(W):
        if (hikeMap[i][j] != '#'):
            hikeMap[i][j] = '.'

sys.setrecursionlimit(H * W + 10)
graph = defaultdict(lambda: [])
dfs(start, 0, None, False, start)

graphReverse = defaultdict(lambda: [])
for k, l in graph.items():
    for d, v in l:
        graphReverse[v].append((d, k))

q = [(0, end)]
ds = {}
vis = set()
while q:
    d, cur = q.pop(0)
    x, y = cur

    if (cur in vis):
        continue

    ds[cur] = d
    vis.add(cur)

    for d2, x in graphReverse[cur]:
        if (x not in vis):
            q.append((d + d2, x))

pq = [(-ds[start], start, set())]
heapq.heapify(pq)
sol = 0
while pq:
    d, cur, visited = heapq.heappop(pq)
    x, y = cur

    d += ds[cur]

    if (cur == end):
        sol = max(sol, -d)

    for d2, x in graph[cur]:
        if (x not in visited):
            heapq.heappush(pq, (d - d2 - ds[x], x, visited | set([x])))

print(sol)
