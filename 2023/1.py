#!/usr/bin/python3

import sys
import regex as re

digits = ['zero', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine']
pattern = '|'.join(digits)

def convert(token):
    if (token.isdigit()):
        return int(token)
    else:
        return digits.index(token)

s = 0
for line in sys.stdin:
    tokens = list(map(convert, re.findall('\d|'+pattern, line, overlapped = True)))
    s += tokens[0] * 10 + tokens[-1]

print(s)
