#!/usr/bin/python3

import sys

def tSlice(t, x):
    return (t[0][x], t[1][x])

def intersect(l1, l2):
    return (max(l1[0], l2[0]) <= min(l1[1], l2[1]))

def rek(ID, gone):
    global p, c

    gone |= set([ID])

    sol = set([ID])
    for cID in c[ID]:
        if (set(p[cID]).issubset(gone)):
            sol |= rek(cID, gone)

    return sol

bs = []
for i, line in enumerate(sys.stdin):
    s, e = line.split('~')
    bs.append((i,
               (list(map(int, s.split(','))),
               list(map(int, e.split(','))))))

N = len(bs)
bs = sorted(bs, key = lambda b: max(tSlice(b[1], 2)))

p = {}
c = {}
for i in range(N):
    p[i] = []
    c[i] = []

for i in range(N):
    idI, b = bs[i]

    Z = 0
    for j in range(i - 1, -1, -1):
        idJ, b2 = bs[j]

        if (max(tSlice(b2, 2)) < Z):
            break

        if (intersect(tSlice(b, 0), tSlice(b2, 0)) and
            intersect(tSlice(b, 1), tSlice(b2, 1))):
            p[idI].append(idJ)
            c[idJ].append(idI)

            Z = max(tSlice(b2, 2))

    l1 = b[0]
    l2 = b[1]

    height = l2[2] - l1[2]
    l1[2] = Z + 1
    l2[2] = l1[2] + height

    bs[i] = (idI, (l1, l2))

    bs = sorted(bs[:i + 1], key = lambda b: max(tSlice(b[1], 2))) + bs[i + 1:]

canDis = 0
sol = 0
for ID in range(N):
    s = rek(ID, set()) - set([ID])

    if (not s):
        canDis += 1

    sol += len(s)

print(canDis)
print(sol)
