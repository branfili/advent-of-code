#!/usr/bin/python3

import sys
import copy
from operator import lt, gt

class Range:
    intervals = None

    def __init__(self, _intervals):
        self.intervals = copy.deepcopy(_intervals)

    def split(r, el):
        att, cond, v = el

        l = r.intervals[att]

        if (cond == lt):
            v1, v2 = v - 1, v
        else:
            v1, v2 = v, v + 1

        sol1 = Range(r.intervals)
        sol2 = Range(r.intervals)

        for i, (a, b) in enumerate(l):
            if (v1 >= b):
                continue

            if (v2 <= a):
                sol1.intervals[att] = l[:i]
                sol2.intervals[att] = l[i:]
            else:
                sol1.intervals[att] = l[:i] + [(a, v1)]
                sol2.intervals[att] = [(v2, b)] + l[i + 1:]

            break

        return sol1, sol2

#    def union(r1, r2):
#        d = {}
#        for k in r1.intervals.keys():
#            l1 = r1.intervals[k]
#            l2 = r2.intervals[k]
#
#            p1, p2 = 0, 0
#            n1, n2 = len(l1), len(l2)
#            l = []
#            reserve = None
#            while (p1 < n1 or p2 < n2):
#                if (reserve is not None):
#                    if (p1 < n1 and min(l1[p1][1], reserve[1]) + 1 >= max(l1[p1][0], reserve[0])):
#                        reserve = (min(reserve[0], l1[p1][0]), max(reserve[1], l1[p1][1]))
#                        p1 += 1
#                    elif (p2 < n2 and min(l2[p2][1], reserve[1]) + 1 >= max(l2[p2][0], reserve[0])):
#                        reserve = (min(reserve[0], l2[p2][0]), max(reserve[1], l2[p2][1]))
#                        p2 += 1
#                    else:
#                        l.append(reserve)
#                        reserve = None
#
#                else:
#                    if (p1 == n1):
#                        l.append(l2[p2])
#                        p2 += 1
#                    elif (p2 == n2):
#                        l.append(l1[p1])
#                        p1 += 1
#                    else:
#                        a1, b1 = l1[p1]
#                        a2, b2 = l2[p2]
#
#                        if (b1 < a2):
#                            l.append(l1[p1])
#                            p1 += 1
#                        elif (b2 < a1):
#                            l.append(l2[p2])
#                            p2 += 1
#                        else:
#                            a1, b1 = l1[p1]
#                            a2, b2 = l2[p2]
#
#                            reserve = (min(a1, a2), max(b1, b2))
#
#                            p1 += 1
#                            p2 += 1
#
#            if (reserve is not None):
#                l.append(reserve)
#
#            d[k] = l
#
#        return Range(d)
#
#    def diff(r1, r2):
#        d = {}
#        for k in r1.intervals.keys():
#            l1 = r1.intervals[k]
#            l2 = r2.intervals[k]
#
#            p1, p2 = 0, 0
#            n1, n2 = len(l1), len(l2)
#            l = []
#            reserve = None
#            while (p1 < n1):
#                a1, b1 = l1[p1]
#
#                if (reserve is not None):
#                    if (reserve[1] < a1):
#                        reserve = None
#                        continue
#                    elif (reserve[1] < b1):
#                        if (reserve[1] >= a1):
#                            l.append((reserve[1] + 1, b1))
#                            p1 += 1
#                        reserve = None
#                        continue
#                    else:
#                        if (reserve[1] == b1):
#                            reserve = None
#                        else:
#                            reserve = (b1 + 1, reserve[1])
#
#                        p1 += 1
#                else:
#                    if (p2 == n2):
#                        l.append(l1[p1])
#                        p1 += 1
#                    else:
#                        a2, b2 = l2[p2]
#
#                        if (b2 < a1):
#                            p2 += 1
#                            continue
#
#                        if (a1 == b1 and a2 == b2):
#                            p1 += 1
#                            continue
#                        elif (b1 < a2):
#                            l.append(l1[p1])
#                            p1 += 1
#                            continue
#
#                        if (a1 < a2):
#                            l.append((a1, a2 - 1))
#
#                        if (b1 > b2):
#                            l.append((b1 + 1, b2))
#                        elif (b1 < b2):
#                            reserve = (b1 + 1, b2)
#
#                        p1 += 1
#                        p2 += 1
#
#
#            d[k] = l
#
#        return Range(d)

class Workflow:
    name = None
    conds = None

    def __init__(self, l):
        x = l.index('{')

        self.name = l[:x]
        l = l[x + 1: -1].split(',')

        self.conds = []
        for token in l:
            if (':' not in token):
                if (token in 'RA'):
                    token = (token == 'A')

                self.conds.append((None, None, None, token))
                continue

            att, cond, v, res = None, None, None, None

            if ('<' in token):
                cond = lt
                x = token.index('<')
            else:
                cond = gt
                x = token.index('>')
            y = token.index(':')

            att = token[:x]
            v = int(token[x + 1:y])
            res = token[y + 1:]

            if (res in 'RA'):
                res = (res == 'A')

            self.conds.append((att, cond, v, res))

    def evaluate(self, part):
        global workflows

        for att, cond, v, res in self.conds:
            if (att is None):
                return res

            if (cond(part.atts[att], v)):
                return res

    def constrainRange(self, r):
        global workflows

        #sol = Range({'x': [], 'm': [], 'a': [], 's': []})
        sol = []
        for att, cond, v, res in self.conds:
            if (att is None):
                if (isinstance(res, bool)):
                    if (res):
                        #sol = Range.union(sol, r)
                        sol.append(r)
                    #else:
                    #    sol = Range.diff(sol, r)
                else:
                    #sol = Range.union(sol, workflows[res].constrainRange(r))
                    sol += workflows[res].constrainRange(r)

                continue

            r1, r2 = Range.split(r, (att, cond, v))
            if (cond == gt):
                r1, r2 = r2, r1

            r = r2

            if (isinstance(res, bool)):
                if (res):
                    #sol = Range.union(sol, r1)
                    sol.append(r1)
                #else:
                #    sol = Range.diff(sol, r1)
            else:
                #sol = Range.union(sol, workflows[res].constrainRange(r1))
                sol += workflows[res].constrainRange(r1)

        return sol

class Part:
    atts = None

    def __init__(self, l):
        l = l[1:-1].split(',')

        self.atts = {}
        for token in l:
            k, v = token.split('=')
            v = int(v)

            self.atts[k] = v

lines = sys.stdin.read()[:-1].split('\n')
x = lines.index('')

workflows = {}
for l in lines[:x]:
    w = Workflow(l)
    workflows[w.name] = w

sol = 0
for l in lines[x + 1:]:
    p = Part(l)

    w = workflows['in']
    while True:
        w = w.evaluate(p)
        if (isinstance(w, bool)):
            break
        else:
            w = workflows[w]

    if (w):
        sol += sum(p.atts.values())

print(sol)

rs = workflows['in'].constrainRange(Range({'x': [(1, 4000)], 'm': [(1, 4000)], 'a': [(1, 4000)], 's': [(1, 4000)]}))
sol = 0
for r in rs:
    sl = 1
    for k, v in r.intervals.items():
        s = 0
        for a, b in v:
            s += (b - a + 1)

        sl *= s
    sol += sl

print(sol)
