#!/usr/bin/python3

def hash(s):
    h = 0
    for c in s:
        h += ord(c)
        h *= 17
        h %= 256
    return h

inst = input().split(',')

sol = 0
for s in inst:
    sol += hash(s)
print(sol)

boxes = []
for i in range(256):
    boxes.append([])
val = {}

for s in inst:
    if ('=' in s):
        s, n = s.split('=')
        h = hash(s)
        n = int(n)

        val[s] = n

        if (s not in boxes[h]):
            boxes[h].append(s)

    else:
        s = s[:-1]
        h = hash(s)

        if (s in boxes[h]):
            boxes[h].remove(s)

sol = 0
for i, b in enumerate(boxes):
    for j, t in enumerate(b):
        sol += (i + 1) * (j + 1) * val[t]
print(sol)
