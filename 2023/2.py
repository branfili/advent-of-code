#!/usr/bin/python3

import sys
import regex as re
from functools import reduce
import operator

bag = {'red': 12, 'green': 13, 'blue': 14}

def isPossible(draw):
    for c, n in draw:
        if bag[c] < n:
            return False
    return True

s = 0
for game in sys.stdin:
    header, draws = game[:-1].split(':')
    draws = draws.split(';')
    gid = int(re.search('(\d+)', header)[0])

    minBag = {'red': 0, 'blue': 0, 'green': 0}
    for d in draws:
        tokens = d.strip().split(',')
        draw = []
        for t in tokens:
            n, c = t.strip().split()
            minBag[c] = max(minBag[c], int(n))

    s += reduce(operator.mul, minBag.values(), 1)

print(s)
