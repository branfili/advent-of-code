#!/usr/bin/python3

import sys

class Node:
    name, left, right = None, None, None

    def __init__(self, name, left = None, right = None):
        self.name = name
        self.left = left
        self.right = right

def gcd(a, b):
    if (b == 0):
        return a

    return gcd(b, a % b)

def lcm(a, b):
    return (a * b // gcd(a, b))

namesNodes = {}

inst = input()
input()

for l in sys.stdin:
    name, _, a, b = l.split()
    a = a[1:-1]
    b = b[:-1]

    if (name in namesNodes.keys()):
        name = namesNodes[name]
    else:
        name = Node(name)
        namesNodes[name.name] = name

    if (a in namesNodes.keys()):
        a = namesNodes[a]
    else:
        a = Node(a)
        namesNodes[a.name] = a

    if (b in namesNodes.keys()):
        b = namesNodes[b]
    else:
        b = Node(b)
        namesNodes[b.name] = b

    name.left = a
    name.right = b

cur = []
for name in namesNodes.keys():
    if (name[-1] == 'A'):
        cur.append(namesNodes[name])

sol = 1
for c in cur:
    i = 0
    while True:
        if (inst[i % len(inst)] == 'L'):
            c = c.left
        else:
            c = c.right

        i += 1

        if (c.name[-1] == 'Z'):
            break

    sol = lcm(sol, i)

print(sol)
