#!/usr/bin/python3

import sys

mem = {}

def gen(s, l, x, y):
    if (y == len(l)):
        if ('#' not in s[x:]):
            return 1
        else:
            return 0

    if ((x, y) in mem.keys()):
        return mem[(x, y)]

    a = l[y]

    if (x + a - 1 >= len(s)):
        return 0

    sol = 0
    if ('.' not in s[x : x + a] and
        (x + a == len(s) or
         s[x + a] != '#')):
        sol += gen(s, l, x + a + 1, y + 1)

    if (s[x] != '#'):
        sol += gen(s, l, x + 1, y)

    mem[(x, y)] = sol
    return sol

def solve(s, l):
    global mem
    mem = {}
    sol = gen(s, l, 0, 0)
    return sol

sol = 0
for line in sys.stdin:
    s, l = line.split()
    l = list(map(int, l.split(',')))

    s = '?'.join([s] * 5)
    l = l * 5

    sol += solve(s, l)

print(sol)
