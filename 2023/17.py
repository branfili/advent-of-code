#!/usr/bin/python3

import sys
import heapq

def add(a, b):
    return (a[0] + b[0], a[1] + b[1])

def check(state):
    global H, W, visited
    p = state[-1]

    return (p[0] >= 0 and p[0] < H and
            p[1] >= 0 and p[1] < W and
            state not in visited)

def getHeat(p):
    global city
    return int(city[p[0]][p[1]])

dirs = [(0, 1), (1, 0), (0, -1), (-1, 0)]

city = sys.stdin.read()[:-1].split('\n')
H, W = len(city), len(city[0])

pq = []
heapq.heapify(pq)
heapq.heappush(pq, (0, (0, 0, (0, 0))))
heapq.heappush(pq, (0, (0, 1, (0, 0))))

visited = set()
while pq:
    heat, state = heapq.heappop(pq)
    stab, d, p = state

    if (state in visited):
        continue

    visited.add(state)

    if (stab < 9):
        nP = add(p, dirs[d])
        nState = (stab + 1, d, nP)

        if (check(nState)):
            nHeat = heat + getHeat(nP)
            heapq.heappush(pq, (nHeat, nState))

        if (stab < 3):
            continue

    if (p == (H - 1, W - 1)):
        print(heat)
        break

    for x in [1, -1]:
        d2 = (d + x + 4) % 4

        nP = add(p, dirs[d2])
        nState = (0, d2, nP)

        if (check(nState)):
            nHeat = heat + getHeat(nP)
            heapq.heappush(pq, (nHeat, nState))
