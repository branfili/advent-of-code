#!/usr/bin/python3

import sys

def isZeroes(l):
    return (l.count(0) == len(l))

s = 0
for line in sys.stdin:
    readings = list(map(int, line.split()))

    lasts = [readings[-1]]

    l = [readings]
    while (not isZeroes(l[-1])):
        l2 = []
        for i in range(len(l[-1]) - 1):
            l2.append(l[-1][i + 1] - l[-1][i])

        l.append(l2)

    l[-1].insert(0, 0)
    for i in range(len(l) - 2, -1, -1):
        l[i].insert(0, l[i][0] - l[i + 1][0])

    s += l[0][0]

print(s)
