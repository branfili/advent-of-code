#!/usr/bin/python3

import sys

dirs = {'R': (0, 1), 'D': (-1, 0), 'L': (0, -1), 'U': (1, 0)}

def add(a, b):
    return (a[0] + b[0], a[1] + b[1])

def cmul(c, a):
    return (c * a[0], c * a[1])

def getLength(line):
    line = sorted(line)

    sol = 0
    c = 0
    for i, p in enumerate(line):
        x, st = p

        if (x != line[i - 1][0]):
            sol += (x - line[i - 1][0]) * (1 if c > 0 else 0)

        c += st

    return sol

def addEvent(e):
    global line
    x, st = e

    if (st == 1 and (x + 1, -1) in line):
        line.remove((x + 1, -1))
    elif (st == -1 and (x - 1, 1) in line):
        line.remove((x - 1, 1))
    else:
        line.append((x, st))

p = (0, 0)

edges = []
for line in sys.stdin:
    d, x, code = line.split()
    x = int(x)
    code = code[2:-1]

    d, x = list(dirs.keys())[int(code[-1])], int(code[0:5], 16)

    p2 = add(p, cmul(x, dirs[d]))

    if (p[1] == p2[1]):
        y = p[1]
        x1 = min(p[0], p2[0])
        x2 = max(p[0], p2[0])

        if (p[0] < p2[0]):
            edges.append((y, 1, (x1, x2)))
        else:
            edges.append((y + 1, -1, (x1, x2)))

    p = p2

edges = sorted(edges, key = lambda s: (s[0], s[1]))

line = []
sol = 0
for i, e in enumerate(edges):
    y, st, (x1, x2) = e

    if (i > 0 and
        y != edges[i - 1][0]):
        sol += (y - edges[i - 1][0]) * getLength(line)

    if (st == 1):
        addEvent((x1, 1))
        addEvent((x2 + 1, -1))
    else:
        addEvent((x1 + 1, -1))
        addEvent((x2, 1))

print(sol)
