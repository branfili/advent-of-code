#!/usr/bin/python3

from functools import reduce

def rek():
    global message, i

    v = int(message[i : i + 3], 2)
    i += 3

    pid = int(message[i : i + 3], 2)
    i += 3
    if (pid == 4):
        s = ''
        while (message[i] == '1'):
            s += message[i + 1 : i + 5]
            i += 5

        s += message[i + 1 : i + 5]
        i += 5

        return int(s, 2)
    else:
        l = []

        if (message[i] == '0'):
            i += 1
            x = int(message[i : i + 15], 2)
            i += 15

            end = i + x
            while (i < end):
                l.append(rek())
        else:
            i += 1
            x = int(message[i : i + 11], 2)
            i += 11

            for j in range(x):
                l.append(rek())

        if (pid == 0):
            return sum(l)
        elif (pid == 1):
            return reduce(lambda x, y: x * y, l)
        elif (pid == 2):
            return min(l)
        elif (pid == 3):
            return max(l)
        elif (pid == 5):
            return (1 if l[0] > l[1] else 0)
        elif (pid == 6):
            return (1 if l[0] < l[1] else 0)
        elif (pid == 7):
            return (1 if l[0] == l[1] else 0)

line = input()

message = ''
for c in line:
    message += '{:04b}'.format(int(c, 16))

i = 0
print(rek())
