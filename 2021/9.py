#!/usr/bin/python3

import sys

global visited, ds, caveMap, N, M

visited = set([])
ds = [(1, 0), (0, -1), (-1, 0), (0, 1)]

caveMap = sys.stdin.read().split('\n')[:-1]
M, N = len(caveMap), len(caveMap[0])

def floodfill(t):
    global visited, ds, caveMap, N, M

    x, y = t

    if (x < 0 or \
        x >= N or \
        y < 0 or \
        y >= M):
        return 0

    if (caveMap[x][y] == '9'):
        return 0

    if (t in visited):
        return 0

    s = 1
    visited.add(t)

    for dx, dy in ds:
        s += floodfill((x + dx, y + dy))

    return s

l = []
for y in range(len(caveMap)):
    for x in range(len(caveMap[y])):
        if (caveMap[x][y] == '9'):
            continue

        if ((x, y) not in visited):
            l.append(floodfill((x, y)))

l = sorted(l, reverse = True)
print(l[0] * l[1] * l[2])
