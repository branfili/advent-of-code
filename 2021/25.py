#!/usr/bin/python3

import sys

lines = sys.stdin.read().split('\n')[:-1]

seaFloorMap = list(map(lambda line: list(line.strip()), lines))
N, M = len(seaFloorMap), len(seaFloorMap[0])

moves = set([])
for step in range(1, 10 ** 8):
    movesMade = []
    for cucumber in ['>', 'v']:
        for i in range(N):
            for j in range(M):
                if (seaFloorMap[i][j] != cucumber):
                    continue

                nx, ny = i, j
                if (cucumber == '>'):
                    ny += 1
                    if (ny == M):
                        ny = 0
                elif (cucumber == 'v'):
                    nx += 1
                    if (nx == N):
                        nx = 0

                if (seaFloorMap[nx][ny] == '.'):
                    moves.add(((i, j), (nx, ny)))

        for (x, y), (nx, ny) in moves:
            seaFloorMap[nx][ny] = seaFloorMap[x][y]
            seaFloorMap[x][y] = '.'

        movesMade.append(bool(moves))
        moves = set([])

    if (not any(movesMade)):
        break

print(step)
