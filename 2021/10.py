#!/usr/bin/python3

import sys

lines = sys.stdin.read().split('\n')[:-1]

score = {
         ')': 3,
         ']': 57,
         '}': 1197,
         '>': 25137
        }

complete = {
            '(': '1',
            '[': '2',
            '{': '3',
            '<': '4',
           }

lefts = '({[<'
rights = ')}]>'

comp = []
result = 0
for l in lines:
    s = []
    flag = True
    for c in l:
        if (c in lefts):
            s.append(c)
        else:
            x = rights.index(c)
            if (s[-1] != lefts[x]):
                result += score[c]
                flag = False
                break
            else:
                s.pop()

    if (not flag):
        continue

    st = ''
    s = list(reversed(s))
    for c in s:
        st += complete[c]

    comp.append(int(st, 5))

comp = sorted(comp)

print(result)
print(comp[len(comp)//2])
