#!/usr/bin/python3

import sys
import numpy as np
from collections import defaultdict, deque
from itertools import permutations
from math import sin, cos, radians
from copy import deepcopy

from time import time

stime = time()

def add(a, b):
    return (a[0] + b[0], \
            a[1] + b[1], \
            a[2] + b[2])

def diff(a, b):
    return (a[0] - b[0], \
            a[1] - b[1], \
            a[2] - b[2])

def dist(s1, s2):
    d = diff(s1, s2)

    return (abs(d[0]) + abs(d[1]) + abs(d[2]))

def rotMatrix(axis, angle):
    c = int(round(cos(radians(angle))))
    s = int(round(sin(radians(angle))))

    if (axis == 'X'):
        return np.array([[1, 0, 0], [0, c, -s], [0, s, c]])
    elif (axis == 'Y'):
        return np.array([[c, 0, s], [0, 1, 0], [-s, 0, c]])
    elif (axis == 'Z'):
        return np.array([[c, -s, 0], [s, c, 0], [0, 0, 1]])

def rotate(p, m):
    x = np.array(list(p)).T
    x = np.matmul(m, x)
    return tuple(x)

def findMatch(c1, c2, m):
    global clouds, minMatch

    p1 = clouds[c1]
    p2 = list(map(lambda p: rotate(p, m), deepcopy(clouds[c2])))

    pointDiff1 = {}
    pointDiff2 = {}

    for i, j in permutations(range(len(p1)), 2):
        pointDiff1[(i, j)] = diff(p1[i], p1[j])

    for i, j in permutations(range(len(p2)), 2):
        pointDiff2[(i, j)] = diff(p2[i], p2[j])

    matchingSet = set([])
    for k1, v1 in pointDiff1.items():
        for k2, v2 in pointDiff2.items():
            if (v1 == v2):
                matchingSet.add((k1, k2))
                break

    if (len(matchingSet) >= minMatch * (minMatch - 1)):
        refPoint = list(matchingSet)[0]
        return diff(p1[refPoint[0][0]], p2[refPoint[1][0]])

minMatch = 12

lines = sys.stdin.read().split('\n')

clouds = []
points = []
for l in lines:
    if (l == ''):
        clouds.append(points)
        continue

    if (l.startswith('---')):
        points = []
        continue

    points.append(tuple(map(int, l.split(','))))

graph = defaultdict(lambda: [])
q = deque()
q.append(0)
notSeen = set(range(1, len(clouds)))
while (q):
    cur = q.popleft()
    itList = deepcopy(notSeen)
    for newNode in itList:
        if (newNode not in notSeen):
            continue

        flag = False
        for d in ['+X', '+Y', '+Z', '-X', '-Y', '-Z']:
            for angle in range(0, 360, 90):
                a = 0
                if (d[1] == 'Y'):
                    a = 90
                elif (d[1] == 'Z'):
                    a = 270

                a += 0 if d[0] == '+' else 180

                m = rotMatrix('Z', a) if d[1] != 'Z' else rotMatrix('Y', a)

                a2 = angle
                if (d[0] == '-'):
                    a2 += 180

                m = np.matmul(rotMatrix(d[1], a2), m)

                dv = findMatch(cur, newNode, m)
                if (dv is not None):
                    graph[cur].append((newNode, dv))
                    clouds[newNode] = list(map(lambda p: rotate(p, m), clouds[newNode]))
                    q.append(newNode)
                    notSeen -= set([newNode])
                    flag = True
                    break

            if (flag):
                break

q.append((0, (0, 0, 0)))
seen = set([])
beacons = set([])
scanners = set([])
while (q):
    cur, d = q.popleft()
    seen.add(cur)
    scanners.add(d)

    for p in clouds[cur]:
        beacons.add(add(d, p))

    for newNode, newD in graph[cur]:
        if (newNode in seen):
            continue

        q.append((newNode, add(d, newD)))

print(len(beacons))

m = 0
scanners = list(scanners)
for s1, s2 in permutations(scanners, 2):
    m = max(m, dist(s1, s2))

print(m)

print(f'Time: {time() - stime:.3f} seconds')
