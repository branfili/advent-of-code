#!/usr/bin/python3

import math
from itertools import product

def tri(n):
    return (n * (n + 1)) // 2

def roundInterval(inter):
    return (int(math.ceil(inter[0])), \
            int(math.floor(inter[1])))

_, _, ix, iy = input().split()

ix = ix[2:-1]
iy = iy[2:]

minx, maxx = map(int, ix.split('..'))
miny, maxy = map(int, iy.split('..'))

minvx, maxvx = 0, 0

for i in range(1, int(math.ceil(2 * math.sqrt(maxx)))):
    if (tri(i) < minx):
        minvx = i

    if (tri(i) <= maxx):
        maxvx = i
    else:
        break

minvx += 1

minvy, maxvy = 0, 0
for i in range(1, int(math.ceil(2 * math.sqrt(abs(miny))))):
    if (tri(i) < abs(maxy)):
        minvy = i

    if (tri(i) <= abs(maxy)):
        maxvy = i
    else:
        break

minvy += 1

print(tri(abs(miny) - 1))

s = set([])

for t in range(1, maxvx + 1):
    a = t
    b = -tri(t - 1)

    curxmin, curxmax = roundInterval(((minx - b) / a, (maxx - b) / a))

    curymin, curymax = roundInterval(((miny - b) / a, (maxy - b) / a))

    s = s.union(product(list(range(curxmin, curxmax + 1)), \
                        list(range(curymin - 1, curymax))))

for t in range(1, maxvy + 1):
    a = t
    b = tri(t - 1)

    curymin, curymax = roundInterval(((abs(maxy) - b) / a, (abs(miny) - b) / a))

    if (2 * (curymin - 1) + t >= minvx):
        s = s.union(product(list(range(minvx, maxvx + 1)), \
                            list(range(curymin - 1, curymax))))
    else:
        s = s.union(product(list(range(minvx, maxvx + 1)),
                            list(range(int(math.ceil((minvx - t) / 2)) + 1, curymax))))

print(len(s))
