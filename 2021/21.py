#!/usr/bin/python3

def add(a, b):
    return (a[0] + b[0], \
            a[1] + b[1])

def mult(c, a):
    return (c * a[0], \
            c * a[1])

def simulate(state, turn):
    global seenStates, diceDistribution

    state2 = (state, turn)
    score, pos = state

    if (state2 in seenStates.keys()):
        return seenStates[state2]

    if (score[0] >= 21):
        return (1, 0)

    if (score[1] >= 21):
        return (0, 1)

    score = list(score)
    pos = list(pos)

    p2 = pos[turn]
    s2 = score[turn]

    result = (0, 0)
    for i, dWays in enumerate(diceDistribution):
        pos[turn] = (pos[turn] - 1 + (i + 3)) % 10 + 1
        score[turn] += pos[turn]

        result = add(result, mult(dWays, simulate((tuple(score), tuple(pos)), turn ^ 1)))

        pos[turn] = p2
        score[turn] = s2

    seenStates[state2] = result
    return result

diceDistribution = [1, 3, 6, 7, 6, 3, 1]

startPos = (4, 3)
seenStates = {}

w1, w2 = simulate(((0, 0), startPos), 0)

print(max(w1, w2))
