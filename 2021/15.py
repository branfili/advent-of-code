#!/usr/bin/python3

import sys

from heapq import heappush as push
from heapq import heappop as pop

ds = {
      (0, 1), \
      (1, 0), \
      (0, -1), \
      (-1, 0)
     }

lines = sys.stdin.read().split('\n')[:-1]

caves = list(map(lambda line: list(map(int, line.strip())), lines))

N, M = len(caves), len(caves[0])

newCaves = []
for i in range(5 * N):
    l = []
    for j in range(5 * M):
        x = caves[i % N][j % M] + (i // N) + (j // M)
        l.append((x % 10) + (x // 10))
    newCaves.append(l)

N *= 5
M *= 5

pq = []
paths = {}

push(pq, (0, (0, 0)))
while pq:
    d, (x, y) = pop(pq)
    if ((x, y) in paths.keys()):
        continue

    paths[(x, y)] = d

    for dx, dy in ds:
        nx, ny = x + dx, y + dy

        if (nx < 0 or \
            ny < 0 or \
            nx >= N or \
            ny >= M):
            continue

        push(pq, (d + newCaves[nx][ny], (nx, ny)))

print(paths[(N - 1, M - 1)])
