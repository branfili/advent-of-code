#!/usr/bin/python3

import sys

def program(inp):
    global instructions

    mem = [0, 0, 0, 0]

    for l in instructions:
        t = l.split()

        iNum = ord(t[1]) - ord('w')

        if (t[0] == 'inp'):
            mem[iNum] = inp[0]
            inp = inp[1:]
            continue

        x = None
        if (t[2] in 'wxyz'):
            x = mem[ord(t[2]) - ord('w')]
        else:
            x = int(t[2])

        if (t[0] == 'add'):
            mem[iNum] += x
        elif (t[0] == 'mul'):
            mem[iNum] *= x
        elif (t[0] == 'div'):
            mem[iNum] //= x
        elif (t[0] == 'mod'):
            mem[iNum] %= x
        elif (t[0] == 'eql'):
            mem[iNum] = (1 if mem[iNum] == x else 0)

    return mem

def generateSolutions(rels, sol):
    global allSolutions

    if (not rels):
        allSolutions.append(int(''.join(sol)))
        return

    for i in range(1, 10):
        if (i + rels[0][2] not in range(1, 10)):
            continue

        sol[rels[0][0]] = str(i)
        sol[rels[0][1]] = str(i + rels[0][2])

        generateSolutions(rels[1:], sol)

lines = sys.stdin.read()

instructions = lines.split('\n')[:-1]

codes = lines.split('inp w\n')[1:]
st = []
relations = []
for i, code in enumerate(codes):
    codeLines = code.split('\n')[:-1]

    if (codeLines[3].split()[2] == '1'):
        st.append((i, int(codeLines[-3].split()[2])))
    else:
        i2, x = st.pop()
        x += int(codeLines[4].split()[2])

        relations.append((i2, i, x))

sol = []
for i in range(14):
    sol.append('x')

allSolutions = []
generateSolutions(relations, sol)

allSolutions = sorted(allSolutions)
print(min(allSolutions), max(allSolutions))
