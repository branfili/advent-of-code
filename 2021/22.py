#!/usr/bin/python3

import sys

def solve(reactor):
    if (not reactor):
        return 0

    if (not reactor[0][1]):
        return reactor[-1][0]

    ev = []
    for i, (typ, inter) in enumerate(reactor):
        xmin, xmax = inter[0]

        ev.append(((xmin, i), (typ, inter[1:])))
        ev.append(((xmax + 1, i), (typ ^ 1, inter[1:])))

    ev = sorted(ev)

    result = 0
    curA = 0
    l = []
    lastPos = None
    for (pos, ID), (typ, inter) in ev:
        if (lastPos is not None):
            result += curA * (pos - lastPos)
        lastPos = pos

        if ((ID, typ ^ 1, inter) in l):
            l.remove((ID, typ ^ 1, inter))
        else:
            l.append((ID, typ, inter))

        l = sorted(l)

        curA = solve(list(map(lambda t: tuple(list(t)[1:]), l)))

    return result

lines = sys.stdin.read().split('\n')[:-1]

reactor = []

for l in lines:
    tokens = l.split()

    typ = (0 if tokens[0] == 'off' else 1)
    inter = list(map(lambda l: tuple(map(int, l[2:].split('..'))), \
                    tokens[1].split(',')))

    reactor.append((typ, inter))

v = solve(reactor)

print(v)
