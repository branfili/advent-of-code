#!/usr/bin/python3

import sys

lines = sys.stdin.read().split('\n')[:-1]

nums = list(map(int, lines[0].split(',')))

boards = []

for i in range(2, len(lines), 6):
    l = []
    for j in range(5):
        l.append(list(map(int, lines[i + j].split())))

    boards.append(l)

b = []
for n in nums:
    for i in range(len(boards)):
        for j in range(5):
            for k in range(5):
                if (boards[i][j][k] == n):
                    boards[i][j][k] = -1

    for i in range(len(boards)):
        if (i in b):
            continue

        s = 0
        for j in range(5):
            for k in range(5):
                if (boards[i][j][k] != -1):
                    s += boards[i][j][k]

        flag = False
        for j in range(5):
            if (all(map(lambda x: x == -1, boards[i][j]))):
                flag = True
                break

            b2 = list(map(lambda x: boards[i][x][j], range(5)))

            if (all(map(lambda x: x == -1, b2))):
                flag = True
                break

        if flag:
            b.append(i)

            if (len(b) == len(boards)):
                print(n * s)
                break
