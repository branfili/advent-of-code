#!/usr/bin/python3

import sys

def fold(s, i):
    s2 = set([])

    for p in s:
        if (p[i[0]] < i[1]):
            s2.add(p)
        else:
            l = [0, 0]
            l[1 - i[0]] = p[1 - i[0]]
            l[i[0]] = 2 * i[1] - p[i[0]]

            s2.add(tuple(l))

    return s2

lines = sys.stdin.read().split('\n')[:-1]

x = lines.index('')

points = set(map(lambda line: tuple(map(int, line.split(','))), lines[:x]))

instructions = list(map(lambda line: ('xy'.index(line.split()[-1][0]), int(line.split()[-1][2:])), \
                        lines[x + 1 :]))

for i in instructions:
    points = fold(points, i)

mxX = max(map(lambda p: p[0], points))
mxY = max(map(lambda p: p[1], points))

for y in range(mxY + 1):
    for x in range(mxX + 1):
        if ((x, y) in points):
            print('#', end='')
        else:
            print('.', end='')
    print(end='\n')
