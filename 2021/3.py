#!/usr/bin/python3

import sys
import copy

lines = sys.stdin.read().split('\n')[:-1]
lines2 = copy.deepcopy(lines)

k = len(lines[0])

for i in range(k):
    s = sum(map(lambda line: int(line[i]), lines))
    s2 = sum(map(lambda line: int(line[i]), lines2))

    a = ('1' if 2 * s < len(lines) else '0')
    b = ('0' if 2 * s2 < len(lines2) else '1')

    if (len(lines) > 1):
        lines = list(filter(lambda line: line[i] == a, lines))
    if (len(lines2) > 1):
        lines2 = list(filter(lambda line: line[i] == b, lines2))

co2 = int(lines[0], 2)
o2 = int(lines2[0], 2)

print(o2 * co2)
