#!/usr/bin/python3

pos = list(map(int, input().split(',')))

result = max(pos) ** 2 * len(pos)
for i in range(max(pos)):
    s = 0
    for x in pos:
        d = abs(x - i)
        s += d * (d + 1) // 2

    result = min(result, s)

print(result)
