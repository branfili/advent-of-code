#!/usr/bin/python3

import sys
from collections import deque

d = [(1, 0), \
     (1, 1), \
     (0, 1), \
     (-1, 1), \
     (-1, 0), \
     (-1, -1), \
     (0, -1), \
     (1, -1) ]

q = deque()

lines = sys.stdin.read().split('\n')[:-1]

octi = list(map(lambda line: list(map(int, line)), lines))

step = 0
result = 0
while True:
    flag = True
    for i in range(len(octi)):
        for j in range(len(octi[i])):
            if (octi[i][j] != 0):
                flag = False
                break
        if (not flag):
            break
    if (flag):
        break

    for i in range(len(octi)):
        for j in range(len(octi[i])):
            octi[i][j] += 1

            if (octi[i][j] == 10):
                octi[i][j] = 0
                result += 1
                q.append((i, j))

    while q:
        x, y = q.popleft()
        for dx, dy in d:
            nx, ny = x + dx, y + dy

            if (nx < 0 or \
                nx >= len(octi) or \
                ny < 0 or \
                ny >= len(octi[0])):
                continue

            if (octi[nx][ny] == 0):
                continue

            octi[nx][ny] += 1

            if (octi[nx][ny] == 10):
                octi[nx][ny] = 0
                result += 1
                q.append((nx, ny))

    step += 1

print(step)
