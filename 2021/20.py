#!/usr/bin/python3

import sys

def convert(s):
    global convKey
    return int(''.join(map(lambda c: convKey[c], s.strip())), 2)

def nextStep(x, y, step):
    global floorMap, mapSize, IEA, convKeyInv

    s = ''
    for dx in range(-1, 1 + 1):
        for dy in range(-1, 1 + 1):
            nx, ny = x + dx, y + dy

            if (nx < 0 or \
                ny < 0 or \
                nx >= mapSize[0] or \
                ny >= mapSize[1]):
                s += convKeyInv[step % 2]
            else:
                s += floorMap[nx][ny]

    return IEA[convert(s)]

convKey = {
            '.': '0',
            '#': '1',
}
convKeyInv = '.#'

maxStep = 50

lines = sys.stdin.read().split('\n')[:-1]

IEA = lines[0]
img = lines[2:]

N, M = len(img), len(img[0])

mapSize = (200, 200) #originally run with (1000, 1000); downscaled for time efficency (with the same accuracy)
floorMap = []
for i in range(mapSize[0]):
    s = ''
    for j in range(mapSize[1]):
        if (i >= mapSize[0] // 2 - N // 2 and \
            i < mapSize[0] // 2 + N // 2 and \
            j >= mapSize[1] // 2 - M // 2 and \
            j < mapSize[1] // 2 + M // 2):
            s += img[i - (mapSize[0] // 2 - N // 2)]\
                    [j - (mapSize[1] // 2 - M // 2)]
        else:
            s += '.'

    floorMap.append(s)

for step in range(maxStep):
    nFloorMap = []
    for i in range(mapSize[0]):
        s = ''
        for j in range(mapSize[1]):
            s += nextStep(i, j, step)

        nFloorMap.append(s)

    floorMap = nFloorMap

result = 0
for i in range(mapSize[0]):
    for j in range(mapSize[1]):
        result += int(convKey[floorMap[i][j]])

print(result)
