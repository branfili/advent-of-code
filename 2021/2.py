#!/usr/bin/python3

import sys

lines = sys.stdin.read().split('\n')[:-1]

x, y, a = 0, 0, 0
for line in lines:
    d, v = line.split()
    v = int(v)

    if (d == 'forward'):
        x += v
        y += v * a
    elif (d == 'down'):
        a += v
    elif (d == 'up'):
        a -= v

print(x * y)
