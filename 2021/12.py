#!/usr/bin/python3

import sys

lines = sys.stdin.read().split('\n')[:-1]

global neighbors
neighbors = {}

def dfs(node, path, visitedSmall):
    global neighbors

    if (node == 'end'):
        return 1

    s = 0
    for ns in neighbors[node]:
        if (ns.islower() and \
            ns in path):
            if (not visitedSmall and
                ns != 'start'):
                s += dfs(ns, [ns] + path, True)

            continue

        s += dfs(ns, [ns] + path, visitedSmall)

    return s

for l in lines:
    tokens = l.split('-')

    if (tokens[0] in neighbors.keys()):
        neighbors[tokens[0]].append(tokens[1])
    else:
        neighbors[tokens[0]] = [tokens[1]]

    if (tokens[1] in neighbors.keys()):
        neighbors[tokens[1]].append(tokens[0])
    else:
        neighbors[tokens[1]] = [tokens[0]]

print(dfs('start', ['start'], False))
