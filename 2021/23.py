#!/usr/bin/python3

import sys

from heapq import heappush as push
from heapq import heappop as pop

from copy import deepcopy

from collections import defaultdict

def output(burs, hall):
    global bSize

    print('#' * (len(hall) + 2))
    print('#' + hall + '#')

    for i in range(bSize):
        if (i == 0):
            print('###', end = '')
        else:
            print('  #', end = '')

        s = burs[i * 4 : (i + 1) * 4]
        print(s[0] + '#' + s[1] + '#' + s[2] + '#' + s[3], end = '')

        if (i == 0):
            print('###')
        else:
            print('#  ')

    print('  #########  ')

def weight(c):
    return 10 ** (ord(c) - ord('A'))

def solved(burs, hall):
    global bSize

    return (burs == 'ABCD' * bSize and \
            hall == '.' * 11)

def heurist(burs, hall):
    result = 0
    return result
    l = [0, 0, 0, 0]
    for i, c in enumerate(burs):
        if (c == '.'):
            continue

        typ = ord(c) - ord('A')
        cur = i % 4

        if (typ == cur):
            continue

        result += weight(c) * ((i // 4) + 1 + 2 * abs(cur - typ))
        l[typ] += 1

    for i, c in enumerate(hall):
        if (c == '.'):
            continue

        typ = ord(c) - ord('A')

        result += weight(c) * abs(i - 2 * (typ + 1))
        l[typ] += 1

    for i in range(len(l)):
        result += (l[i] * (l[i] + 1) // 2) * (10 ** i)

    return result

bSize = 2

burrowMap = sys.stdin.read().split('\n')[:-1]

if (bSize == 4):
    burrowMap = burrowMap[:-2] + \
                ['  #D#C#B#A#  ', \
                 '  #D#B#A#C#  '] + \
                burrowMap[-2:]

burs = ''
for x in range(bSize):
    burs += burrowMap[x + 2][3:11:2]

hall = burrowMap[1][1:-1]

startBurs = deepcopy(burs)
startHall = deepcopy(hall)

states = defaultdict(lambda: 1e10)

formerStates = {}

pq = []
push(pq, (heurist(burs, hall), 0, (burs, hall), None))

while pq:
    _, d, (burs, hall), fState = pop(pq)

    states[(burs, hall)] = d
    formerStates[(burs, hall)] = fState

    if (solved(burs, hall)):
        print(d)

        curState = (burs, hall)
        while (curState != (startBurs, startHall)):
            burs, hall = curState

            print()
            output(burs, hall)
            print(states[(burs, hall)])

            curState = formerStates[curState]

        print()
        output(startBurs, startHall)
        print(0)

        break

    for i, c in enumerate(hall):
        if (c == '.'):
            continue

        typ = ord(c) - ord('A')

        flag = True
        for j in range(bSize):
            if (burs[typ + j * 4] not in ['.', c]):
                flag = False
                break

        for x in range(bSize - 1, -1, -1):
            if (burs[typ + x * 4] == '.'):
                break

        absD = abs(i - 2 * (typ + 1))

        if (i < 2 * (typ + 1)):
            if (hall[i : 2 * (typ + 1)] != absD * '.'):
                continue
        else:
            if (hall[2 * (typ + 1) : i] != absD * '.'):
                continue

        burs2 = list(deepcopy(burs))
        hall2 = list(deepcopy(hall))

        hall2[i] = '.'
        burs2[typ + x * 4] = c

        burs2 = ''.join(burs2)
        hall2 = ''.join(hall2)

        d2 = d + weight(c) * (x + 1 + absD)

        if (d2 > states[(burs2, hall2)]):
            continue

        push(pq, (d2 + heurist(burs2, hall2), d2, (burs2, hall2), (burs, hall)))

    bursDone = [False, False, False, False]
    for i, c in enumerate(burs):
        if (c == '.'):
            continue

        typ = ord(c) - ord('A')
        cur = i % 4

        if (bursDone[cur]):
            continue

        for j in range(len(hall)):
            if (j % 2 == 0 and \
                j not in [0, 10]):
                continue

            absD = abs(j - 2 * (cur + 1))

            if (j < 2 * (cur + 1)):
                if (hall[j : 2 * (cur + 1)] != absD * '.'):
                    continue
            else:
                if (hall[2 * (cur + 1) : j] != absD * '.'):
                    continue

            burs2 = list(deepcopy(burs))
            hall2 = list(deepcopy(hall))

            burs2[i] = '.'
            hall2[j] = c

            burs2 = ''.join(burs2)
            hall2 = ''.join(hall2)

            d2 = d + weight(c) * ((i // 4) + 1 + absD)

            if (d2 > states[(burs2, hall2)]):
                continue

            push(pq, (d2 + heurist(burs2, hall2), d2, (burs2, hall2), (burs, hall)))

        bursDone[cur] = True
