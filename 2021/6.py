#!/usr/bin/python3

global d
d = {}

def rek(x, k):
    global d

    if ((x, k) in d.keys()):
        return d[(x, k)]

    if (k <= x):
        d[(x, k)] = 1
        return d[(x, k)]

    if (x > 7):
        d[(x, k)] = rek(x - 7, k - 7)
        return d[(x, k)]

    d[(x, k)] = rek(x, k - 7) + rek(x + 2, k - 7)
    return d[(x, k)]

l = list(map(int, input().split(',')))
N = 256

z = 0
for x in l:
    z += rek(x, N)
print(z)
