#!/usr/bin/python3

import sys

lines = list(map(int, sys.stdin.read().split('\n')[:-1]))

sums = []
for i in range(len(lines) - 2):
    sums.append(sum(lines[i:i+3]))

z = 0

for i in range(1, len(sums)):
    if (sums[i] > sums[i - 1]):
        z += 1

print(z)
