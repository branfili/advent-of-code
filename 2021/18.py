#!/usr/bin/python3

import sys

from functools import reduce
from itertools import permutations
from copy import deepcopy

class Node:
    def __init__(self, x = -1):
        self.x = x
        self.left = None
        self.right = None
        self.parent = None

    def add_left(self, left):
        self.left = left
        self.left.parent = self

    def add_right(self, right):
        self.right = right
        self.right.parent = self

def convert(line):
    if (line.isdigit()):
        return Node(int(line))

    l = []
    d = 0
    for i, x in enumerate(line[1 : -1]):
        if (d == 0):
            l.append(i + 1)

        if (x == '['):
            d += 1
        elif (x == ']'):
            d -= 1

    x = l[1]

    n = Node()

    n.add_left(convert(line[1 : x]))
    n.add_right(convert(line[x + 1 : -1]))

    return n

def explode(n, d):
    if (n.x != -1):
        return (False, n)

    if (d == 4):
        a, b = n.left.x, n.right.x

        n2 = n.parent
        last_n2 = n
        while (n2 is not None and \
               n2.left is last_n2):
            last_n2 = n2
            n2 = n2.parent

        if (n2 is not None):
            n2 = n2.left
            while (n2.x == -1):
                n2 = n2.right

            n2.x += a

        n2 = n.parent
        last_n2 = n
        while (n2 is not None and \
               n2.right is last_n2):
            last_n2 = n2
            n2 = n2.parent

        if (n2 is not None):
            n2 = n2.right
            while (n2.x == -1):
                n2 = n2.left

            n2.x += b

        return (True, Node(0))

    b1, nn = explode(n.left, d + 1)
    n.add_left(nn)

    b2, nn = explode(n.right, d + 1)
    n.add_right(nn)

    return (b1 or b2, n)

def split(n):
    if (n.x >= 10):
        n2 = Node(n.x // 2)
        n3 = Node(n.x - n.x // 2)

        n = Node()
        n.add_left(n2)
        n.add_right(n3)

        return (True, n)

    if (n.x != -1):
        return (False, n)

    b, nn = split(n.left)
    if (b):
        n.add_left(nn)
        return (True, n)

    b, nn = split(n.right)
    if (b):
        n.add_right(nn)
        return (True, n)

    return (False, n)

def fix(n):
    fl, n = explode(n, 0)
    if (not fl):
        fl, n = split(n)

    return (fl, n)

def add(n1, n2):
    n = Node()

    n.add_left(n1)
    n.add_right(n2)

    fl, n = fix(n)
    while (fl):
        fl, n = fix(n)

    return n

def output(n):
    if (n is None):
        print('None', end='')
        return

    if (n.x != -1):
        print(n.x, end='')
        return

    print('[', end='')
    output(n.left)
    print(',', end='')
    output(n.right)
    print(']', end='')

def mag(n):
    if (n.x != -1):
        return n.x

    return (3 * mag(n.left) + \
            2 * mag(n.right))

lines = sys.stdin.read().split('\n')[:-1]

snailnums = list(map(convert, lines))

sol = deepcopy(snailnums[0])
for s in snailnums[1:]:
    sol = add(sol, deepcopy(s))

print(mag(sol))

m = 0
for s1, s2 in permutations(snailnums, 2):
    m = max(m, mag(add(deepcopy(s1), deepcopy(s2))))

print(m)
