#!/usr/bin/python3

import sys

lines = sys.stdin.read().split('\n')[:-1]

s = 0
for l in lines:
    tokens = list(map(lambda t: set(list(t)), l.split()))

    inToken = tokens[:-5]
    inToken = sorted(inToken, key = len)

    one = inToken[0]
    four = inToken[2]
    six = list(filter(lambda t: not one.issubset(t)  and \
                                len(t) == 6,
                      inToken))[0]

    outToken = tokens[-4:]

    z = 0
    for t in outToken:
        z *= 10
        if (len(t) == 2):
            z += 1
        elif (len(t) == 3):
            z += 7
        elif (len(t) == 4):
            z += 4
        elif (len(t) == 7):
            z += 8
        elif (len(t) == 6):
            if (t == six):
                z += 6
            elif (four.issubset(t)):
                z += 9
            else:
                z += 0
        elif (len(t) == 5):
            if (one.issubset(t)):
                z += 3
            elif (t.issubset(six)):
                z += 5
            else:
                z += 2

    s += z

print(s)
