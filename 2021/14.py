#!/usr/bin/python3

import sys
import numpy as np

def convert(s):
    return (ord(s[0]) - ord('A')) * 26 + (ord(s[1]) - ord('A'))

N = 40

lines = sys.stdin.read().split('\n')[:-1]

polymer = lines[0]

mat = []
for i in range(26 ** 2):
    l = []
    for j in range(26 ** 2):
        l.append(0)
    mat.append(l)

for l in lines[2:]:
    dg, c = l.split(' -> ')

    mat[convert(dg[0] + c)][convert(dg)] += 1
    mat[convert(c + dg[1])][convert(dg)] += 1

v = []
for i in range(26 ** 2):
    v.append(0)

for i in range(len(polymer) - 1):
    v[convert(polymer[i : i + 2])] += 1

nv = np.array([v]).T
nm = np.array(mat)

for step in range(N):
    nv = np.matmul(nm, nv)

f = []
for i in range(0, 26 ** 2, 26):
    f.append(np.sum(nv[i : i + 26]))

f[ord(polymer[-1]) - ord('A')] += 1

f = sorted(f)
while f[0] == 0:
    f = f[1:]

print(f[-1] - f[0])
