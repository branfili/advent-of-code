#!/usr/bin/python3

import sys
import itertools

def intersect(a, b):
    if (a[0][0] == a[1][0] and \
        b[0][0] == b[1][0]):

        if (a[0][0] != b[0][0]):
            return set([])

        start = max(min(a[0][1], a[1][1]), \
                    min(b[0][1], b[1][1]))
        end = min(max(a[0][1], a[1][1]), \
                  max(b[0][1], b[1][1]))

        if (start > end):
            return set([])

        return set(map(lambda y: (a[0][0], y), \
                       range(start, end + 1)))

    if (a[0][0] == a[1][0]):
        a, b = b, a

    if (b[0][0] == b[1][0]):
        if (b[0][0] < min(a[0][0], a[1][0]) or \
            b[0][0] > max(a[0][0], a[1][0])):
            return set([])

        ka = (a[1][1] - a[0][1]) / float(a[1][0] - a[0][0])
        la = a[0][1] - ka * a[0][0]

        y = int(round(b[0][0] * ka + la))

        if (y > int(y) and y < int(y + 1)):
            return set([])

        if (y < min(a[0][1], a[1][1]) or \
            y > max(a[0][1], a[1][1]) or \
            y < min(b[0][1], b[1][1]) or \
            y > max(b[0][1], b[1][1])):
            return set([])

        return set([(b[0][0], y)])

    ka = (a[1][1] - a[0][1]) / float(a[1][0] - a[0][0])
    la = a[0][1] - ka * a[0][0]
    kb = (b[1][1] - b[0][1]) / float(b[1][0] - b[0][0])
    lb = b[0][1] - kb * b[0][0]

    if (abs(ka - kb) < 1E-6):
        if (abs(la - lb) >= 1E-6):
            return set([])

        start = max(min(a[0][0], a[1][0]), \
                    min(b[0][0], b[1][0]))
        end = min(max(a[0][0], a[1][0]), \
                  max(b[0][0], b[1][0]))

        if (start > end):
            return set([])

        return set(map(lambda x: (x, int(round(ka * x + la))), \
                       range(start, end + 1)))

    x = (lb - la) / (ka - kb)
    y = ka * x + la

    if ((x > int(x) and x < int(x + 1)) or \
        (y > int(y) and y < int(y + 1))):
        return set([])

    x = int(round(x))
    y = int(round(y))

    if (x < min(a[0][0], a[1][0]) or \
        x > max(a[0][0], a[1][0]) or \
        x < min(b[0][0], b[1][0]) or \
        x > max(b[0][0], b[1][0]) or \
        y < min(a[0][1], a[1][1]) or \
        y > max(a[0][1], a[1][1]) or \
        y < min(b[0][1], b[1][1]) or \
        y > max(b[0][1], b[1][1])):
        return set([])

    return set([(x, y)])

lines = sys.stdin.read().split('\n')[:-1]

smoke = []
for l in lines:
    tokens = l.split()

    smoke.append((tuple(map(int, tokens[0].split(','))), \
                  tuple(map(int, tokens[2].split(',')))))

result = set([])
for s1, s2 in itertools.combinations(smoke, 2):
    inter = intersect(s1, s2)
    result = result.union(intersect(s1, s2))

print(len(result))
